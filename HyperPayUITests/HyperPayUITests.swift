//
//  HyperPayUITests.swift
//  HyperPayUITests
//
//  Created by Suhayb Ahmad on 7/8/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import XCTest

class HyperPayUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLogin() {
        
        let app = XCUIApplication()
        let scrollViewsQuery = app.scrollViews
        let elementsQuery = scrollViewsQuery.otherElements
        
        elementsQuery.textFields["Username (Phone No.)"].tap()
        elementsQuery.textFields["Username (Phone No.)"].typeText("962788149187")
        
        elementsQuery.secureTextFields["Password"].tap()
        elementsQuery.secureTextFields["Password"].typeText("123456")
        
        scrollViewsQuery.children(matching: .other).element.tap()
        
        elementsQuery.buttons["SIGN IN"].tap()
        
        let dashElm = app.staticTexts["Dashboard"]
        let elmExists = NSPredicate(format: "exists == 1")
        
        expectation(for: elmExists, evaluatedWith: dashElm, handler: nil)
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testFailureLogin() {
        
        let app = XCUIApplication()
        let scrollViewsQuery = app.scrollViews
        let elementsQuery = scrollViewsQuery.otherElements
        
        elementsQuery.textFields["Username (Phone No.)"].tap()
        elementsQuery.textFields["Username (Phone No.)"].typeText("9627888888888")
        
        elementsQuery.secureTextFields["Password"].tap()
        elementsQuery.secureTextFields["Password"].typeText("1234ABCc")
        
        scrollViewsQuery.children(matching: .other).element.tap()
        
        elementsQuery.buttons["SIGN IN"].tap()
        
        let alertElm = app.alerts["HyperPay"]
        let alertExists = NSPredicate(format: "exists == 1")
        
        expectation(for: alertExists, evaluatedWith: alertElm, handler: nil)
        waitForExpectations(timeout: 20, handler: nil)
        
        let result = alertElm.staticTexts.element(boundBy: 1).label
        alertElm.buttons["OK"].tap()
        
        XCTAssertEqual(result,  "You Have entered wrong Username/Password")
    }
    
    func testSignUp() {
        
        let app = XCUIApplication()
        let scrollViewsQuery = app.scrollViews
        let elementsQuery = scrollViewsQuery.otherElements
        
        elementsQuery.buttons["Don't have an account? Sign Up Now"].tap()
        elementsQuery.textFields["First Name"].tap()
        elementsQuery.textFields["First Name"].typeText("Obaida")
        
        elementsQuery.textFields["Last Name"].tap()
        elementsQuery.textFields["Last Name"].typeText("Khaleel")
        
        elementsQuery.textFields["Email Address"].tap()
        elementsQuery.textFields["Email Address"].typeText("obaida.khaleel@hyperpay.com")
        
        elementsQuery.secureTextFields["Password"].tap()
        elementsQuery.secureTextFields["Password"].typeText("1234ABC")
        
        elementsQuery.secureTextFields["Confirm Password"].tap()
        elementsQuery.secureTextFields["Confirm Password"].typeText("1234ABC")
        
        elementsQuery.textFields["Merchant name"].tap()
        elementsQuery.textFields["Merchant name"].typeText("Hyperpay")
        
        elementsQuery.textFields["Phone Number"].tap()
        elementsQuery.textFields["Phone Number"].typeText("7888888888")
        
        scrollViewsQuery.children(matching: .other).element.tap()
        elementsQuery.buttons["Create Account"].tap()
    }
    
}
