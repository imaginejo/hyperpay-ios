//
//  HyperPayApi.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 7/3/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON
import Result
import Alamofire
import KeychainAccess


enum ApiEnviroment {
    case production
    case staging
    
    var host:String {
        
        switch self {
        case .production:
            return "http://hypermob.hyperpay.com"
        case .staging:
            return "https://private-068b05-hyperpaymobileapi.apiary-mock.com"
        }
    }
}

enum ApiLanguage:String {
    case arabic = "ar"
    case english = "en"
}

enum HyperPay {
    
    case register(firstname:String, lastname:String, email:String, password:String, merchant:String, mobile:String)
    case authenticate(username:String, password:String)
    case summary(date:Date)
    case transactions(reportType:String, channel:String, fromDate:Date?, toDate:Date?, lastTransactionId:Int, pageSize:Int, sort:String)
    case searchTransactions(reportType:String, channel:String, fromDate:Date?, toDate:Date?, filters:[String:Any]?, lastTransactionId:Int, pageSize:Int, sort:String)
    case graph(channel:String, from:Date, to:Date)
    case notifications(lastNotificationId:Int, pageSize:Int)
    
    case deleteNotification(id:Int)
    case deleteAllNotifications
    
    case appConfig
    case registerFirebase(token:String)
    case updateUserSettings(push:Bool, lang:String, oldPass:String?, newPass:String?)
    
    case resetPassword(username:String, password:String, pin:String)
    case forgotPassword(username:String)
    
    case validatePin(username:String, pin:String)
    
    case none
}

extension HyperPay:TargetType {
    
    var baseURL: URL {
        return URL(string: "\(Network.environment.host)/api/v1")!
    }
    
    var path: String {
        
        switch self {
        case .register:
            return "/request"
        case .authenticate:
            return "/auth/login"
        case .summary:
            return "/transaction/summary"
        case .transactions:
            return "/transaction/list"
        case .searchTransactions:
            return "/transaction/search"
        case .graph:
            return "/transaction/graph"
        case .notifications:
            return "/notification/list"
        case .deleteNotification:
            return "/notification/id"
        case .deleteAllNotifications:
            return "/notification/all"
        case .appConfig:
            return "/app/config"
        case .registerFirebase:
            return "/user/firebase"
        case .updateUserSettings:
            return "/user/settings"
        case .forgotPassword:
            return "/auth/forget-password"
        case .resetPassword:
            return "/auth/reset-password"
        case .validatePin:
            return "/auth/validate-bin"
        case .none:
            return ""
        }
    }
    
    var method: Moya.Method {
        
        switch self {
        case .summary,
             .searchTransactions,
             .transactions,
             .graph,
             .notifications:
            
            return .get
            
        case .deleteNotification, .deleteAllNotifications:
            
            return .delete
            
        case .register,
             .authenticate,
             .appConfig,
             .registerFirebase,
             .updateUserSettings,
             .resetPassword, .forgotPassword, .validatePin:
            
            return .post
            
        case .none:
            
            return .get
        }
    }
    
    var sampleData: Data {
        return Data(count: 0)
    }
    
    var task: Task {
        
        switch self {
        case .none:
            return .requestPlain
        case let .register(firstname, lastname, email, password, merchant, mobile):
            
            return .requestParameters(parameters: [
                    "first_name": firstname,
                    "last_name": lastname,
                    "email": email,
                    "password": password,
                    "merchant_name": merchant,
                    "mobile_number": mobile,
                ], encoding: JSONEncoding.default)
            
        case let .authenticate(username, password):
            
            return .requestParameters(parameters: [
                        "username": username,
                        "password": password,
                        "grant_type": "password",
                        "client_id": 2,
                        "client_secret": "vSaMdV0kGv3ohxatziq1fmQIqccf1iMnttA0w1xh"
                    ], encoding: JSONEncoding.default)
            
        case .summary(let date):
            
            return .requestParameters(parameters: ["date": date.serverDayFormat ], encoding: URLEncoding.default)
            
        case let .transactions(reportType, channel, fromDate, toDate, lastTransactionId, pageSize, sort):
            
            var params:[String:Any] = [
                "reportType": reportType,
                "channelID": channel,
                "lastTransactionId": lastTransactionId,
                "pageSize": pageSize,
                "sort": sort,
            ]
            
            if let start = fromDate {
                params["from"] = start.serverDayFormat
            }
            
            if let end = toDate {
                params["to"] = end.serverDayFormat
            }
            
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
            
        case let .graph(_, from, to):
            
            return .requestParameters(parameters: [
                        "channelID": "",
                        "from": from.serverDayFormat,
                        "to": to.serverDayFormat
                    ], encoding: URLEncoding.default)
            
        case let .notifications(lastNotificationId, pageSize):
            
            return .requestParameters(parameters: [
                        "lastNotificationId": lastNotificationId,
                        "pageSize": pageSize
                    ], encoding: URLEncoding.default)
        
        case .deleteNotification(let id):
            
            return .requestParameters(parameters: ["id": id], encoding: JSONEncoding.default)
            
        case .appConfig:
            
            return .requestPlain
            
        case let .searchTransactions(reportType, channel, fromDate, toDate, filters, lastTransactionId, pageSize, sort):
            
            var params:[String:Any] = [
                "reportType": reportType,
                "channelID": channel,
                "lastTransactionId": lastTransactionId,
                "pageSize": pageSize,
                "sort": sort,
            ]
            
            if let start = fromDate {
                params["from"] = start.serverDayFormat
            }
            
            if let end = toDate {
                params["to"] = end.serverDayFormat
            }
            
            if let _filters = filters {
                _filters.forEach { (name, value) in
                    params[name] = value
                }
            }
            
            debugPrint(params)
            
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
            
        case .registerFirebase(let token):
            
            return .requestParameters(parameters: ["token": token], encoding: JSONEncoding.default)
        case let .updateUserSettings(push, lang, oldPass, newPass):
            
            return .requestParameters(parameters: [
                "transaction_notification" : push,
                "preferred_language": lang,
                "old_password": oldPass ?? NSNull(),
                "new_password": newPass ?? NSNull()
            ], encoding: JSONEncoding.default)
            
        case .deleteAllNotifications:
            
            return .requestPlain
            
        case .resetPassword(let username, let password, let pin):
            
            return .requestParameters(parameters: [
                        "username": username,
                        "pin": pin,
                        "password": password
                    ], encoding: JSONEncoding.default)
            
        case .forgotPassword(let username):
            
            return .requestParameters(parameters: ["username": username], encoding: JSONEncoding.default)
            
        case .validatePin(let username, let pin):
            
            return .requestParameters(parameters: ["username": username, "pin": pin], encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        
        let deviceToken:String = UserDefaults.standard[.deviceToken] ?? "iOS_SIMULATOR"
        var headers =  [
            "Accept-Language" : Network.language.rawValue,
            "Source" : "iOS",
            "Device-Id": deviceToken,
            "Version" : "1.1"
        ]
        
        if self.needsAuth {
            headers["Authorization"] = "Bearer \(Network.authenticatedUser!.accessToken)"
        }
        
        return headers
    }
    
    var needsAuth:Bool {
        
        switch self {
        case .register,
             .authenticate,
             .resetPassword, .forgotPassword, .validatePin:
            return false
        default:
            return true
        }
    }
}

typealias SuccessHandler = (Any) -> Void
typealias FailureHandler = (JSON) -> Void

enum CustomError: Error {
    case custom(String)
    case something
}

extension MoyaError {
    
    var error:Error? {
        
        switch self {
        case .underlying(let error, _):
            return error
        default:
            return nil
        }
    }
}

extension CustomError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .custom(let message):
            return message
        case .something:
            return "Something went wrong"
        }
    }
}

struct Network {
    
    static var isAuthenticated:Bool {
        
        if let isAuth:Bool = UserDefaults.standard[.isAuthenticated] {
            return isAuth && containsCredintials()
        } else {
            removeCredintials()
        }
        
        return false
    }
    
    static private(set) var authenticatedUser:ResponseModel.User!
    static private(set) var appConfig:ResponseModel.AppConfig!
    
    static var environment:ApiEnviroment = .production
    static var language:ApiLanguage = Locale.current.isRightToLeft ? .arabic : .english
    
    static func restoreUser(completion:@escaping (Bool, String?) -> Void) {
        
        guard self.isAuthenticated, let (user, pass) = retreiveCredintials() else {
            self.logoutUser()
            completion(false, "Not Authenticated")
            return
        }
        
        self.authenticateUser(withPhoneNumber: user, andPassword: pass, completion: completion)
    }
    
    static func authenticateUser(withPhoneNumber number:String, andPassword password:String, completion:@escaping (Bool, String?) -> Void) {
        
        Network.request(.authenticate(username: number, password: password), success: { (model) in
            
            if let user = model as? ResponseModel.User {
                
                Network.authenticatedUser = user
                Network.registerFirebase()
                
                UserDefaults.standard[.isAuthenticated] = true
                UserDefaults.standard[.companyName] = user.companyName
                secureUsername(number, andPassword: password)
                
                let group = DispatchGroup()
                
                group.enter()
                Network.request(.appConfig, success: { (model) in
                    
                    Network.appConfig = model as? ResponseModel.AppConfig
                    group.leave()
                    
                }) { (err) in
                    
                    print("Failed to load app config")
                    group.leave()
                }
                
                group.enter()
                Alamofire.request(user.companyLogoUrl).responseImage(completionHandler: { (res) in
                    
                    user.image = res.value
                    
                    if let imgData = res.value {
                        UserDefaults.standard[.companyPicture] = UIImagePNGRepresentation(imgData)
                    }
                    
                    group.leave()
                })
                
                group.notify(queue: DispatchQueue.main, execute: {
                    
                    guard Network.appConfig != nil else {
                        Network.logoutUser()
                        completion(false, "login-failure".local)
                        return
                    }
                    
                    completion(true, nil)
                })
                
            } else {
                
                completion(false, nil)
            }
            
        }) { (err) in
            
            completion(false, err.localizedDescription)
        }
    }
    
    static var isFirebaseTokenSent:Bool = false
    static func registerFirebase() {
        
        if self.isFirebaseTokenSent == false,
            Network.authenticatedUser != nil,
            let token:String = UserDefaults.standard[.deviceToken] {
            
            Network.request(.registerFirebase(token: token), success: { (model) in
                
                if let success = model as? Bool, success {
                    
                    self.isFirebaseTokenSent = true
                    print("FIREBASE TOKEN UPDATED SUCCESSFULLY")
                } else {
                    print("FAILED TO UPDATE FIREBASE TOKEN")
                }
                
            }) { (err) in
                
                print("ERROR SENDING FIREBASE TOKEN: \(err.localizedDescription)")
            }
        }
    }
    
    static func logoutUser() {
        
        removeCredintials()
        UserDefaults.standard.deleteObject(.isAuthenticated)
        UserDefaults.standard.deleteObject(.companyName)
        UserDefaults.standard.deleteObject(.companyPicture)
    }
    
    static let provider = MoyaProvider<HyperPay>(plugins: [
        NetworkLoggerPlugin(cURL: true)
    ])
    
    struct DummyCancelled:Cancellable {
        var isCancelled: Bool = true
        func cancel() {}
    }
    
    @discardableResult static func request(_ target:HyperPay, success:@escaping SuccessHandler, failure:@escaping (MoyaError) -> Void) -> Cancellable {
        
        switch target {
        case .none:
            return DummyCancelled()
        default:
            break
        }
        
        let successHandler = success
        
        return provider.request(target) { (result) in
            
            switch result {
            case .success(let res):
                
                do {
                    
                    let json = try JSON(data: res.data)
                    
                    debugPrint(json)
                    
                    if let success = json["success"].bool, success && json["result"].exists() {
                        
                        if let model = ResponseModel.parse(target: target, json: json["result"]) {
                            successHandler(model)
                            return
                        }
                        
                    } else if let err = json["errors"].dictionary {
                        
                        var errors = ""
                        
                        for (_, value) in err {
                            let er = value.string ?? ""
                            errors += er.count > 0 ? "* \(er)\n" : ""
                        }
                        
                        if errors.hasSuffix("\n") {
                            errors = String( errors.prefix(errors.count - 1) )
                        }
                        
                        if err.keys.count == 1 {
                            errors = String( errors.suffix(from: errors.index(errors.startIndex, offsetBy: 2)) )
                        }
                        
                        failure( MoyaError.underlying(CustomError.custom(errors), nil) )
                        return
                        
                    } else if let err = json["errors"].string {
                        
                        failure( MoyaError.underlying(CustomError.custom(err), nil) )
                        return
                    }
                    
                    failure( MoyaError.underlying(CustomError.something, nil) )
                    
                } catch {
                    
                    failure( MoyaError.underlying(error, res) )
                }
                
            case .failure(let err):
                failure(err)
            }
        }
    }
}

@discardableResult func secureUsername(_ username:String, andPassword password:String ) -> Bool {
    
    let keychain = Keychain(service: "com.hyperpay.HyperMobile")
    
    do {
        try keychain.set(username, key: "Username")
        try keychain.set(password, key: "Password")
        return true
    } catch {
        return false
    }
}

func retreiveCredintials() -> (username:String, password:String)? {
    
    let keychain = Keychain(service: "com.hyperpay.HyperMobile")
    
    let username = try? keychain.get("Username")
    let password = try? keychain.get("Password")
    
    if let userOp = username, let user = userOp,
        let passOp = password, let pass = passOp {
        
        return (user, pass)
    }
    
    return nil
}

func containsCredintials() -> Bool {
    
    let keychain = Keychain(service: "com.hyperpay.HyperMobile")
    
    do {
        let userExists = try keychain.contains("Username")
        let passExists = try keychain.contains("Password")
        return userExists && passExists
    } catch {
        return false
    }
}

@discardableResult func removeCredintials() -> Bool {
    
    let keychain = Keychain(service: "com.hyperpay.HyperMobile")
    
    do {
        try keychain.remove("Username")
        try keychain.remove("Password")
        return true
    } catch {
        return false
    }
}
