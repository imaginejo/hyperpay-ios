//
//  ResponseModel.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 7/3/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftRichString

class ResponseModel {
    
    static func parse(target:HyperPay, json:JSON) -> Any? {
        
        switch target {
        case .none:
            return true
        case .register:
            return true
        case .authenticate:
            return User(json)
        case .summary:
            return Summary(json)
        case .transactions, .searchTransactions:
            return TransactionsResponse(json)
        case .graph:
            return parseGraphPoints(json)
        case .notifications:
            return parseNotifications(json)
        case .appConfig:
            return AppConfig(json)
        case .registerFirebase,
             .updateUserSettings,
             .deleteNotification,
             .deleteAllNotifications,
             .resetPassword,
             .forgotPassword:
            
            return true
        case .validatePin:
            
            return parsePinValidation(json)
        }
    }
    
    static func parsePinValidation(_ json:JSON) -> Bool {
        
        guard let valid = json["valid"].bool else {
            return false
        }
        return valid
    }
    
    static func parseUser(json:JSON) -> User? {
        return User(json)
    }
    
    static func parseConfig(json:JSON) -> AppConfig? {
        return AppConfig(json)
    }
    
    private static func parseNotifications(_ json:JSON) -> [Notification]? {
        
        guard let items = json["items"].array else {
            return nil
        }
        
        var notifsList = [Notification]()
        for ndata in items {
            
            if let notif = Notification(ndata) {
                
                notifsList.append(notif)
            }
        }
        
        return notifsList
    }
    
    private static func parseGraphPoints(_ json:JSON) -> [GraphPoint]? {
        
        guard let array = json.array else {
            return nil
        }
        
        var points = [GraphPoint]()
        for pdata in array {
            
            if let _date = pdata["requestDate"].shortDate,
                let volume = pdata["volume"].double,
                let count = pdata["count"].int {
                
                points.append( GraphPoint(date: _date, volume: volume, count: count) )
            }
        }
        
        return points
    }
    
    class AppConfig {
        
        var json:JSON
        var searchFields:[SearchFilterField]
        
        fileprivate init?(_ json:JSON) {
            
            guard let searchArr = json["search"].array else {
                return nil
            }
            
            self.json = json
            self.searchFields = searchArr.map({ (json) -> SearchFilterField in
                
                let name = json["name"].stringValue
                let type = json["type"].stringValue.lowercased().trimmingCharacters(in: .whitespaces)
                
                let field:SearchFilterField
                
                switch type {
                case "dropdown":
                    
                    let values = (json["options"].array ?? json["Options"].array)?.map({ (op) -> (String, String) in
                        return (op["label"].stringValue, op["value"].stringValue)
                    }) ?? [(String, String)]()
                    
                    let ftype:SearchFilterFieldType = values.count > 2 ? .multiSelect : .select
                    field = SearchFilterField(type: ftype, name: name, values: values)
                    
                case "numericcompare":
                    
                    field = SearchFilterField(type: .number, name: name)
                    
                default: //including "textbox"
                    
                    field = SearchFilterField(type: .text, name: name)
                }
                
                field.label = json["label"].string
                return field
            })
        }
    }
    
    class Notification {
        
        var id:Int
        var status:Int
        var date:Date
        var text:NSAttributedString
        var iconUrl:String
        var color:UIColor
        
        fileprivate init?(_ json:JSON) {
            
            guard let id = json["id"].int,
                let status = json["status"].int,
                let date = json["created_at"].serverDate,
                let rawtext = json["text"].string,
                let icon = json["icon"].string else {
                return nil
            }
            
            let pattern = "%%[^%]*%%"
            
            self.id = id
            self.status = status
            self.date = date
            self.iconUrl = icon
            
            if let colorHex = json["color"].string,
                let color = UIColor(hexString: colorHex),
                let regStyle = RegExpPatternStyles(pattern: pattern, style: { (style) -> (Void) in
                    style.color = color
                }) {
                
                let attrText = rawtext.set(regExpStyles: [regStyle])
                let regEx = try! NSRegularExpression(pattern: pattern, options: .allowCommentsAndWhitespace)
                
                while let match = regEx.firstMatch(in: attrText.string,
                                                   options: .reportCompletion,
                                                   range: NSRange(location: 0, length: attrText.length)) {
                        
                    let rng = match.range
                    let r1 = NSRange(location: rng.lowerBound, length: 2)
                    let r2 = NSRange(location: rng.upperBound - 4, length: 2)
                    
                    attrText.replaceCharacters(in: r1, with: "")
                    attrText.replaceCharacters(in: r2, with: "")
                }
                
                self.color = color
                self.text = attrText
                
            } else {
                
                self.color = UIColor.clear
                self.text = NSAttributedString(string: rawtext)
            }
        }
    }
    
    struct GraphPoint {
        
        var date:Date
        var volume:Double
        var count:Int
    }
    
    class Summary {
        
        struct Stats {
            
            static let zero = Stats(count: 0, volume: 0, countGrowth: 0, volumeGrowth: 0)
            
            var count:Int
            var volume:Double
            var countGrowth:Int
            var volumeGrowth:Int
        }
        
        class StatsGroup {
            
            var rejected = Stats.zero
            var positive = Stats.zero
            var neutral = Stats.zero
            var negative = Stats.zero
            
            var totalCount:Int = 0
            var totalVolume:Double = 0
            var id:String
            
            init(id:String) {
                
                self.id = id
            }
        }
        
        var channelGroups:[StatsGroup]
        var totalGroup:StatsGroup
        
        fileprivate init?(_ json:JSON) {
            
            guard let dict = json.dictionary else {
                return nil
            }
            
            self.channelGroups = [StatsGroup]()
            self.totalGroup = StatsGroup(id: "total")
            
            for (key, value) in dict {
                
                if let valDict = value.dictionary {
                    
                    let group:StatsGroup
                    if key == "total" {
                        group = self.totalGroup
                    } else {
                        
                        group = StatsGroup(id: key)
                        self.channelGroups.append(group)
                    }
                    
                    for (name, data) in valDict {
                        
                        if let count = data["count"].int, let volume = data["volume"].double {
                            
                            if name == "total" {
                                
                                group.totalCount = count
                                group.totalVolume = volume
                                
                            } else if
                                let countGrowth = data["countGrowth"].int,
                                let volumeGrowth = data["volumeGrowth"].int {
                                
                                let stat = Stats(count: count,
                                                 volume: volume,
                                                 countGrowth: countGrowth,
                                                 volumeGrowth: volumeGrowth)
                                
                                switch name {
                                case "rejected":
                                    group.rejected = stat
                                case "positive":
                                    group.positive = stat
                                case "neutral":
                                    group.neutral = stat
                                case "negative":
                                    group.negative = stat
                                default:
                                    break
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    class User {
        
        struct UserType {
            
            struct Pivot {
                var merchantUserID:Int
                var merchantTypeID:Int
            }
            
            var name:String
            var fields:String
            var pivot:Pivot
        }
        
        struct Channel {
            var id:String
            var currency:String
            var name:String
        }
        
        func channel(forId id:String) -> Channel? {
            
            return self.channels.first(where: { (ch) -> Bool in
                return ch.id == id
            })
        }
        
        var image:UIImage?
        var firstName:String
        var lastname:String
        var email:String
        var id:Int
        var mobileNumber:String
        
        var accessToken:String
        var companyLogoUrl:String
        var companyName:String
        var transactionNotificaiton:Bool
        
        var channels = [Channel]()
        var types = [UserType]()
        
        var fullName:String {
            return "\(self.firstName) \(self.lastname)"
        }
        
        fileprivate init?(_ json:JSON) {
            
            let info = json["userInfo"]
            let types = json["userType"]
            let cinfo = json["CompanyInformation"]
            
            /*
            "userInfo": {
                "id": 1,
                "merchant_id": 1,
                "first_name": "Bashar",
                "last_name": "Khanfar",
                "email": "basharkhanfar@gmail.com",
                "mobile_number": "962788149187",
                "transaction_notification": 1,
                "preferred_language": "en",
                "status": 1
            },
            */
            guard let id = info["id"].int,
                let fname = info["first_name"].string,
                let lname = info["last_name"].string,
                let email = info["email"].string,
                let mobile = info["mobile_number"].string,
                let token = json["access_token"].string,
                let logoUrl = cinfo["logo"].string,
                let cName = cinfo["name"].string else {
                
                return nil
            }
            
            self.id = id
            self.firstName = fname
            self.lastname = lname
            self.email = email
            self.transactionNotificaiton = info["transaction_notification"].boolValue
            self.mobileNumber = mobile
            self.accessToken = token
            
            self.companyLogoUrl = logoUrl.hasPrefix("http") ? logoUrl : "\(Network.environment.host)/\(logoUrl)"
            self.companyName = cName
            self.channels = json["channels"].arrayValue.map({ (chjs) -> Channel in
                
                return Channel(id: chjs["channel_id"].string! ,
                               currency: chjs["currency"].string! ,
                               name: chjs["channel_name"].string! )
            })
            
            types.array?.forEach({ (tjs) in
                if let name = tjs["name"].string,
                    let fields = tjs["fields"].string,
                    let mUserID = tjs["pivot"]["merchant_user_id"].int,
                    let mTypeID = tjs["pivot"]["merchant_type_id"].int {
                    
                    self.types.append( UserType(
                        name: name,
                        fields: fields,
                        pivot: UserType.Pivot(merchantUserID: mUserID, merchantTypeID: mTypeID)) )
                }
            })
        }
    }
}

extension JSON {
    
    var stringInt:Int? {
        
        if let str = self.string {
            return Int(str)
        }
        
        return nil
    }
}

class TransactionsResponse {
    
    struct Summary {
        var positive:Double?
        var nutral:Double?
        var negative:Double?
        var rejected:Double?
        var total:Double?
        
        var positiveValue:Double { return self.positive ?? 0 }
        var nutralValue:Double { return self.nutral ?? 0 }
        var negativeValue:Double { return self.negative ?? 0 }
        var rejectedValue:Double { return self.rejected ?? 0 }
        var totalValue:Double { return self.total ?? 0 }
    }
    
    var summary:Summary
    var transactions:[TransactionReport]
    var pageSize:Int
    var resultsCount:Int
    
    init?(_ json:JSON) {
        
        let sumData = json["summary"]
        guard sumData.exists(), let array = json["items"].array else {
            return nil
        }
        
        self.summary = Summary(positive: sumData["positive"].double,
                               nutral: sumData["nutral"].double,
                               negative: sumData["negative"].double,
                               rejected: sumData["rejected"].double,
                               total: sumData["total"].double)
        
        var transactions = [TransactionReport]()
        for transJS in array {
            
            if let trans = TransactionReport(transJS) {
                transactions.append(trans)
            } else {
                print("SKIPPED TRANSACTION: \(transJS)")
            }
        }
        
        
        self.transactions = transactions
        self.pageSize = json["_meta"]["pageSize"].stringInt ?? -1
        self.resultsCount = json["_meta"]["noOfResults"].int ?? -1
    }
}


class TransactionReport {
    
    var id:Int
    var transactionId:String
    var merchantName:String
    var channelName:String
    var channelId:String
    var currency:String
    var amount:Double
    var result:String
    var uniqueId:String
    var shortId:String
    var paymentMethod:String
    var paymentType:String
    var bin:Int
    var binCountry:String
    var fullAccountNumber:Int
    var verStatus:String
    var returnCode:String
    var accountHolder:String
    var batchNo:String
    var insitituteName:String
    var requestDate:Date
    var resultDate:Date
    
    var riskScore:String?
    var connectorTxID3:String?
    var ccvResult:String?
    var credit:Double?
    var debit:Double?
    var reasonCode:String?
    var authorizeId:String?
    
    init?(_ json:JSON) {
        
        guard
            let _id = json["id"].int,
            let transId = json["TransactionId"].string,
            let merchName = json["MerchantName"].string,
            let chName = json["ChannelName"].string,
            let chID = json["ChannelId"].string,
            let crr = json["currency"].string,
            let amnt = json["Amount"].double,
            let res = json["Result"].string,
            let unqId = json["UniqueId"].string,
            let shtId = json["ShortId"].string,
            let ptMethod = json["PaymentMethod"].string,
            let ptType = json["PaymentType"].string,
            let _bin = json["Bin"].int,
            let binCtry = json["BinCountry"].string,
            let accNumber = json["FullAccountNumber"].int,
            let status = json["VerStatus"].string,
            let rtrnCode = json["ReturnCode"].string,
            let accHolder = json["AccountHolder"].string,
            let btNumber = json["BatchNo"].string,
            let instName = json["ClearingInsitituteName"].string,
            let reqDate = json["RequestTimestamp"].serverDate,
            let resDate = json["ResultTimestamp"].serverDate else {
                
                return nil
        }
        
        self.id = _id
        self.transactionId = transId
        self.amount = amnt
        self.batchNo = btNumber
        self.bin = _bin
        self.binCountry = binCtry
        self.channelName = chName
        self.merchantName = merchName
        self.currency = crr
        self.accountHolder = accHolder
        self.fullAccountNumber = accNumber
        self.insitituteName = instName
        self.requestDate = reqDate
        self.resultDate = resDate
        self.paymentType = ptType
        self.paymentMethod = ptMethod
        self.returnCode = rtrnCode
        self.verStatus = status
        self.uniqueId = unqId
        self.shortId = shtId
        self.result = res
        self.channelId = chID
        self.riskScore = json["ExtraFields"]["RiskScore"].string
        
        self.reasonCode = json["ReasonCode"].string
        self.debit = json["Debit"].double
        self.credit = json["Credit"].double
        self.connectorTxID3 = json["ConnectorTxID3"].string
        self.ccvResult = json["CVVResult"].string
        self.authorizeId = json["AuthorizeId"].string
    }
}

extension JSON {
    
    var shortDate:Date? {
        
        let df = DateFormatter()
        df.locale = Locale(identifier: "en_US")
        
        df.dateFormat = "yyyy-MM-dd"
        if let str = self.string, let date = df.date(from: str) {
            return date
        }
        
        df.dateFormat = "yyyy-MM"
        if let str = self.string, let date = df.date(from: str) {
            return date
        }
        
        return nil
    }
    
    var serverDate:Date? {
        
        let df = DateFormatter()
        df.locale = Locale(identifier: "en_US")
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let str = self.string {
            return df.date(from: str)
        }
        
        return nil
    }
}
