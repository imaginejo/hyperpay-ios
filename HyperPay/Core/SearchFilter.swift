//
//  SearchFilter.swift
//  HyperControls
//
//  Created by Suhayb Ahmad on 7/2/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

enum FilterCondition {
    case or
    case and
}

class SearchFilter:Equatable {
    
    static func == (lhs: SearchFilter, rhs: SearchFilter) -> Bool {
        return lhs.id == rhs.id
    }
    
    var condition:FilterCondition = .and
    
    var id:String = UUID().uuidString
    var field:SearchFilterField
    var value:SearchFilterValue
    var displayHeight:CGFloat
    
    init(_ field:SearchFilterField) {
        
        self.field = field
        self.displayHeight = 80
        
        switch field.type {
        case .multiSelect:
            self.value = SearchFilterMultiValue()
        case .select, .text:
            self.value = SearchFilterSimpleValue()
        case .number:
            self.value = SearchFilterNumberValue()
        case .nameValue:
            self.value = SearchFilterPairValue()
        }
    }
    
    private init(field:SearchFilterField, value:SearchFilterValue){
        
        self.field = field
        self.value = value
        self.displayHeight = 80
    }
    
    @discardableResult func multiValue(inserting value:String?, at index:Int) -> SearchFilterMultiValue {
        
        var nval = self.value as! SearchFilterMultiValue
        nval.values.insert(value, at: index)
        self.value = nval
        
        return nval
    }
    
    @discardableResult func multiValue(updating value:String?, at index:Int) -> SearchFilterMultiValue {
        
        var nval = self.value as! SearchFilterMultiValue
        nval.values[index] = value
        self.value = nval
        
        return nval
    }
    
    @discardableResult func multiValue(removingValueAt index:Int) -> SearchFilterMultiValue {
        
        var nval = self.value as! SearchFilterMultiValue
        nval.values.remove(at: index)
        self.value = nval
        
        return nval
    }
    
    func multiValue() -> SearchFilterMultiValue {
        
        return self.value as! SearchFilterMultiValue
    }
    
    @discardableResult func pairValue(name:String? = nil, value:String? = nil) -> SearchFilterPairValue {
        
        var nval = self.value as! SearchFilterPairValue
        
        if name != nil || value != nil {
            
            nval.name = name ?? nval.name
            nval.value = value ?? nval.value
            self.value = nval
        }
        
        return nval
    }
    
    @discardableResult func numberValue(operation:SearchFilterNumberComparison? = nil, value:Double? = nil) -> SearchFilterNumberValue {
        
        var nval = self.value as! SearchFilterNumberValue
        
        if operation != nil || value != nil {
            
            nval.operation = operation ?? nval.operation
            nval.value = value ?? nval.value
            self.value = nval
        }
        
        
        return nval
    }
    
    @discardableResult func simpleValue(value:String? = nil) -> SearchFilterSimpleValue {
        
        var nval = self.value as! SearchFilterSimpleValue
        
        if value != nil {
            nval.value = value ?? nval.value
            self.value = nval
        }
        
        return nval
    }
    
    
    func copy() -> SearchFilter {
        
        let filter = SearchFilter(field: self.field, value: self.value)
        filter.condition = self.condition
        filter.displayHeight = self.displayHeight
        return filter
    }
}

protocol SearchFilterValue:CustomStringConvertible {
    
    var isValid:Bool { get }
}

struct SearchFilterSimpleValue:SearchFilterValue {
    var value:String?
    
    var isValid: Bool {
        return self.value != nil
    }
    
    var description:String {
        return self.value ?? "NULL"
    }
}

struct SearchFilterMultiValue:SearchFilterValue {
    var values = [String?]()
    
    var description:String {
        var str = ""
        
        self.values.forEach { (vl) in
            str += vl ?? "NULL"
            str += ", "
        }
        
        return String( str.prefix(upTo: str.index(str.endIndex, offsetBy: -2)) )
    }
    
    var isValid: Bool {
        
        for i in 0 ..< values.count {
            if values[i] == nil {
                return false
            }
        }
        
        return true
    }
}

struct SearchFilterNumberValue:SearchFilterValue {
    
    var value:Double = 0
    var operation:SearchFilterNumberComparison = .equals
    
    var description:String {
        return "(\(self.operation) \(self.value))"
    }
    
    var isValid: Bool {
        return true
    }
}

struct SearchFilterPairValue:SearchFilterValue {
    
    var name:String?
    var value:String?
    
    var description:String {
        return "(\(self.name ?? "NULL") : \(self.value ?? "NULL"))"
    }
    
    var isValid: Bool {
        return self.name != nil && self.value != nil
    }
}

enum SearchFilterNumberComparison:String {
    case equals = "="
    case greaterThan = ">"
    case greaterThanOrEqual = ">="
    case smallerThan = "<"
    case smallerThanOrEqual = "<="
}

enum SearchFilterFieldType {
    case text
    case select
    case multiSelect
    case number
    case nameValue
}

class SearchFilterField {
    
    struct PossibleValue {
        var value:String
        var name:String?
        
        var title:String {
            return name ?? value
        }
    }
    
    var label:String?
    var name:String
    
    var title:String {
        return self.label ?? self.name
    }
    
    var type:SearchFilterFieldType
    var possibleValues:[PossibleValue]?
    
    init(type:SearchFilterFieldType, name:String) {
        self.name = name
        self.type = type
    }
    
    convenience init(type:SearchFilterFieldType, name:String, values:[(String, String)]) {
        
        self.init(type: type, name: name)
        self.possibleValues = values.map({ (pval) -> PossibleValue in
            let (tl, vl) = pval
            return PossibleValue(value: vl, name: tl)
        })
    }
    
    convenience init(type:SearchFilterFieldType, name:String, values:[String]) {
        
        self.init(type: type, name: name)
        self.possibleValues = values.map { (vl) -> PossibleValue in
            return PossibleValue(value: vl, name: nil)
        }
    }
}

class SearchFields {
    
    static let shared = SearchFields()
    var list:[SearchFilterField]
    
    private init() {
        
        self.list = [
            SearchFilterField(type: .text, name: "Short ID"),
            SearchFilterField(type: .text, name: "Transaction ID"),
            SearchFilterField(type: .text, name: "Unique ID"),
            SearchFilterField(type: .select, name: "Status"),
            SearchFilterField(type: .select, name: "Method"),
            SearchFilterField(type: .select, name: "Type"),
            SearchFilterField(type: .text, name: "Usage"),
            SearchFilterField(type: .multiSelect, name: "Currency", values: ["$", "€", "¥", "£", "JOD"]),
            SearchFilterField(type: .multiSelect, name: "Account country", values:
                [
                    "Jordan", "Iraq", "Syria",
                    "Lebanon", "United States", "United Kingdom",
                    "France", "Saudi Arabia", "United Arab Emirates"
                ]),
            SearchFilterField(type: .number, name: "Amount"),
            SearchFilterField(type: .text, name: "Bank Code / BIC"),
            SearchFilterField(type: .text, name: "Card or account holder"),
            SearchFilterField(type: .nameValue, name: "Connector Detail"),
            SearchFilterField(type: .text, name: "Credit Card Brand"),
            SearchFilterField(type: .nameValue, name: "Criterion"),
            SearchFilterField(type: .text, name: "Customer country"),
            SearchFilterField(type: .text, name: "Customer name"),
            SearchFilterField(type: .select, name: "Debit Card Brand"),
            SearchFilterField(type: .text, name: "Email"),
            SearchFilterField(type: .text, name: "Invoice ID"),
            SearchFilterField(type: .text, name: "IP"),
            SearchFilterField(type: .text, name: "Mandate ID"),
            SearchFilterField(type: .text, name: "Number / IBAN – BIN (First 6 Digits)"),
            SearchFilterField(type: .text, name: "Number / IBAN – Exact"),
            SearchFilterField(type: .text, name: "Number / IBAN – Last 4 Digits"),
            SearchFilterField(type: .text, name: "Purchase Date (yyyy-MM-dd)"),
            SearchFilterField(type: .text, name: "Return Code"),
            SearchFilterField(type: .select, name: "Result", values:[("Approved", "ACK"), ("Declined", "DCK")]),
            SearchFilterField(type: .number, name: "Riskscore"),
            SearchFilterField(type: .text, name: "Shopper ID"),
            SearchFilterField(type: .select, name: "Source"),
            SearchFilterField(type: .select, name: "Virtual Account Brand")
        ]
    }
}


