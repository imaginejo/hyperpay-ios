//
//  UIKitExtensions.swift
//  IzwetnaApp
//
//  Created by AhmeDroid on 12/13/17.
//  Copyright © 2017 Imagine Technologies. All rights reserved.
//

import UIKit
import QuartzCore

extension UIView {
    
    class func loadFromNibNamed(_ nibNamed: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
    func applyDarkShadow(opacity:Float, offsetY:CGFloat, radius:CGFloat = 1.0) -> Void {
        
        self.applyDarkShadow(opacity: opacity, offset: CGSize(width: 0, height: offsetY), radius: radius)
    }
    
    func applyDarkShadow(opacity:Float, offset:CGSize, radius:CGFloat = 1.0) -> Void {
        
        self.applyShadow(color: UIColor.black, opacity: opacity, offset: offset, radius: radius)
    }
    
    func applyShadow(color:UIColor, opacity:Float, offsetY:CGFloat, radius:CGFloat = 1.0) -> Void {
        
        self.applyShadow(color:color, opacity: opacity, offset: CGSize(width: 0, height: offsetY), radius: radius)
    }
    
    func applyShadow(color:UIColor, opacity:Float, offset:CGSize, radius:CGFloat = 1.0) -> Void {
        
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
        self.layer.shadowOffset = offset
        self.layer.shadowColor = color.cgColor
    }
    
    func shake(duration:CFTimeInterval = 0.3, displacement:CGFloat = 5 ) -> Void {
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = duration / 4.0
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - displacement, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + displacement, y: self.center.y))
        
        self.layer.add(animation, forKey: "position")
    }
}

extension CALayer {
    var borderUIColor: UIColor {
        set {
            self.borderColor = newValue.cgColor
        }
        
        get {
            return UIColor(cgColor: self.borderColor!)
        }
    }
    var shadowUIColor: UIColor {
        set {
            self.shadowColor = newValue.cgColor
        }
        
        get {
            return UIColor(cgColor: self.shadowColor!)
        }
    }
}

extension UIColor {
    
    var isGreen:Bool {
        
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return max(r, g, b) == g
    }
    
    var isRed:Bool {
        
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return max(r, g, b) == r
    }
    
    var isBlue:Bool {
        
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return max(r, g, b) == b
    }

    convenience init(valueRed red:CGFloat, green:CGFloat, blue:CGFloat, alpha:CGFloat) {
        self.init(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha)
    }
     
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        let hexColor = hexString.replacingOccurrences(of: "#", with: "")
        
        if hexColor.count == 8 {
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                a = CGFloat(hexNumber & 0x000000ff) / 255
                
                self.init(red: r, green: g, blue: b, alpha: a)
                return
            }
            
        } else if hexColor.count == 6 {
            
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                
                r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
                g = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
                b = CGFloat( hexNumber & 0x0000ff ) / 255
                
                self.init(red: r, green: g, blue: b, alpha: 1.0)
                return
            }
        }
        
        return nil
    }
}

extension String {
    
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func compressingWhiteSpaces() -> String {
        
        let nString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        
        do {
            
            let regEx = try NSRegularExpression(pattern: "\\s+", options: .allowCommentsAndWhitespace)
            return regEx.stringByReplacingMatches(in: nString,
                                                  options: .reportCompletion,
                                                  range: NSRange(location: 0, length: nString.count),
                                                  withTemplate: " ")
        } catch let error {
            
            print("Error compressing white spaces: \(error.localizedDescription)")
            return nString
        }
    }
    
    var local:String {
        return NSLocalizedString(self, comment: "")
    }
    
    func replacingArabicNumbers() -> String {
        
        let nf = NumberFormatter()
        nf.locale = Locale(identifier: "ar_JO")
        
        var _str = self
        
        for i in 0 ..< 10 {
            
            let number = NSNumber(value: i)
            if let arNum = nf.string(from: number) {
                _str = _str.replacingOccurrences(of: arNum, with: number.description(withLocale: Locale(identifier: "en_US")))
            }
        }
        
        return _str
    }
}

extension NSAttributedString {
    
    func heightWithConstrainedWidth(_ width: CGFloat) -> CGFloat {
        
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        return boundingBox.height
    }
}

extension UIImage {
    
    func imageWithColor(_ color:UIColor) -> UIImage {
        
        let size = self.size
        
        UIGraphicsBeginImageContextWithOptions(size, false, self.scale)
        let context = UIGraphicsGetCurrentContext();
        
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(.normal)
        
        let rect = CGRect(x:0.0, y:0.0, width:size.width, height:size.height);
        
        context?.clip(to: rect, mask: self.cgImage!)
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();
        
        return image;
    }
    
    func drawnCentrally(on anotherImage: UIImage) -> UIImage {
        
        return self.drawn(on: anotherImage,
                          offset: CGPoint(x: abs(self.size.width - anotherImage.size.width) / 2.0,
                                          y: abs(self.size.height - anotherImage.size.height) / 2.0))
    }
    
    func drawn(on anotherImage: UIImage, offset:CGPoint = CGPoint.zero) -> UIImage {
        
        let sz1 = self.size
        let sz2 = anotherImage.size
        let size = CGSize(width: max(sz1.width, sz2.width), height: max(sz1.height, sz2.height))
        
        UIGraphicsBeginImageContextWithOptions(size, false, self.scale)
        let context = UIGraphicsGetCurrentContext();
        
        context?.translateBy(x: 0, y: size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.draw(anotherImage.cgImage!, in: CGRect(origin: CGPoint.zero, size: sz2))
        context?.draw(self.cgImage!, in: CGRect(origin: offset, size: sz1))
        
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();
        
        return image;
    }
}

extension Float {
    
    func formatted(_ fractions:Int) -> String? {
        
        let numberFormat = NumberFormatter()
        numberFormat.maximumFractionDigits = fractions
        numberFormat.locale = Locale.current
        return numberFormat.string(from: NSNumber(value: self))
    }
}

extension Double {
    
    func formatted(_ fractions:Int) -> String? {
        
        let numberFormat = NumberFormatter()
        numberFormat.maximumFractionDigits = fractions
        numberFormat.minimumIntegerDigits = 1
        numberFormat.locale = Locale(identifier: "en")
        return numberFormat.string(from: NSNumber(value: self))
    }
}

extension UIApplication {
    
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        /*
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        */
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

class RoundedButton : UIButton {
    
    var borderLayer = CAShapeLayer()
    var corners:UIRectCorner?
    var radii:CGSize?
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat)
    {
        borderLayer.lineWidth = 1.0
        borderLayer.strokeColor = UIColor.clear.cgColor
        borderLayer.fillColor = UIColor.clear.cgColor
        
        let radii = CGSize(width: radius, height: radius)
        borderLayer.path = UIBezierPath(roundedRect: self.bounds,
                                        byRoundingCorners: corners,
                                        cornerRadii: radii).cgPath
        
        self.corners = corners
        self.radii = radii
        self.layer.addSublayer(borderLayer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        borderLayer.frame = self.layer.bounds
        
        if let _corners = self.corners,
            let _radii = self.radii {
            borderLayer.path = UIBezierPath(roundedRect: self.bounds,
                                            byRoundingCorners: _corners,
                                            cornerRadii: _radii).cgPath
        }
    }
}

extension UIButton {
    
    func setBackground( _ color:UIColor ) -> Void {
        
        if let blayer = self.layer.sublayers?.first as? CAShapeLayer {
            
            blayer.fillColor = color.cgColor
        } else {
            
            self.backgroundColor = color
        }
    }
    
    func clearBackground() -> Void {
        
        if let blayer = self.layer.sublayers?.first as? CAShapeLayer {
            blayer.fillColor = UIColor.clear.cgColor
        } else {
            self.backgroundColor = nil
        }
    }
    
    func setStroke(color:UIColor, width:CGFloat) -> Void {
        
        if let blayer = self.layer.sublayers?.first as? CAShapeLayer {
            
            blayer.strokeColor = color.cgColor
            blayer.lineWidth = width
        } else {
            
            layer.borderWidth = width
            layer.borderColor = color.cgColor
        }
    }
    
    func clearStroke() -> Void {
        
        if let blayer = self.layer.sublayers?.first as? CAShapeLayer {
            
            blayer.strokeColor = UIColor.clear.cgColor
        } else {
            
            layer.borderWidth = 0.0
            layer.borderColor = nil
        }
    }
}

class SnappingCollectionViewLayout: UICollectionViewFlowLayout {
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        guard let collectionView = collectionView else {
            return super.targetContentOffset(
                forProposedContentOffset: proposedContentOffset,
                withScrollingVelocity: velocity)
        }
        
        let dOffset = (collectionView.frame.width - self.itemSize.width) / 2.0
        var offsetAdjustment = CGFloat.greatestFiniteMagnitude
        let horizontalOffset = proposedContentOffset.x + dOffset
        
        let targetRect = CGRect(x: proposedContentOffset.x, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
        
        let layoutAttributesArray = super.layoutAttributesForElements(in: targetRect)
        layoutAttributesArray?.forEach({ (layoutAttributes) in
            
            let itemOffset = layoutAttributes.frame.origin.x
            if fabsf(Float(itemOffset - horizontalOffset)) < fabsf(Float(offsetAdjustment)) {
                offsetAdjustment = itemOffset - horizontalOffset
            }
        })
        
        return CGPoint(x: proposedContentOffset.x + offsetAdjustment, y: proposedContentOffset.y)
    }
}


extension Date {
    
    static func serverDayDate(string:String) -> Date? {
    
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        df.locale = Locale(identifier: "en_US")
    
        return df.date(from: string)
    }
    
    func format(_ format:String) -> String {
        return self.format(format, locale: Locale.current)
    }
    
    func format(_ format:String, locale:Locale) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    var serverDayFormat : String {
        
        let df = DateFormatter()
        df.locale = Locale(identifier: "en_US")
        df.dateFormat = "yyyy-MM-dd"
        return df.string(from: self)
    }
}


extension UIViewController {
    
    func fillEdgeSpace(withColor color:UIColor, edge:UIRectEdge) -> Void {
        
        guard edge == .top || edge == .bottom else {
            print("Can't be applied to right or left edges ..")
            return
        }
        
        if #available(iOS 11.0, *) {
            
            if let insets = UIApplication.shared.keyWindow?.safeAreaInsets {
                
                if edge == .top, insets.top <= 0 {
                    return
                }
                
                if edge == .bottom, insets.bottom <= 0 {
                    return
                }
                
                var r = self.view.frame
                r.origin.y = insets.top
                r.size.height -= insets.top + insets.bottom
                
                let bv = UIView(frame: CGRect(x: 0, y: r.maxY, width: r.width, height: insets.bottom))
                bv.backgroundColor = color
                self.view.addSubview(bv)
                
                if edge == .top {
                    bv.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
                } else {
                    bv.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
                }
                
                bv.heightAnchor.constraint(equalToConstant: insets.bottom).isActive = true
                bv.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
                bv.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
            }
        }
    }
}


extension UITabBar {
    // Workaround for iOS 11's new UITabBar behavior where on iPad, the UITabBar inside
    // the Master view controller shows the UITabBarItem icon next to the text
    override open var traitCollection: UITraitCollection {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UITraitCollection(horizontalSizeClass: .compact)
        }
        return super.traitCollection
    }
}

