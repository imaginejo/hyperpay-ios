//
//  Constants.swift
//  HyperPay
//
//  Created by Mac on 5/22/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

struct NotificationNames {
    
    static let ShowSideMenu = Notification.Name("ShowSideMenu")
    static let HideSideMenu = Notification.Name("HideSideMenu")
    static let HideSideMenuNonAnimated = Notification.Name("HideSideMenuNonAnimated")
    
    static let WeekTransactionCountWasRequested = Notification.Name("WeekTransactionCountWasRequested")
    static let MonthTransactionCountWasRequested = Notification.Name("MonthTransactionCountWasRequested")
    static let YearTransactionCountWasRequested = Notification.Name("YearTransactionCountWasRequested")
    static let ShowTransactionDetails = Notification.Name("ShowTransactionDetails")
}

struct Colors {
    
    static let main = UIColor(red: 180/255.0, green: 149/255.0, blue: 79/255.0, alpha: 1)
}


extension UserDefaults {
    
    public struct Key: Hashable, RawRepresentable, ExpressibleByStringLiteral {
        public var rawValue: String
        
        public init(rawValue: String) {
            self.rawValue = rawValue
        }
        
        public init(stringLiteral value: String) {
            self.rawValue = value
        }
    }
    
    func set<T>(_ value: T?, forKey key: Key) {
        set(value, forKey: key.rawValue)
    }
    
    func value<T>(forKey key: Key) -> T? {
        return value(forKey: key.rawValue) as? T
    }
    
    subscript<T>(key: Key) -> T? {
        get { return value(forKey: key) }
        set { set(newValue, forKey: key.rawValue) }
    }
    
    func register(defaults: [Key: Any]) {
        let mapped = Dictionary(uniqueKeysWithValues: defaults.map { (key, value) -> (String, Any) in
            return (key.rawValue, value)
        })
        
        register(defaults: mapped)
    }
    
    func deleteObject(_ key:Key) -> Void {
        self.removeObject(forKey: key.rawValue)
    }
}

public extension UserDefaults.Key {
    
    static let isAuthenticated:UserDefaults.Key = "IS_AUTHENTICATED"
    
    static let companyName:UserDefaults.Key = "COMPANY_NAME"
    static let companyPicture:UserDefaults.Key = "COMPANY_PICTURE"
    
    static let config: UserDefaults.Key = "APP_CONFIG"
    static let deviceToken: UserDefaults.Key = "DeviceToken"
    static let languageCode: UserDefaults.Key = "LanguageCode"
    static let regionCode: UserDefaults.Key = "RegionCode"
}
