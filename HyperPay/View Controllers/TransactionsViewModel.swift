//
//  TransactionsViewModel.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 8/6/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

protocol TransactionsViewModelDelegate: class {
    
    func displaySummary(_ summary:TransactionsResponse.Summary)
    func buildTransactionsRequest(lastIndex:Int) -> HyperPay
    func onFetchWillStart()
    func onFetchFinished()
    func onFetchCompleted(withSections sections:IndexSet, andRows rows:[IndexPath])
    func onFetchFailed(with reason: String)
}

fileprivate class TransactionReportsList {
    
    var day:String
    var list = [TransactionReport]()
    
    init(_ day:String) {
        self.day = day
    }
    
    subscript(index: Int) -> TransactionReport! {
        get { return list[index] }
        set { list[index] = newValue }
    }
}

final class TransactionsViewModel : NSObject {
    
    private(set) var totalCount:Int = 0
    private var transactions = [TransactionReportsList]()
    
    func indexForReportsListOfDay(_ day:String) -> Int? {
        
        return self.transactions.index { (list) -> Bool in
            return list.day == day
        }
    }
    
    var headersMap = [Int:DayHeaderView]()
    var pageLastIndex:Int = -1
    func reloadData() -> Void {
        
        self.pageLastIndex = 0
        self.clear()
        self.headersMap.removeAll()
        self.loadTransactionsData()
    }
    
    var hasMore : Bool {
        return self.pageLastIndex > -1
    }
    
    func isItemTheLast(_ index:IndexPath ) -> Bool {
        
        return index.section == self.transactions.count - 1
            && index.row == self.transactions[index.section].list.count - 1
    }
    
    weak var delegate:TransactionsViewModelDelegate?
    
    func loadTransactionsData() -> Void {
        
        guard self.pageLastIndex >= 0,
            let request = self.delegate?.buildTransactionsRequest(lastIndex: self.pageLastIndex) else {
            return
        }
        
        self.delegate?.onFetchWillStart()
        
        Network.request(request, success: { (model) in
                
            self.delegate?.onFetchFinished()
            
            guard let response = model as? TransactionsResponse else {
                self.delegate?.onFetchFailed(with: "There is no reports for today")
                return
            }
            
            self.totalCount = response.resultsCount
            if response.transactions.count == 5 {
                self.pageLastIndex = response.transactions.last!.id
            } else {
                self.pageLastIndex = -1
            }
            
            self.displayTransactionData(response.transactions)
            self.delegate?.displaySummary(response.summary)
            
        }, failure: { (err) in
            
            self.pageLastIndex = -1
            self.delegate?.onFetchFinished()
            self.delegate?.onFetchFailed(with: err.localizedDescription)
        })
    }
    
    private func displayTransactionData(_ loadedReports:[TransactionReport]) -> Void {
        
        let df = DateFormatter()
        df.dateFormat = "d MMMM yyyy - hh:mm a"
        
        var sections = IndexSet()
        var rows:[IndexPath] = []
        loadedReports.forEach { (rep) in
            
            let day = df.string(from: rep.requestDate ).uppercased()
            let section:Int
            
            var dayList:TransactionReportsList
            if let index = self.indexForReportsListOfDay(day) {
                dayList = self.transactions[index]
                section = index
            } else {
                dayList = TransactionReportsList(day)
                section = self.transactions.count
                sections.insert(section)
                self.transactions.append(dayList)
            }
            
            rows.append(IndexPath(row: dayList.list.count, section: section))
            dayList.list.append(rep)
        }
        
        self.delegate?.onFetchCompleted(withSections: sections, andRows: rows)
    }
    
    func refreshHeaderViews(_ tableView:UITableView ) -> Void {
        
        let coy = tableView.contentOffset.y
        
        var toBeRemoved = [Int]()
        
        self.headersMap.forEach { (section, view) in
            
            guard section < tableView.numberOfSections else {
                toBeRemoved.append(section)
                return
            }
            
            let hy = tableView.rectForHeader(inSection: section).origin.y
            let isFloating = coy > hy && view.isDisplayed
            
            if isFloating != view.isFloating {
                
                if isFloating {
                    view.darken()
                } else {
                    view.lighten()
                }
            }
            
            view.isFloating = isFloating
        }
        
        toBeRemoved.forEach { (sec) in
            self.headersMap.removeValue(forKey: sec)
        }
    }
    
    func clear() -> Void {
        
        self.transactions.forEach { (rep) in
            rep.list.removeAll()
        }
        self.transactions.removeAll()
    }
    
    var sectionsCount : Int {
        
        return self.transactions.count
    }
    
    func transactionsCountForSection( section:Int ) -> Int {
        
        if section < self.transactions.count {
            return self.transactions[section].list.count
        }
        
        return 0
    }
    
    func transactionsForIndex(_ index:IndexPath ) -> TransactionReport {
        
        return self.transactions[index.section].list[index.row]
    }
    
    func headerViewForSection(_ section:Int) -> DayHeaderView {
        
        if let view = self.headersMap[section] {
            
            return view
        } else {
            
            let view = DayHeaderView.create()
            view.dateLabel.text = self.transactions[section].day.replacingArabicNumbers()
            self.headersMap[section] = view
            
            return view
        }
    }
}

