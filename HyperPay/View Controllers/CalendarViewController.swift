//
//  CalendarViewController.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 7/4/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

enum CalendarSelectionMode {
    case year
    case month
    case week
    case day
}

class CalendarViewController: UIViewController {
    
    private let MAX:Int = 100_000
    private let dateFormatter = DateFormatter()
    
    private var months = [String]()
    private var days = [String]()
    private var weeks = [String]()
    
    private var refYear:Int = 0
    var calendar = Calendar(identifier: .gregorian)
    
    private var initialDate = Date()
    
    var startDate:Date = Date()
    var endDate:Date = Date()
    var duration:TimeInterval {
        
        return self.endDate.timeIntervalSince1970 - self.startDate.timeIntervalSince1970
    }
    
    typealias CalendarSelectionHandler = (Date, Date) -> Void
    
    var selectionHandler:CalendarSelectionHandler?
    
    static func launch(mode:CalendarSelectionMode, withInitialDate initDate:Date, selection:@escaping CalendarSelectionHandler) -> Void {
        
        if let topVC = UIApplication.topViewController(){
           
            let calendarVC = CalendarViewController(nibName: "CalendarViewController", bundle: nil)
            
            calendarVC.initialDate = initDate
            calendarVC.selectionHandler = selection
            calendarVC.modalPresentationStyle = .overCurrentContext
            calendarVC.modalTransitionStyle = .crossDissolve
            calendarVC.selectionMode = mode
            topVC.present(calendarVC, animated: true, completion: nil)
        }
    }
    
    var selectionMode:CalendarSelectionMode = .day
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBAction func closeWithSelection(_ sender: Any) {
        
        self.calculateTime()
        self.dismiss(animated: true) {
            self.selectionHandler?(self.startDate, self.endDate)
            self.selectionHandler = nil
        }
    }
    
    @IBAction func viewWasTapped(_ sender: Any) {
        
        self.selectionHandler = nil
        self.dismiss(animated: true, completion: nil)
    }
}

extension CalendarViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    override func viewDidLoad() {
        
        self.fillMonths()
        self.refYear = self.calendar.dateComponents(in: .current, from: Date()).year!
        
        let currentDate = self.initialDate
        let comps = self.calendar.dateComponents(in: .current, from: currentDate)
        
        let years = comps.year! - self.refYear
        self.pickerView.selectRow(years + MAX / 2, inComponent: 0, animated: false)
        
        switch self.selectionMode {
        case .week:
            self.updateWeeks()
        case .day:
            self.updateDays()
        default:
            break
        }
        
        switch self.selectionMode {
        case .month:
            let index = comps.month! - 1
            self.pickerView.selectRow(index, inComponent: 1, animated: false)
        case .week:
            let index = comps.weekOfYear! - 1
            self.pickerView.selectRow(index, inComponent: 1, animated: false)
        case .day:
            let months = comps.month! - 1
            self.pickerView.selectRow(months, inComponent: 1, animated: false)
            self.pickerView.selectRow(comps.day! - 1, inComponent: 2, animated: false)
        default:
            break
        }
    }
    
    func fillMonths() -> Void {
        
        let df = DateFormatter()
        df.dateFormat = "MMMM"
        var dateComps = DateComponents(calendar: self.calendar,
                                       timeZone: TimeZone.current, month: 0, day:1)
        for i in 1 ... 12 {
            dateComps.month = i
            self.months.append( df.string(from: dateComps.date!)  )
        }
    }
    
    func updateDays() -> Void {
        
        let months = self.pickerView.selectedRow(inComponent: 1) + 1
        
        var dateComps = DateComponents(calendar: self.calendar,
                                       timeZone: TimeZone.current, month: months, day:0)
        
        let df = DateFormatter()
        df.dateFormat = "dd"
        
        self.days.removeAll()
        
        for i in 1 ... 31 {
            dateComps.day = i
            if dateComps.isValidDate {
                self.days.append( df.string(from: dateComps.date!) )
            }
        }
        
        self.pickerView.reloadComponent(2)
    }
    
    func updateWeeks() -> Void {
        
        let years = self.pickerView.selectedRow(inComponent: 0) - MAX / 2
        
        var dateComps = DateComponents(calendar: self.calendar,
                                       timeZone: .current, year: years + refYear)
        
        print(dateComps.year!)
        dateComps.weekOfYear = 0
        
        let df = DateFormatter()
        df.dateFormat = "dd MMM"
        
        self.weeks.removeAll()
        
        for i in 1 ... 52 {
            
            df.dateFormat = "dd MMM"
            dateComps.weekOfYear = i
            dateComps.weekday = 1
            dateComps.hour = 0
            dateComps.minute = 0
            dateComps.second = 0
            dateComps.nanosecond = 0
            
            let str1 = df.string(from: dateComps.date! )
            
            dateComps.weekday = 7
            dateComps.hour = 23
            dateComps.minute = 59
            dateComps.second = 59
            dateComps.nanosecond = Int( 999e6 )
            
            let str2 = df.string(from: dateComps.date! )
            
            df.dateFormat = "ww"
            let noStr = df.string(from: dateComps.date! )
            
            self.weeks.append( "\(noStr) (\(str1) - \(str2))" )
        }
        
        self.pickerView.reloadComponent(1)
    }
    
    func calculateTime() -> Void {
        
        let df = DateFormatter()
        df.dateFormat = "dd MM yyyy HH:mm:ss.SSS"
        
        
        let years = self.pickerView.selectedRow(inComponent: 0) - MAX / 2
        
        var dateComps1 = DateComponents(calendar: self.calendar,
                                       timeZone: TimeZone.current, year: years + refYear)
        
        dateComps1.hour = 0
        dateComps1.minute = 0
        dateComps1.second = 0
        dateComps1.nanosecond = 0
        
        var dateComps2 = dateComps1
        dateComps2.hour = 23
        dateComps2.minute = 59
        dateComps2.second = 59
        dateComps2.nanosecond = Int( 999e6 )
        
        switch self.selectionMode {
        case .year:
            dateComps1.day = 1
            dateComps1.month = 1
            
            dateComps2.day = 31
            dateComps2.month = 12
        case .month:
            
            dateComps1.month = self.pickerView.selectedRow(inComponent: 1) + 1
            dateComps2.month = dateComps1.month
            
            dateComps1.day = 1
            
            for i in 28 ... 31 {
                
                dateComps2.day = i
                if dateComps2.isValidDate == false {
                    dateComps2.day = i - 1
                    break
                }
            }
            
        case .day:
            
            dateComps1.month = self.pickerView.selectedRow(inComponent: 1) + 1
            dateComps1.day = self.pickerView.selectedRow(inComponent: 2) + 1
            
            dateComps2.month = dateComps1.month
            dateComps2.day = dateComps1.day
            
        case .week:
            
            let weeks = self.pickerView.selectedRow(inComponent: 1) + 1
            
            dateComps1.weekday = 1
            dateComps2.weekday = 7
            
            if weeks == 1 {
                
                dateComps1.weekOfYear = 2
                dateComps2.weekOfYear = 2
                
                let date1 = Calendar.current.date(byAdding: .weekOfYear, value: -1, to: dateComps1.date! )!
                let date2 = Calendar.current.date(byAdding: .weekOfYear, value: -1, to: dateComps2.date! )!
                
                dateComps1 = Calendar.current.dateComponents(in: .current, from: date1)
                dateComps2 = Calendar.current.dateComponents(in: .current, from: date2)
                
            } else {
                
                dateComps1.weekOfYear = weeks
                dateComps2.weekOfYear = weeks
            }
        }
        
        self.startDate = dateComps1.date!
        self.endDate = dateComps2.date!
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        switch self.selectionMode {
        case .year:
            return 1
        case .month, .week:
            return 2
        case .day:
            return 3
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch component {
        case 0:
            return MAX
        case 1:
            
            switch self.selectionMode {
            case .day, .month:
                return self.months.count
            default:
                return self.weeks.count
            }
            
        case 2:
            return self.days.count
        default:
            return 0
        }
    }
    
    func titleForRow(_ row: Int, forComponent component: Int) -> String? {
        
        switch component {
        case 0:
            return self.yearText(forRow: row)
        case 1:
            
            switch self.selectionMode {
            case .day, .month:
                return self.months[row]
            default:
                return self.weeks[row]
            }
            
        case 2:
            return self.days[row]
        default:
            return "n/a"
        }
    }
    
    func attributedTitleForRow(_ row: Int, forComponent component: Int) -> NSAttributedString? {
        
        if let title = self.titleForRow(row, forComponent: component) {
            
            let attrStr = NSMutableAttributedString(string: title)
            if component == 1 && self.selectionMode == .week {
                
                let rng = NSRange(location: 3, length: title.count - 3)
                attrStr.addAttribute(.font, value: UIFont.systemFont(ofSize: 12), range: rng)
                attrStr.addAttribute(.foregroundColor, value: UIColor.brown, range: rng)
            }
            
            return attrStr
        }
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = view as? UILabel ?? UILabel()
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = UIColor.black
        label.attributedText = self.attributedTitleForRow(row, forComponent: component)
        label.sizeToFit()
        
        return label
    }
    
    func yearText(forRow row:Int) -> String {
        
        let di = row - MAX / 2
        let dateComps = DateComponents(calendar: self.calendar,
                                       timeZone: TimeZone.current, year: di + refYear)
        
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.string(from: dateComps.date!)
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if component == 0 && self.selectionMode == .week {
            self.updateWeeks()
        }
        
        if component == 1 && self.selectionMode == .day {
            self.updateDays()
        }
        
        self.calculateTime()
    }
}

