//
//  NotificationsViewController.swift
//  HyperPay
//
//  Created by Mac on 6/3/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import Moya
import SVPullToRefresh
import SVProgressHUD

class NotificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var notificationsList = [ResponseModel.Notification]()
    @IBOutlet weak var tableView: UITableView!
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 80.0
        
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = self.refreshControl
        } else {
            self.tableView.addSubview(self.refreshControl)
        }
        
        self.refreshControl.addTarget(self, action: #selector( refreshRequested ), for: .valueChanged)
        
        self.tableView.addInfiniteScrolling {
            
            self.loadNotificationsData(lastIndex: self.lastPageIndex, completion: { (_index) in
                
                self.lastPageIndex = _index ?? self.lastPageIndex
                self.tableView.infiniteScrollingView.stopAnimating()
            })
        }
        
        self.lastPageIndex = 0
        self.loadNotificationsData(lastIndex: 0)
    }
    
    var lastPageIndex:Int = 0
    
    @objc func refreshRequested() -> Void {
        
        self.lastPageIndex = 0
        self.notificationsList.removeAll()
        self.loadNotificationsData(lastIndex: 0) { (_index) in
            
            self.refreshControl.endRefreshing()
            self.lastPageIndex = _index ?? self.lastPageIndex
        }
    }
    
    var currentReq:Cancellable?
    func loadNotificationsData(lastIndex:Int, completion:((Int?) -> Void)? = nil) -> Void {
        
        self.currentReq?.cancel()
        guard lastIndex > -1 else {
            completion?(nil)
            return
        }
        
        let pg = 30
        self.currentReq = Network.request(.notifications(lastNotificationId: lastIndex, pageSize: pg), success: { (model) in
            
            if let notifs = model as? [ResponseModel.Notification] {
                
                self.notificationsList.append(contentsOf: notifs)
                self.tableView.reloadData()
                
                let index = -1
                if notifs.count == pg {
                    self.lastPageIndex = notifs.last!.id
                }
                
                completion?(index)
            } else {
                
                completion?(nil)
            }
            
        }) { (err) in
            
            completion?(nil)
            
            if (err as NSError?)?.code == NSURLErrorCancelled {
                showErrorMessage(err.localizedDescription)
            }
        }
    }
    
    @IBAction func clearAllNotifications(_ sender: Any) {
        
        SVProgressHUD.show()
        SVProgressHUD.setMaximumDismissTimeInterval(2)
        SVProgressHUD.setMinimumDismissTimeInterval(2)
        
        Network.request(.deleteAllNotifications, success: { (model) in
            
            SVProgressHUD.dismiss()
            SVProgressHUD.showSuccess(withStatus: "All notifications were deleted successfully")
            
            self.lastPageIndex = 0
            self.notificationsList.removeAll()
            self.tableView.reloadData()
            
            
        }) { (err) in
            
            SVProgressHUD.dismiss()
            SVProgressHUD.showError(withStatus: err.localizedDescription)
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.notificationsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Notifications") as! NotificationsTableViewCell
        let notif = self.notificationsList[indexPath.row]
        
        let red = UIColor(valueRed: 224, green: 49, blue: 49, alpha: 1)
        let green = UIColor(valueRed: 31, green: 151, blue: 81, alpha: 1)
        
        cell.notificationPic.backgroundColor = notif.color.isRed ? red : green
        cell.notificationPic.image = UIImage(named : "Transaction_Icon-1")
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor(valueRed: 247, green: 251, blue: 255, alpha: 1)
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        cell.notificationContent.attributedText = notif.text
        cell.notificationDate.text = notif.date.format("d MMM yyyy - HH:mm", locale: Locale(identifier: "en_US"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            
            let notif = self.notificationsList[indexPath.row]
            
            SVProgressHUD.show()
            Network.request(.deleteNotification(id: notif.id), success: { (_) in
                
                SVProgressHUD.dismiss()
                
                self.notificationsList.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                self.updateBackground()
                
            }, failure: { (err) in
                
                SVProgressHUD.showError(withStatus: err.localizedDescription)
            })
        }
        
        return [delete]
    }
    
    func updateBackground() -> Void {
        
        self.tableView.indexPathsForVisibleRows?.forEach({ (index) in
            
            if let cell = self.tableView.cellForRow(at: index) {
                
                cell.backgroundColor = index.row % 2 == 0
                    ? UIColor(valueRed: 247, green: 251, blue: 255, alpha: 1)
                    : UIColor.white
            }
        })
    }
}


