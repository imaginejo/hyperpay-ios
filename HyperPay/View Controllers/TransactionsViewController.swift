//
//  TransactionViewController.swift
//  HyperPay
//
//  Created by Mac on 5/23/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import SVProgressHUD

class TransactionsViewController: BaseTransactionsViewController, ChannelSelectorDelegate, SearchFilterViewControllerDelegate {
    
    @IBOutlet weak var channelSelector: ChannelSelector!
    
    @IBOutlet weak var rejectedValueLabel: UILabel!
    @IBOutlet weak var pendingValueLabel: UILabel!
    @IBOutlet weak var acceptedValueLabel: UILabel!
    @IBOutlet weak var amountValueLabel: UILabel!
    
    @IBOutlet weak var statsView: UIView!
    @IBOutlet weak var amountNameLabel: UILabel!
    
    var reportsList = [ReportsList]()
    class ReportsList {
        
        var day:String
        var list = [TransactionReport]()
        
        init(_ day:String) {
            self.day = day
        }
    }
    
    func indexForReportsListOfDay(_ day:String) -> Int? {
        
        return self.reportsList.index { (repList) -> Bool in
            return repList.day == day
        }
    }
    
    var selectedChannel:ResponseModel.User.Channel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dateRange = (Date(), Date())
        self.selectedChannel = Network.authenticatedUser.channels.first
        self.channelSelector.channels = Network.authenticatedUser.channels
        self.channelSelector.selectorDelegate = self
        self.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.channelSelector.resetToSelectedIndex()
    }
    
    override func displaySummary(_ summary: TransactionsResponse.Summary) {
        
        self.amountValueLabel.setMoney(summary.totalValue)
        
        self.acceptedValueLabel.setMoney(summary.positiveValue)
        self.pendingValueLabel.setMoney(summary.nutralValue) // =>
        self.rejectedValueLabel.setMoney(summary.rejectedValue)
    }
    
    override func reportTypeDidChange(_ reportType:ReportType){
        
        self.statsView.isHidden = self.selectedReportType == .reconciliation
        self.amountNameLabel.text = self.selectedReportType == .reconciliation ? "total-reconcile".local : "balance-income".local
        self.reloadData()
    }
    
    func channelSelector(_ selector: ChannelSelector, didSelectChannel channel: ResponseModel.User.Channel, atIndex index: Int) {
        
        self.selectedChannel = channel
        self.reloadData()
    }
    
    @IBAction func openSearchFilters(_ sender: Any) {
        
        if let searchFilterVC = self.storyboard?.instantiateViewController(withIdentifier: "searchFilterVC") as? SearchFilterViewController {
            
            searchFilterVC.currentDateRange = self.dateRange
            searchFilterVC.delegate = self
            self.navigationController?.present(searchFilterVC, animated: true, completion: nil)
        }
    }
    
    var searchSelection:[SearchFilter]!
    func searchFilterVC(_ searchFilterVC: SearchFilterViewController, didDismissWithSelection filters: [SearchFilter]) {
        
        self.dateRange = searchFilterVC.currentDateRange
        
        if filters.isEmpty == false {
            
            self.searchSelection = filters
            self.performSegue(withIdentifier: "to_search_results", sender: nil)
            
        } else if searchFilterVC.currentDateRange != nil {
            
            self.reloadData()
        }
    }
    
    override func buildTransactionsRequest(lastIndex: Int) -> HyperPay {
        
        var start:Date?, end:Date?
        if let range = self.dateRange {
            let (_st, _en) = range
            start = _st
            end = _en
        }
        
        return .transactions(
            reportType: self.selectedReportType.serverValue,
            channel: self.selectedChannel.id,
            fromDate: start, toDate: end,
            lastTransactionId: lastIndex,
            pageSize: 5,
            sort: self.selectedSortType.serverValue)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let searchVC = segue.destination as? SearchResultsViewController {
            
            searchVC.dateRange = self.dateRange
            searchVC.currentChannel = self.selectedChannel
            searchVC.currentSelection = self.searchSelection
        }
    }
}


