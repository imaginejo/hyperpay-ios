//
//  FiltersViewController.swift
//  HyperControls
//
//  Created by Suhayb Ahmad on 6/21/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

protocol SearchFilterViewControllerDelegate:class {
    
    func searchFilterVC(_ searchFilterVC:SearchFilterViewController, didDismissWithSelection filters:[SearchFilter]) -> Void
}

class SearchFilterViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, FilterCellDelegate, SelectingViewCellDelegate, DateRangeSelectorDelegate {
    
    @IBOutlet weak var dateRangeSelector: DateRangeSelector!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var addFilterButton: UIButton!
    
    var currentDateRange:(Date, Date)?
    
    var filtersList = [SearchFilter]()
    @IBOutlet weak var tableView:UITableView!
    
    weak var delegate:SearchFilterViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.headerView.applyDarkShadow(opacity: 0.13, offsetY: 1, radius: 3)
        
        self.addFilterButton.layer.borderColor = #colorLiteral(red: 0.514334761, green: 0.6655734413, blue: 0.7977255126, alpha: 1)
        self.addFilterButton.layer.borderWidth = 1
        self.addFilterButton.layer.cornerRadius = 4
        
        self.dateRangeSelector.delegate = self
        
        if let range = self.currentDateRange {
            let (start, end) = range
            self.dateRangeSelector.setRange(start: start, end: end)
        }
        
        self.fillEdgeSpace(withColor: #colorLiteral(red: 1, green: 0.5764705882, blue: 0, alpha: 1), edge: .bottom)
        
        let tapReco = UITapGestureRecognizer(target: self, action: #selector( viewWasTapped(_:) ) )
        tapReco.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tapReco)
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notif) in
            
            if let frame = notif.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
                self.bottomConst.constant = frame.height * -1
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillHide, object: nil, queue: nil) { _ in
            
            self.bottomConst.constant = 0
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func dateRangeSelector(_ selector: DateRangeSelector, didChangeRange range: (Date, Date)) {
        
        self.currentDateRange = range
    }
    
    func dateRangeSelectorDidClear(_ selector: DateRangeSelector) {
        
        self.currentDateRange = nil
    }
    
    @IBAction func createNewFilter(_ sender: Any) {
        
        let items = Network.appConfig.searchFields.filter({ (field) -> Bool in
            
            switch field.type {
            case .multiSelect, .select:
                
                return self.filtersList.contains(where: { (fltr) -> Bool in
                    return fltr.field.name == field.name
                }) == false
                
            default:
                return true
            }
            
        }).map { (field) -> FieldItem in
            
            return FieldItem(field)
        }
        
        ItemPickerViewController.launch(forList: items, withItemName: "field".local, withDelegate: self)
    }
    
    @objc func viewWasTapped(_ sender:Any) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func dismissView(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return max(self.filtersList.count * 2 - 1, 0) //+ 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var _cell:UITableViewCell
        
        if indexPath.row % 2 == 0 {
            
            let index = indexPath.row / 2
            let filter = self.filtersList[index]
            let cellID:String
            
            switch filter.field.type {
            case .multiSelect:
                cellID = "filterCell_multiSelect"
            case .number:
                cellID = "filterCell_number"
            case .text:
                cellID = "filterCell_text"
            case .nameValue:
                cellID = "filterCell_nameValue"
            case .select:
                cellID = "filterCell_select"
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! FilterViewCell
            
            cell.delegate = self
            cell.popForFilter( self.filtersList[index] )
            
            _cell = cell
        
        } else {
            
            let index = Int( ceil( Double(indexPath.row) / 2 ) )
            let filter = self.filtersList[index]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "conditionCell", for: indexPath) as! ConditionViewCell
            cell.currentFilter = filter
            cell.optionsControl.selectedSegmentIndex = filter.condition == .and ? 0 : 1
            
            _cell = cell
        }
        
        return _cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row % 2 == 0 {
            return self.filtersList[ indexPath.row / 2 ].displayHeight
        } else {
            return 50
        }
    }
    
    func filterCell(_ cell: UITableViewCell, didRequireDeletionFor filter: SearchFilter) {
        
        self.filtersList = self.filtersList.filter({ (_filter) -> Bool in
            return _filter != filter
        })
        
        if let indexPath = self.tableView.indexPath(for: cell) {
            
            var indecies = [indexPath]
            
            if indexPath.row > 0 {
                
                indecies.append(IndexPath(row: indexPath.row - 1, section: indexPath.section))
                
            } else if indexPath.row == 0 && self.filtersList.count > 0 {
                
                indecies.append(IndexPath(row: indexPath.row + 1, section: indexPath.section))
            }
            
            self.tableView.deleteRows(at: indecies, with: .automatic)
        }
    }
    
    func selectingCellDidRequestFieldFilter(_ filter: SearchFilter) {
        
        self.filtersList.append(filter)
        
        var indecies = [IndexPath]()
        if self.filtersList.count == 1 {
            
            indecies.append( IndexPath(row: 0, section: 0) )
            
        } else {
            
            let ci = self.filtersList.count * 2 - 3
            let fi = self.filtersList.count * 2 - 2
            
            indecies.append( IndexPath(row: ci, section: 0) )
            indecies.append( IndexPath(row: fi, section: 0) )
        }
        
        self.tableView.insertRows(at: indecies, with: .automatic)
        self.tableView.scrollToRow(at: indecies.first!, at: .top, animated: true)
    }
    
    func filterCellRequireUpdate(_ cell: UITableViewCell) {
        
        if let index = self.tableView.indexPath(for: cell){
            self.tableView.reloadRows(at: [index], with: .automatic)
        }
    }
    
    @IBAction func applyFilters(_ sender: Any) {
        
        let isNotValid = self.filtersList.contains { (filter) -> Bool in
            return filter.value.isValid == false
        }
        
        if isNotValid {
            showErrorMessage("error-filter-filling".local)
            return
        }
        
        print("\n\n-")
        self.filtersList.forEach { (filter) in
            print(filter.condition)
            print("   \(filter.field.name) = \(filter.value)")
        }
        print("\n")
        
        self.dismiss(animated: true) {
            
            self.delegate?.searchFilterVC(self, didDismissWithSelection: self.filtersList)
        }
    }
}

extension SearchFilterViewController:ItemPickerViewControllerDelegate {
    
    func itemPicker(_ itemSelector: ItemPickerViewController, didSelectItem item: ItemPickerViewController.Item) {
        
        let filter = SearchFilter( (item as! FieldItem).field )
        self.selectingCellDidRequestFieldFilter(filter)
    }
    
    func itemPickerDidClearSelection(_ itemSelector: ItemPickerViewController) {}
}
