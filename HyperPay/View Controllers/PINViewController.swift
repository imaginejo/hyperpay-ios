//
//  PINViewController.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 7/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import SVProgressHUD

class PINViewController: ResetPasswordViewController {

    @IBOutlet var codeDigitFields: [UITextField]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 0 ..< codeDigitFields.count {
            
            let textField = codeDigitFields[i]
            textField.delegate = self
            textField.keyboardType = .numbersAndPunctuation
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.spellCheckingType = .no
            textField.returnKeyType = (i + 1) == codeDigitFields.count ? .done : .next
            textField.addTarget(self, action: #selector( textFieldEdited(_:) ), for: .editingChanged )
        }
    }
    
    func contructConfirmationCode() -> String? {
        
        var code = ""
        for digitField in self.codeDigitFields {
            code += digitField.text ?? ""
        }
        
        return code.count == self.codeDigitFields.count ? code : nil
    }
    
    var phoneNumber:String!
    var currentCode:String!
    @IBAction func submitForm(_ sender: Any) {
        
        if let code = contructConfirmationCode()?.replacingArabicNumbers() {
            
            SVProgressHUD.setDefaultStyle(.light)
            SVProgressHUD.setDefaultMaskType(.clear)
            SVProgressHUD.show()
            
            Network.request(.validatePin(username: self.phoneNumber, pin: code), success: { (model) in
                
                SVProgressHUD.dismiss()
                
                if let res = model as? Bool, res {
                    
                    self.currentCode = code
                    self.performSegue(withIdentifier: "pin_to_newpassword", sender: nil)
                    
                } else {
                    
                    showErrorMessage("error-invalid-pin".local)
                }
                
            }) { (err) in
                
                SVProgressHUD.dismiss()
                showErrorMessage("error-validating-pin".local)
                print(err.localizedDescription)
            }
            
        } else {
            
            showErrorMessage("error-fill-fields".local)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "pin_to_newpassword" {
            
            let newVC = segue.destination as! NewPasswordViewController
            
            newVC.phoneNumber = self.phoneNumber
            newVC.pinCode = self.currentCode
        }
    }
}

extension PINViewController:UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.focusToNextField(textField, withNextValue: nil)
        return true
    }
    
    func focusToNextField(_ textField:UITextField, withNextValue value: String?) -> Void {
        
        if let index = codeDigitFields.index(of: textField) {
            
            if index < (codeDigitFields.count - 1) {
                
                let nextField = codeDigitFields[index + 1]
                nextField.text = value ?? nextField.text
                nextField.becomeFirstResponder()
                
            } else {
                
                textField.resignFirstResponder()
            }
        }
    }
    
    func focusToPrevField(_ textField:UITextField) -> Void {
        
        if let index = codeDigitFields.index(of: textField) {
            
            if index > 0 {
                
                let prevField = codeDigitFields[index - 1]
                prevField.becomeFirstResponder()
                
            } else {
                
                textField.resignFirstResponder()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let char = string.cString(using: .utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (string.isEmpty && isBackSpace == -92) {
            
            textField.text = nil
            self.focusToPrevField(textField)
            return false
            
        } else if string.count > 1, let ci = self.codeDigitFields.index(of: textField) {
            
            for i in 0 ..< string.count {
                
                if i < self.codeDigitFields.count - ci {
                    
                    let ind = string.index(string.startIndex, offsetBy: i)
                    self.codeDigitFields[ci + i].text = String(string[ind])
                }
            }
            
            return false
        }
        
        return true
    }
    
    @objc func textFieldEdited(_ sender: Any) {
        
        let textField = sender as! UITextField
        
        if let str = textField.text, str.count == 2 {
            
            textField.text = String( str[str.startIndex] )
            
            let nextChar = String( str.suffix(from: str.index(str.startIndex, offsetBy: 1)) )
            self.focusToNextField(textField, withNextValue: nextChar)
        }
    }
}
