//
//  BaseTransactionsViewController.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 7/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

enum ReportType:String {
    case general = "report-general"
    case reconciliation = "report-reconciliation"
    case risk = "report-risk"
    
    var serverValue:String {
        switch self {
        case .general:
            return "General"
        case .reconciliation:
            return "Reconciliation"
        case .risk:
            return "Risk"
        }
    }
    
    var label:String {
        return self.rawValue.local
    }
}

enum SortType:String {
    case amountLowToHigh = "amount-sort-low-high"
    case amountHighToLow = "amount-sort-high-low"
    case dateNewToOld = "date-sort-new-old"
    case dateOldToNew = "date-sort-old-new"
    
    var serverValue:String {
        
        switch self {
        case .amountLowToHigh:
            return "Amount_asc"
        case .amountHighToLow:
            return "Amount_desc"
        case .dateNewToOld:
            return "date_desc"
        case .dateOldToNew:
            return "date_asc"
        }
    }
    
    var arrow:UIImage {
        switch self{
        case .amountLowToHigh, .dateNewToOld:
            return #imageLiteral(resourceName: "drop_down_list")
        case .amountHighToLow, .dateOldToNew:
            return #imageLiteral(resourceName: "drop_up_list")
        }
    }
    
    var label:String {
        return self.rawValue.local
    }
}

class BaseTransactionsViewController: UIViewController, TransactionsViewModelDelegate {
    
    static let cellIdentifier:String = "TransactionCell"
    
    var viewModel = TransactionsViewModel()
    var dateRange:(Date, Date)?
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var reportTypeButton: UIButton!
    @IBOutlet weak var sortTypeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBAction func showSideMenu(_ sender: Any) {
        
        NotificationCenter.default.post(name: NotificationNames.ShowSideMenu, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel.delegate = self
        self.reportTypeButton.setTitle(self.selectedReportType.label, for: .normal)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 180
        
        let nib = UINib(nibName: "TransactionsTableViewCell", bundle: Bundle.main)
        self.tableView.register(nib, forCellReuseIdentifier: BaseTransactionsViewController.cellIdentifier)
        
        if #available(iOS 11, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    private class ReportAlertAction: UIAlertAction {
        var type:ReportType = .general
    }
    
    private class SortAlertAction: UIAlertAction {
        var type:SortType = .amountLowToHigh
    }
    
    var selectedReportType:ReportType = .general
    func reportTypeDidChange(_ reportType:ReportType){
        self.reloadData()
    }
    
    var selectedSortType:SortType = .amountLowToHigh
    func sortTypeDidChange(_ sortType:SortType){
        self.reloadData()
    }
    
    @IBAction func changeReportType(_ sender: Any) {
        
        let button = sender as! UIView
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let handler:((UIAlertAction) -> Void) = { (action) in
            
            let type = (action as! ReportAlertAction).type
            
            self.selectedReportType = type
            self.reportTypeButton.setTitle(type.label, for: .normal)
            self.reportTypeDidChange(type)
        }
        
        let option1 = ReportAlertAction(title: ReportType.general.label, style: .default, handler: handler)
        let option2 = ReportAlertAction(title: ReportType.reconciliation.label, style: .default, handler: handler)
        let option3 = ReportAlertAction(title: ReportType.risk.label, style: .default, handler: handler)
        
        option1.type = ReportType.general
        option2.type = ReportType.reconciliation
        option3.type = ReportType.risk
        
        switch self.selectedReportType {
        case .general:
            option1.setValue(#imageLiteral(resourceName: "check_mark"), forKey: "image")
        case .reconciliation:
            option2.setValue(#imageLiteral(resourceName: "check_mark"), forKey: "image")
        case .risk:
            option3.setValue(#imageLiteral(resourceName: "check_mark"), forKey: "image")
        }
        
        alert.addAction(option1)
        alert.addAction(option2)
        alert.addAction(option3)
        alert.addAction(UIAlertAction(title: "cancel".local, style: .cancel, handler: nil))
        
        if let popoverVC = alert.popoverPresentationController {
            popoverVC.sourceRect = button.bounds
            popoverVC.sourceView = button
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func changeSortingType(_ sender: Any) {
        
        let button = sender as! UIView
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let handler:((UIAlertAction) -> Void) = { (action) in
            
            let type = (action as! SortAlertAction).type
            
            self.selectedSortType = type
            self.sortTypeButton.setTitle(type.label.uppercased(), for: .normal)
            self.sortTypeButton.setImage(type.arrow, for: .normal)
            self.sortTypeDidChange(type)
        }
        
        let option1 = SortAlertAction(title: SortType.amountLowToHigh.label, style: .default, handler: handler)
        let option2 = SortAlertAction(title: SortType.amountHighToLow.label, style: .default, handler: handler)
        let option3 = SortAlertAction(title: SortType.dateNewToOld.label, style: .default, handler: handler)
        let option4 = SortAlertAction(title: SortType.dateOldToNew.label, style: .default, handler: handler)
        
        option1.type = SortType.amountLowToHigh
        option2.type = SortType.amountHighToLow
        option3.type = SortType.dateNewToOld
        option4.type = SortType.dateOldToNew
        
        switch self.selectedSortType {
        case .amountLowToHigh:
            option1.setValue(#imageLiteral(resourceName: "check_mark"), forKey: "image")
        case .amountHighToLow:
            option2.setValue(#imageLiteral(resourceName: "check_mark"), forKey: "image")
        case .dateNewToOld:
            option3.setValue(#imageLiteral(resourceName: "check_mark"), forKey: "image")
        case .dateOldToNew:
            option4.setValue(#imageLiteral(resourceName: "check_mark"), forKey: "image")
        }
        
        alert.addAction(option1)
        alert.addAction(option2)
        alert.addAction(option3)
        alert.addAction(option4)
        alert.addAction(UIAlertAction(title: "cancel".local, style: .cancel, handler: nil))
        
        if let popoverVC = alert.popoverPresentationController {
            popoverVC.sourceRect = button.bounds
            popoverVC.sourceView = button
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func reloadData() -> Void {
        
        self.viewModel.clear()
        self.tableView.reloadData()
        self.viewModel.reloadData()
    }
    
    func displaySummary(_ summary: TransactionsResponse.Summary) {}
    func buildTransactionsRequest(lastIndex: Int) -> HyperPay {
        return .none
    }
    
    func onFetchWillStart() {
        self.indicator.startAnimating()
    }
    
    func onFetchFinished() {
        self.indicator.stopAnimating()
    }
    
    func onFetchCompleted(withSections sections: IndexSet, andRows rows: [IndexPath]) {
        
        self.tableView.beginUpdates()
        if sections.isEmpty == false {
            self.tableView.insertSections(sections, with: .none)
        }
        
        self.tableView.insertRows(at: rows, with: .none)
        self.tableView.endUpdates()
    }
    
    func onFetchFailed(with reason: String) {
        
        showErrorMessage(reason)
    }
}

extension BaseTransactionsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.viewModel.sectionsCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.viewModel.transactionsCountForSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellID = BaseTransactionsViewController.cellIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! TransactionsTableViewCell
        let report = self.viewModel.transactionsForIndex(indexPath)
        
        cell.displayReport(report, asType: self.selectedReportType)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.viewModel.isItemTheLast(indexPath) && self.viewModel.hasMore {
            self.viewModel.loadTransactionsData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return self.viewModel.headerViewForSection(section)
    }
    
    func tableView(_ tableView: UITableView, didEndDisplayingHeaderView view: UIView, forSection section: Int) {
        
        (view as! DayHeaderView).isDisplayed = false
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        (view as! DayHeaderView).isDisplayed = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.viewModel.refreshHeaderViews(self.tableView)
    }
}

