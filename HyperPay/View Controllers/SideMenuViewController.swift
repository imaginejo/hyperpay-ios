//
//  SideMenuViewController.swift
//  HyperPay
//
//  Created by Mac on 5/29/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var logoView: UIImageView!
    
    weak var targetNavigation:UINavigationController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.logoView.layer.borderWidth = 0.5
        self.logoView.layer.cornerRadius = 3
        self.logoView.layer.borderColor = UIColor(red: 233/255, green: 236/255, blue: 239/255, alpha: 1).cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let df = DateFormatter()
        df.dateFormat = "EEEE - dd MMM yyyy"
        
        self.dateLabel.text = df.string(from: Date()).uppercased().replacingArabicNumbers()
        self.nameLabel.text = Network.authenticatedUser?.fullName ?? "Some User"
        self.logoView.image = (Network
            .authenticatedUser?.image ?? #imageLiteral(resourceName: "company_logo"))
            .af_imageAspectScaled(toFit: self.logoView.frame.insetBy(dx: 5, dy: 5).size)
    }
    
    @IBAction func showPage(_ sender: Any) {
        let button = sender as! UIButton
        
        self.dismiss(animated: true, completion: {
            
            switch button.tag {
            case 0:
                if let accountSettingsVC = self.storyboard?.instantiateViewController(withIdentifier: "Notifications") {
                    self.targetNavigation?.pushViewController(accountSettingsVC, animated: true)
                }
                
            case 1:
                if let notificationsVC = self.storyboard?.instantiateViewController(withIdentifier: "settingsVC") {
                    
                    self.targetNavigation?.pushViewController(notificationsVC, animated: true)
                }
                
            case 2:
                if let contactUsVC = self.storyboard?.instantiateViewController(withIdentifier: "contactUsVC") {
                    
                    self.targetNavigation?.pushViewController(contactUsVC, animated: true)
                }
                
            case 3:
                
                Network.logoutUser()
                self.targetNavigation?.popToRootViewController(animated: true)
       
            default:
                break
            }
        })
    }

}
