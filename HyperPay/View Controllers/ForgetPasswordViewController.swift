//
//  forgetPasswordViewController.swift
//  HyperPay
//
//  Created by Mac on 5/28/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import SideMenu
import SVProgressHUD
import Moya

class ResetPasswordViewController: BaseViewController {
    
    @IBOutlet var roundViews: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isSidMenuSwipeEnabled = false
        self.roundViews.forEach { (view) in
            view.layer.cornerRadius = 4
            view.applyDarkShadow(opacity: 0.4, offsetY: 1, radius: 2)
        }
    }
    
    @IBAction func goback(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func viewWasTapped(_ sender: Any) {
        
        self.view.endEditing(true)
    }
}


class ForgetPasswordViewController: ResetPasswordViewController {
    
    @IBOutlet weak var phoneField: PhoneNumberField!
    
    var currentPhone:String!
    
    @IBAction func submitForm(_ sender: Any) {
        
        guard let phone = self.phoneField.phoneNumber else {
            showErrorMessage("error-phone".local)
            return
        }
        
        self.currentPhone = phone
        
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.setMaximumDismissTimeInterval(2.5)
        SVProgressHUD.show()
        
        Network.request(.forgotPassword(username: phone), success: { (model) in
           
            SVProgressHUD.dismiss()
            SVProgressHUD.showSuccess(withStatus: "Please make sure to check your SMS messages for next steps")
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 4.2) {
                self.performSegue(withIdentifier: "forgot_to_pin", sender: nil)
            }
            
        }) { (err) in
            
            SVProgressHUD.dismiss()
            SVProgressHUD.showError(withStatus: err.localizedDescription)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        (segue.destination as! PINViewController).phoneNumber = self.currentPhone
    }
}
