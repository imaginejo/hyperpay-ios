//
//  TransactionsNavigationControllerViewController.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 10/7/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import SideMenu

class TransactionsNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(forName: NotificationNames.ShowTransactionDetails, object: nil, queue: nil) { (notif) in
            
            if let uniqueId = notif.userInfo?["UniqueId"] as? String {
                self.openTransactionDetails(uniqueId)
            }
        }
        
        if let uniqueId = AppDelegate.toBeOpenedTransactionUniqueId {
            self.openTransactionDetails(uniqueId)
        }
    }
    
    func dismissAllModals(completion:@escaping () -> Void) -> Void {
        
        if let rootNav = self.tabBarController?.navigationController {
            
            var presentedVC = rootNav.presentedViewController
            while let _presentedVC = presentedVC?.presentedViewController {
                presentedVC = _presentedVC
            }
            
            guard let firstPresented = presentedVC else {
                completion()
                return
            }
            
            let group = DispatchGroup()
            
            group.enter()
            firstPresented.dismiss(animated: true, completion: { group.leave() })
            
            while let presented = presentedVC?.presentingViewController, presented != rootNav {
                
                group.enter()
                presented.dismiss(animated: true) { group.leave() }
                presentedVC = presented
            }
            
            group.notify(queue: DispatchQueue.main) {
                completion()
            }
            
        } else {
            
            completion()
        }
    }
    
    func openTransactionDetails(_ uniqueId:String) -> Void {
        
        self.dismissAllModals {
            
            AppDelegate.toBeOpenedTransactionUniqueId = nil
            
            if let lastVC = self.viewControllers.last as? TransactionDetailsViewController {
                
                lastVC.reloadForTransaction(uniqueId: uniqueId)
                
            }else if let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "transactionDetailsVC") as? TransactionDetailsViewController {
                
                detailsVC.transactionUniqueId = uniqueId
                self.pushViewController(detailsVC, animated: false)
            }
        }
    }
}
