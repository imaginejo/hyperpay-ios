//
//  NewPasswordViewController.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 7/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewPasswordViewController: ResetPasswordViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.passwordField.returnKeyType = .next
        self.passwordField.delegate = self
        
        self.confirmField.returnKeyType = .send
        self.confirmField.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.passwordField {
            
            self.confirmField.becomeFirstResponder()
            
        } else {
            
            self.confirmField.resignFirstResponder()
            self.submitForm(NSNull())
        }
        
        return true
    }
    
    var phoneNumber:String!
    var pinCode:String!
    
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmField: UITextField!
    
    @IBAction func submitForm(_ sender: Any) {
        
        guard
            let pass = self.passwordField.text, pass.isEmpty == false,
            let confirm = self.confirmField.text, confirm.isEmpty == false,
            pass == confirm else {
            showErrorMessage("error-fill-pwd-confirm".local)
            return
        }
        
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.setMaximumDismissTimeInterval(1.0)
        SVProgressHUD.show()
        
        Network.request(.resetPassword(username: self.phoneNumber,
                                       password: pass,
                                       pin: self.pinCode), success:
            { (model) in
                
            
                SVProgressHUD.dismiss()
                SVProgressHUD.showSuccess(withStatus: "status-pwd-reset".local)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.navigationController?.popToRootViewController(animated: true)
                })
                
        }) { (err) in
            
            SVProgressHUD.dismiss()
            SVProgressHUD.showError(withStatus: err.localizedDescription)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.navigationController?.popToRootViewController(animated: true)
            })
        }
    }
}
