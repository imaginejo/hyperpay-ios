//
//  SignUpSuccessViewController.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 9/27/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class SignUpSuccessViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backButton.layer.cornerRadius = 4
        self.backButton.applyDarkShadow(opacity: 0.4, offsetY: 1, radius: 2)
    }
    
    @IBAction func goBackToStart(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
}
