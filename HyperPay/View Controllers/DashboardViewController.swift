//
//  DashboardViewController.swift
//  HyperPay
//
//  Created by Mac on 5/22/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var channelDisplays = [DashboardChannelDisplay]()
    
    @IBOutlet var roundViews: [UIView]!
    @IBOutlet weak var infoContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func showSideMenu(_ sender: Any) {
        
        NotificationCenter.default.post(name: NotificationNames.ShowSideMenu, object: nil)
    }
    
    @IBOutlet weak var rejectedLabel: UILabel!
    @IBOutlet weak var neutralLabel: UILabel!
    @IBOutlet weak var positiveLabel: UILabel!
    @IBOutlet weak var negativeLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var projectLabel: UILabel!
    @IBOutlet weak var timeRangeLabel: UILabel!
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = self.refreshControl
        } else {
            self.tableView.addSubview(self.refreshControl)
        }
        
        self.refreshControl.tintColor = UIColor.white
        self.refreshControl.addTarget(self, action: #selector( refreshRequested ), for: .valueChanged)
        
        self.logoView.image = (Network
            .authenticatedUser?.image ?? #imageLiteral(resourceName: "company_logo"))
            .af_imageAspectScaled(toFit: self.logoView.frame.insetBy(dx: 5, dy: 5).size)
        
        self.logoView.layer.cornerRadius = 4
        self.logoView.layer.borderColor = UIColor(white: 0.90, alpha: 1).cgColor
        self.logoView.layer.borderWidth = 1
        self.logoView.layer.masksToBounds = true
        
        self.infoContainer.layer.masksToBounds = true
        self.infoContainer.layer.cornerRadius = 5
        
        let df = DateFormatter()
        df.dateFormat = "EEEE - dd MMM yyyy"
        
        self.dateLabel.text = df.string(from: Date()).uppercased().replacingArabicNumbers()
        self.projectLabel.text = Network.authenticatedUser?.companyName
        
        self.amountLabel.text = "⇋ "
        self.negativeLabel.text = ""
        self.positiveLabel.text = ""
        self.neutralLabel.text = ""
        self.rejectedLabel.text = ""
        
        self.timeRangeLabel.text = df.string(from: Date()).replacingArabicNumbers()
        
        self.roundViews.forEach { (view) in
            view.layer.cornerRadius = 5
            view.applyDarkShadow(opacity: 0.4, offsetY: 1, radius: 2)
        }
        
        let font = self.customerLabel.font!
        let name = Network.authenticatedUser?.fullName ?? "Some User"
        let msg = "\("welcome-back".local), \(name)"
        
        let welcomeMsg = NSMutableAttributedString(string: msg)
        welcomeMsg.addAttribute(
            .font,
            value: UIFont(name: Locale.current.isRightToLeft ? "DINNextLTW23-Bold" : "SeroPro-Medium", size: font.pointSize)!,
            range: (msg as NSString).range(of: name))
        
        self.customerLabel.attributedText = welcomeMsg
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 200
        self.reloadDashboardData()
    }
    
    @objc func refreshRequested() -> Void {
        
        self.reloadDashboardData {
            
            self.refreshControl.endRefreshing()
        }
    }
    
    func reloadDashboardData(completion:( () -> Void )? = nil) -> Void {
        
        Network.request(.summary(date: Date()), success: { (loadedModel) in
            
            completion?()
            
            if let summaryData = loadedModel as? ResponseModel.Summary {
                
                let total = summaryData.totalGroup
                self.amountLabel.text = "⇋ \( total.totalCount )"
                
                self.negativeLabel.text = "\( total.negative.count )"
                self.positiveLabel.text = "\( total.positive.count )"
                self.neutralLabel.text = "\( total.neutral.count )"
                self.rejectedLabel.text = "\( total.rejected.count )"
                
                self.channelDisplays = Network.authenticatedUser.channels
                    .filter({ (channel) -> Bool in
                        
                        return summaryData.channelGroups.contains(where: { $0.id == channel.id })
                        
                    }).map({ (channel) -> DashboardChannelDisplay in
                    
                    let summary = summaryData.channelGroups.first(where: { (group) -> Bool in
                        return group.id == channel.id
                    })
                    
                    return DashboardChannelDisplay(info: channel, summary: summary!)
                })
                
                self.tableView.reloadData()
            }
            
        }) { (err) in
            
            completion?()
            showErrorMessage(err.localizedDescription)
        }
    }
    
}

extension DashboardViewController: UITableViewDelegate, UITableViewDataSource, DashboardTableViewCellDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.channelDisplays.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "dashboardChannelCell", for: indexPath) as! DashboardTableViewCell
        
        cell.delegate = self
        cell.show( self.channelDisplays[indexPath.row] )
        
        return cell
    }
    
    func dashboardCellDidTapHeader(_ cell: DashboardTableViewCell) {
        
        let isExpanded = cell.currentDisplay.state == .expanded
        if let cindex = self.tableView.indexPath(for: cell) {
            
            self.channelDisplays.forEach { (disp) in disp.state = .collapsed }
            self.tableView.visibleCells.forEach { (_c) in
                
                if let index = self.tableView.indexPath(for: _c),
                    let gcell = _c as? DashboardTableViewCell {
                    
                    if isExpanded {
                         gcell.currentDisplay.state = .collapsed
                    } else {
                         gcell.currentDisplay.state = index == cindex ? .expanded : .collapsed
                    }
                    
                    gcell.adapt(gcell.currentDisplay)
                }
            }
        }
        
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            
            if let sindex = self.tableView.indexPath(for: cell) {
                self.tableView.scrollToRow(at: sindex, at: .top, animated: true)
            }
        }
        
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        
        CATransaction.commit()
    }
}

class DashboardChannelDisplay {
    
    enum State {
        case expanded
        case collapsed
        
        var carretImage: UIImage {
            switch self {
            case .collapsed:
                return #imageLiteral(resourceName: "drop_down_list")
            case .expanded:
                return #imageLiteral(resourceName: "drop_up_list")
            }
        }
        
        var height: CGFloat {
            switch self {
            case .collapsed:
                return 130
            case .expanded:
                return 480
            }
        }
        
        var hidden: Bool {
            switch self {
            case .collapsed:
                return true
            case .expanded:
                return false
            }
        }
    }
    
    var state:State = .collapsed
    var info:ResponseModel.User.Channel
    var summary:ResponseModel.Summary.StatsGroup
    
    init(info:ResponseModel.User.Channel, summary:ResponseModel.Summary.StatsGroup) {
        self.info = info
        self.summary = summary
    }
}
