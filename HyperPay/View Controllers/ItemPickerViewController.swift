//
//  ItemPickerViewController.swift
//  Shakawekom
//
//  Created by AhmeDroid on 4/11/17.
//  Copyright © 2017 Imagine Technologies. All rights reserved.
//

import UIKit

protocol ItemPickerViewControllerDelegate: class {
    func itemPicker(_ itemSelector: ItemPickerViewController, didSelectItem item: ItemPickerViewController.Item)
    func itemPickerDidClearSelection(_ itemSelector: ItemPickerViewController )
}

class ItemPickerViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    class Item:Equatable {
        
        static func == (lhs: ItemPickerViewController.Item, rhs: ItemPickerViewController.Item) -> Bool {
            return lhs.id == rhs.id
        }
        
        var id = UUID().uuidString
        var title:String {
            return "N/A"
        }
    }
    
    static func launch(forList list: [Item], withItemName itemName: String? = nil, withInitialItem initialItem:Item? = nil, withDelegate delegate: ItemPickerViewControllerDelegate) -> Void {
        
        if let viewC = UIApplication.topViewController(),
            let itemPickerVC = viewC.storyboard?.instantiateViewController(withIdentifier: "itemPickerVC") as? ItemPickerViewController  {
            
            itemPickerVC.itemName = itemName ?? "Item"
            itemPickerVC.itemsList = list
            itemPickerVC.selectedItem = initialItem
            itemPickerVC.modalTransitionStyle = .crossDissolve
            itemPickerVC.modalPresentationStyle = .overCurrentContext
            itemPickerVC.delegate =  delegate
            
            viewC.present(itemPickerVC, animated: true, completion: nil)
        } else {
            
            print("Can't launch item picker on non-UIViewController objects")
        }
    }
    
    private var itemsList:[Item]!
    private var selectedItem:Item?
    var itemName:String?
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var selectionLabel: UILabel!
    
    var delegate:ItemPickerViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.numberOfTapsRequired = 1
        
        self.view.addGestureRecognizer(tapRecognizer)
        tapRecognizer.addTarget(self, action: #selector( viewWasTapped ))
        
        if let item = self.selectedItem,
            let index = self.itemsList.index(of: item) {
            
            self.pickerView.selectRow(index, inComponent: 0, animated: true)
        }
        
        self.selectionLabel.text = "\("select".local) \(self.itemName ?? "item".local)"
        
        if self.selectedItem == nil {
            self.selectedItem = self.itemsList[0]
        }
    }
    
    @objc func viewWasTapped() -> Void {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearSelection(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
            self.delegate?.itemPickerDidClearSelection(self)
            self.delegate = nil
        }
    }
    
    @IBAction func finishSelection(_ sender: Any) {
        
        self.dismiss(animated: true) { 
            
            if let item = self.selectedItem {
                self.delegate?.itemPicker(self, didSelectItem: item)
                self.delegate = nil
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.itemsList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return self.itemsList[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.selectedItem = self.itemsList[row]
    }
}
