//
//  ItemSelectorViewController.swift
//  Shakawekom
//
//  Created by AhmeDroid on 4/11/17.
//  Copyright © 2017 Imagine Technologies. All rights reserved.
//

import UIKit

protocol ItemSelectorViewControllerDelegate:class {
    func itemSelectorDidSelectItem(_ item: ItemSelectorViewController.Item)
    func itemSelectorDidClearSelection(_ itemSelector:ItemSelectorViewController )
}

class ItemSelectorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    static func launch(forList list:[Item], withTitle title: String, withDelegate delegate:ItemSelectorViewControllerDelegate) -> Void {
        
        if let viewC = UIApplication.topViewController(),
            let itemSelector = viewC.storyboard?.instantiateViewController(withIdentifier: "itemSelectorVC") as? ItemSelectorViewController {
            
            itemSelector.delegate = delegate
            itemSelector.selectionTitle = title
            itemSelector.itemsList = list
            
            viewC.present(itemSelector, animated: true, completion: nil)
        } else {
            
            print("Can't launch item selectors on non-UIViewController objects")
        }
    }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var barContainer: UIView!
    
    @IBOutlet weak var barContainerHeight: NSLayoutConstraint!
    var delegate:ItemSelectorViewControllerDelegate?
    var itemsList:[Item]!
    var selectionTitle:String!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        
        let searchBar = self.searchController.searchBar
        searchBar.sizeToFit()
        
        self.barContainerHeight.constant = searchBar.frame.height
        self.barContainer.addSubview(searchBar)
        
        self.definesPresentationContext = true
        self.headerTitle.text = selectionTitle
    }
    
    var searchResults:[Item]?
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
       
        self.searchResults = itemsList.filter { item in
            return item.title.lowercased().contains(searchText.lowercased())
        }
        
        tableView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    @IBAction func clearSelection(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
            self.delegate?.itemSelectorDidClearSelection(self)
        }
    }
    
    @IBAction func closeView(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let text = self.searchController.searchBar.text,
            text.isEmpty == false && searchController.isActive {
            
            return self.searchResults?.count ?? 0
        }
        
        return self.itemsList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = self.itemForIndexPath(indexPath)
        self.searchController.isActive = false
        
        self.dismiss(animated: true, completion: {
            self.delegate?.itemSelectorDidSelectItem(item)
        })
    }
    
    func itemForIndexPath(_ indexPath:IndexPath) -> Item {
        
        let item: Item
        if let text = self.searchController.searchBar.text,
            text.isEmpty == false && searchController.isActive {
            
            item = (self.searchResults?[indexPath.row])!
            
        } else {
            
            item = itemsList[indexPath.row]
        }
        
        return item
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath)
        let item = itemForIndexPath(indexPath)
        
        cell.textLabel?.text = item.title
        if let imgName = item.imageName, let image = UIImage(named: imgName) {
            cell.imageView?.image = image
        } else {
            cell.imageView?.image = nil
        }
        
        return cell
    }
    
    deinit {
        self.searchController.view.removeFromSuperview()
    }
    
    class Item {
        
        var imageName:String?
        var imageUrl:String?
        var title:String {
            return "n/a"
        }
    }
    
    class BasicItem:Item {
        
        private var _title:String
        override var title: String {
            return _title
        }
        
        init(_ title:String) {
            self._title = title
        }
        
        func withImageName(_ str:String) -> Item {
            self.imageName = str
            return self
        }
        
        func withImageUrl(_ str:String) -> Item {
            self.imageUrl = str
            return self
        }
    }
    
    class SimpleItem:BasicItem {
        
        var id:Int
        init(id:Int, title:String) {
            self.id = id
            super.init(title)
        }
    }
}

