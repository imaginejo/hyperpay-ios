//
//  SignUpViewController.swift
//  HyperPay
//
//  Created by Mac on 5/28/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import Validator
import SVProgressHUD

class CountryInfo:ItemSelectorViewController.Item {
    
    var isoCode:String
    var dialCode:String
    var arabicName:String
    var englishName:String
    
    override var title: String {
        return self.englishName
    }
    
    init(code:String, dial:String, arName:String, enName:String) {
        
        self.isoCode = code
        self.dialCode = dial
        self.arabicName = arName
        self.englishName = enName
        
        super.init()
        self.imageName = code
    }
    
    private static var _allCountries = [CountryInfo]()
    static var all:[CountryInfo] {
        
        if _allCountries.count == 0 {
            
            if let path = Bundle.main.path(forResource: "Countries", ofType: "plist"),
                let countries = NSArray(contentsOfFile: path) as? [[String:String]]{
                
                _allCountries = countries.map({ (dict) -> CountryInfo in
                    
                    return CountryInfo(code: dict["isoCode"]!, dial: dict["dialCode"]!,
                                       arName: dict["name_ar"]!, enName: dict["name_en"]!)
                })
            }
        }
        
        return _allCountries
    }
    
    class func country(forCode isoCode:String) -> CountryInfo? {
        
        return all.first(where: { (info) -> Bool in
            return info.isoCode == isoCode
        })
    }
}

class SignUpViewController: BaseViewController, UITextFieldDelegate {
    
    var showClick : Bool = true
    
    @IBOutlet weak var phoneField: PhoneNumberField!
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var emailAddressField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var merchantNameField: UITextField!
    
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet var roundViews: [UIView]!
   
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isSidMenuSwipeEnabled = false
        
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notif) in
            
            if let frame = notif.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
                self.bottomConst.constant = frame.height
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillHide, object: nil, queue: nil) { _ in
            
            self.bottomConst.constant = 0
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        self.roundViews.forEach { (view) in
            view.layer.cornerRadius = 4
            view.applyDarkShadow(opacity: 0.4, offsetY: 1, radius: 2)
        }
    }
    
    @IBAction func viewDidTap(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
        
    @IBAction func showPasswordPressed(_ sender: Any) {
        
        if self.showClick == true {
            self.passwordField.isSecureTextEntry = false
            self.showClick  = false
            self.showPasswordButton.setTitle("hide".local, for: .normal)
        }else {
            self.passwordField.isSecureTextEntry = true

            self.showClick = true
            self.showPasswordButton.setTitle("show".local, for: .normal)
        }
    }
    
    @IBAction func submitSignup(_ sender: Any) {
        
        let reqRule = ValidationRuleRequired<String>(error: ValidationError.name)
        let emailRule = ValidationRulePattern(pattern: EmailValidationPattern.standard, error: ValidationError.email)
        
        var passRules = ValidationRuleSet<String>()
        passRules.add(rule: ValidationRuleLength(min: 6, max: 40, lengthType: .characters, error: ValidationError.password))
        passRules.add(rule: ValidationRuleEquality<String>(dynamicTarget: { () -> String in
            return self.confirmPasswordField.text ?? ""
        }, error: ValidationError.confirm))
        
        guard let firstName = self.firstNameField.text, firstName.validate(rule: reqRule).isValid,
            let lastName = self.lastNameField.text, lastName.validate(rule: reqRule).isValid,
            let merchant = self.merchantNameField.text, merchant.validate(rule: reqRule).isValid,
            let phone = self.phoneField.phoneNumber, phone.validate(rule: reqRule).isValid else {
                
                showErrorMessage("error-fill-fields".local)
                return
        }
        
        guard let email = self.emailAddressField.text, email.validate(rule: emailRule).isValid else {
            showErrorMessage("error-invalid-email".local)
            return
        }
        
        guard let pass = self.passwordField.text, pass.validate(rules: passRules).isValid else {
            showErrorMessage("error-invalid-pwd".local)
            return
        }
        
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show(withStatus: "registering".local)
        
        Network.request(.register(firstname: firstName,
                                  lastname: lastName,
                                  email: email,
                                  password: pass,
                                  merchant: merchant,
                                  mobile: phone),
            success: { (model) in
                
                SVProgressHUD.dismiss()
                
                if let res = model as? Bool, res {
                    self.performSegue(withIdentifier: "signup_to_success", sender: nil)
                }
                
        }) { (err) in
            
            SVProgressHUD.dismiss()
            showErrorMessage(err.localizedDescription)
        }
    }
}


