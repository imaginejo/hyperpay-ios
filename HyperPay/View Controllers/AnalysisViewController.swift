//
//  AnalysisViewController.swift
//  HyperPay
//
//  Created by Mac on 5/31/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class AnalysisViewController: UIViewController {
    
    @IBOutlet var roundViews: [UIView]!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.roundViews.forEach { (view) in
            view.layer.cornerRadius = 5
        }
    }
    
    @IBAction func showSideMenu(_ sender: Any) {
        
        NotificationCenter.default.post(name: NotificationNames.ShowSideMenu, object: nil)
    }
    
    var selectedChartType:ChartType = .transactionCount
    @IBAction func openChart(_ sender: Any) {
        
        let button = sender as! UIButton
        self.selectedChartType = button.tag == 0 ? .transactionCount : .turnovers
        self.performSegue(withIdentifier: "analysis_to_chart", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        (segue.destination as! AnalysisChartViewController).chartType = self.selectedChartType
    }
}
