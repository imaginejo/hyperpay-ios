//
//  DatePickerViewController.swift
//  Shakawekom
//
//  Created by AhmeDroid on 4/11/17.
//  Copyright © 2017 Imagine Technologies. All rights reserved.
//

import UIKit

protocol DatePickerViewControllerDelegate {

    func datePickerController(_ datePickerVC: DatePickerViewController, didSelectDate selectedDate: Date) -> Void
    func datePickerControllerDidClearSelection(_ datePickerVC:DatePickerViewController) -> Void
}

class DatePickerViewController: UIViewController {
    
    static func launch(delegate:DatePickerViewControllerDelegate?, initialDate:Date? = nil, maximumDate:Date? = nil, tag:Int = 0) -> Void {
        
        if let viewC = UIApplication.topViewController(),
            let datePickerVC = viewC.storyboard?.instantiateViewController(withIdentifier: "datePickerVC") as? DatePickerViewController  {
            
            datePickerVC.tag = tag
            datePickerVC.maximumDate = maximumDate
            datePickerVC.currentDate = initialDate ?? Date()
            datePickerVC.modalTransitionStyle = .crossDissolve
            datePickerVC.modalPresentationStyle = .overCurrentContext
            datePickerVC.delegate =  delegate
            
            viewC.present(datePickerVC, animated: true, completion: nil)
        } else {
            
            print("Can't launch date picker on non-UIViewController objects")
        }
    }

    var tag:Int = 0
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var selectionLabel: UILabel!
    
    var delegate:DatePickerViewControllerDelegate?
    var maximumDate:Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.datePicker.maximumDate = self.maximumDate
        
        if let _date = self.currentDate {
            self.updateDateLabel(_date)
            self.datePicker.setDate(_date, animated: false)
        }
        
        let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.numberOfTapsRequired = 1
        
        self.view.addGestureRecognizer(tapRecognizer)
        tapRecognizer.addTarget(self, action: #selector( viewWasTapped ))
    }
    
    @objc func viewWasTapped() -> Void {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    private var currentDate:Date!
    @IBAction func datePickerChanged(_ sender: Any) {
        
        let selectedDate = self.datePicker.date
        
        self.updateDateLabel(selectedDate)
        self.currentDate = selectedDate
    }
    
    func updateDateLabel(_ date:Date) -> Void {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE yyyy/MM/dd"
        
        self.selectionLabel.text = formatter.string(from: date)
    }
    
    @IBAction func clearSelection(_ sender: Any) {
        
        self.dismiss(animated: true) {
            self.delegate?.datePickerControllerDidClearSelection(self)
        }
    }
    
    @IBAction func finishSelection(_ sender: Any) {
        
        self.dismiss(animated: true) { 
            
            if self.currentDate != nil {
                self.delegate?.datePickerController(self, didSelectDate: self.currentDate)
            }
        }
    }
}
