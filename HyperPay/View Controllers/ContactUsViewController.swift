//
//  ContactUsViewController.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 10/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    @IBOutlet weak var topView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.topView.applyDarkShadow(opacity: 0.2, offsetY: 0, radius: 2)
    }
    
    @IBAction func closeView(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func callSaudiArabia(_ sender: Any) {
        
        if let url = URL(string: "telprompt:00966112888188"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func callJordan(_ sender: Any) {
        
        if let url = URL(string: "telprompt:00962782909030"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func openMailDialog(_ sender: Any) {
        
        if let url = URL(string: "mailto:info@hyperpay.com"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
}
