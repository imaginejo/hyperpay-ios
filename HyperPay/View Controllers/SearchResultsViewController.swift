//
//  SearchResultsViewController.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 7/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class SearchResultsViewController: BaseTransactionsViewController, SearchFilterViewControllerDelegate {
   
    var currentSelection:[SearchFilter]!
    var currentChannel:ResponseModel.User.Channel!
    
    @IBOutlet weak var countLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.updateCountLabels(searching: false)
        self.reloadData()
    }
    
    func updateCountLabels(searching:Bool) -> Void {
        
        let font = self.countLabel.font!
        let attrs:[NSAttributedStringKey: Any] = [
            NSAttributedStringKey.font: UIFont(name: Locale.current.isRightToLeft ? "DINNextLTW23-Bold" : "SeroPro-Medium", size: font.pointSize)!,
            NSAttributedStringKey.foregroundColor: UIColor.white
        ]
        
        if searching {
            
            self.countLabel.attributedText = NSAttributedString(string: "searching".local, attributes: attrs)
            
        } else {
            
            let name =  String(format: "search-count".local, self.viewModel.totalCount)
            let msg = String(format: "search-results".local, name)
            
            let attrMsg = NSMutableAttributedString(string: msg)
            let range = (msg as NSString).range(of: name)
            
            attrMsg.addAttributes(attrs, range: range)
            self.countLabel.attributedText = attrMsg
        }
    }
    
    @IBAction func openSearchFilters(_ sender: Any) {
        
        if let searchFilterVC = self.storyboard?.instantiateViewController(withIdentifier: "searchFilterVC") as? SearchFilterViewController {
            
            searchFilterVC.currentDateRange = self.dateRange
            searchFilterVC.filtersList = self.currentSelection.map({ (filter) -> SearchFilter in
                return filter.copy()
            })
            
            searchFilterVC.delegate = self
            self.navigationController?.present(searchFilterVC, animated: true, completion: nil)
        }
    }
    
    func searchFilterVC(_ searchFilterVC: SearchFilterViewController, didDismissWithSelection filters: [SearchFilter]) {
        
        self.currentSelection = filters
        self.reloadData()
    }
    
    override func onFetchCompleted(withSections sections: IndexSet, andRows rows: [IndexPath]) {
        
        self.updateCountLabels(searching: false)
        super.onFetchCompleted(withSections: sections, andRows: rows)
    }
    
    override func buildTransactionsRequest(lastIndex: Int) -> HyperPay {
        
        var start:Date?, end:Date?
        if let range = self.dateRange {
            let (_st, _en) = range
            start = _st
            end = _en
        }
        
        var filters = [String:Any]()
        self.currentSelection.forEach { (filter) in
            
            let key = filter.condition == .and ? "and" : "or";
            var value:Any?
            
            switch filter.field.type {
            case .text, .select:
                value = filter.simpleValue().value!
            case .multiSelect:
                value = filter.multiValue().values.map({ $0! })
            case .number:
                value = [
                    "operation" : filter.numberValue().operation.rawValue,
                    "value": filter.numberValue().value
                ]
            case .nameValue:
                
                if let name = filter.pairValue().name {
                    value = [
                        name: filter.pairValue().value!
                    ]
                }
            }
            
            filters[key + "[\(filter.field.name)]"] = value ?? NSNull()
        }
        
        return .searchTransactions(
            reportType: self.selectedReportType.serverValue,
            channel: self.currentChannel.id,
            fromDate: start, toDate: end, filters: filters,
            lastTransactionId: lastIndex,
            pageSize: 5,
            sort: self.selectedSortType.serverValue)
    }
    
    @IBAction func closeView(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
