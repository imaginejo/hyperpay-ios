//
//  MainTabBarController.swift
//  HyperPay
//
//  Created by Mac on 5/29/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        let selectedColor   = UIColor(red: 54/255.0, green: 81/255.0, blue: 108/255.0, alpha: 1.0)
        let unselectedColor = UIColor(red: 169/255.0, green: 188/255.0, blue: 204/255.0, alpha: 1.0)
        
        let appearance = UITabBarItem.appearance()
        let font:UIFont
        
        if Locale.current.isRightToLeft {
            font = UIFont(name: "DINNextLTW23-Regular", size: 11)!
        } else {
            font =  UIFont(name: "SeroPro",size: 10.0)!
        }
        
        appearance.setTitleTextAttributes([NSAttributedStringKey.font : font ,NSAttributedStringKey.foregroundColor  : unselectedColor ], for: .normal)
        appearance.setTitleTextAttributes([NSAttributedStringKey.font : font ,NSAttributedStringKey.foregroundColor  : selectedColor ], for:.selected)
        
        NotificationCenter.default.addObserver(forName: NotificationNames.ShowTransactionDetails, object: nil, queue: nil) { (notif) in
            self.selectedIndex = 1
        }
        
        if AppDelegate.toBeOpenedTransactionUniqueId != nil {
            self.selectedIndex = 1
        }
    }
}


