//
//  TransactionDetailsViewController.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 10/7/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import SwiftyJSON

class TransactionDetailsViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    var transactionUniqueId:String!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var channelLabel: InsetLabel!
    @IBOutlet weak var paymentTypeLabel: InsetLabel!
    @IBOutlet weak var paymentMethodLabel: InsetLabel!
    @IBOutlet weak var transactionIdLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.channelLabel.insetX = 7
        self.paymentMethodLabel.insetX = 7
        self.paymentTypeLabel.insetX = 7
        
        self.container.layer.cornerRadius = 6.2
        self.container.applyDarkShadow(opacity: 0.4, offsetY: 1)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector( transactionIdLabelTapped(_:) ))
        
        self.transactionIdLabel.isUserInteractionEnabled = true
        self.transactionIdLabel.addGestureRecognizer(tap)
        
        self.scrollView.isHidden = true
        self.fetchData()
    }
    
    @objc func transactionIdLabelTapped(_ sender: Any) {
        
        showAlertMessage("Transaction ID", message: self.currentReport.transactionId, okLabel: "Close")
    }
    
    var currentReport:TransactionReport!
    
    func reloadForTransaction(uniqueId:String) -> Void {
        self.transactionUniqueId = uniqueId
        self.scrollView.isHidden = true
        self.fetchData()
    }
    
    func fetchData() -> Void {
        
        print("LOADING TRANSACTION: \(self.transactionUniqueId ?? "N/A")")
        
        self.indicator.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            
            self.indicator.stopAnimating()
            
            if let model = TransactionReport( JSON(parseJSON: sampleData) ) {
                self.fillAndPopFields(model)
            }
        }
    }
    
    func fillAndPopFields(_ report:TransactionReport) -> Void {
        
        self.currentReport = report
        self.scrollView.isHidden = false
        
        let df = DateFormatter()
        df.dateFormat = "d MMMM yyyy - h:mm a"
        
        self.dateLabel.text = df.string(from: report.requestDate).uppercased().replacingArabicNumbers()
        self.nameLabel.text = report.accountHolder
        self.transactionIdLabel.text = report.transactionId
        
        self.volumeLabel.attributedText = moneyString(report.amount, size: 14)
        self.currencyLabel.text = report.currency
        
        if report.result == "ACK" {
            self.volumeLabel.textColor = #colorLiteral(red: 0.1843137255, green: 0.5647058824, blue: 0.2862745098, alpha: 1)
        } else {
            self.volumeLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
    
        self.paymentMethodLabel.text = report.verStatus
        self.paymentTypeLabel.text = report.ccvResult
        
        if report.result == "ACK" {
            self.channelLabel.text = "approved".local
            self.channelLabel.backgroundColor = #colorLiteral(red: 0.6535916924, green: 0.738933742, blue: 0.2482890487, alpha: 1)
        } else {
            self.channelLabel.text = "declined".local
            self.channelLabel.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        
        let fields:[(String, String)] = [
            ("UNIQUE ID", report.uniqueId ),
            ("INSITITUTE NAME", report.insitituteName ),
            ("AUTHORIZE ID", report.authorizeId ?? "N/A" ),
            ("BIN\\ BIN COUNTRY", "\(report.bin) \\ \( report.binCountry )"),
            ("ENTITY ID", report.channelId ),
            ("MERCHANT BANK ID", "TBD" ),
            ("FULL ACCOUNT NUMBER", "\(report.fullAccountNumber)" ),
            ("CONNECTOR TXID3", report.connectorTxID3 ?? "N/A" ),
            ("PAYMENT METHOD", report.paymentMethod ),
            ("PAYMENT TYPE", report.paymentType ),
            ("CHANNEL NAME", report.channelName ),
            ("RISK SCORE", report.riskScore ?? "N/A" ),
            ("RESPONSE CODE", report.returnCode )
        ]

        fields.forEach { (name, value) in
            
            let view = TransactionPropertyView.create()
            
            view.nameLabel.text = name.uppercased().local
            view.valueLabel.text = value
            
            let const = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal,
                                           toItem: nil, attribute: .notAnAttribute,
                                           multiplier: 1, constant: 35)
            const.priority = .defaultHigh
            view.addConstraint( const )
            
            self.stackView.addArrangedSubview(view)
        }
    }

    @IBAction func dismissView(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}


let sampleData = """
{
"PaymentMethod" : "MASTER",
"ExtraFields" : null,
"Bin" : 536122,
"BinCountry" : "JO",
"FullAccountNumber" : 3535,
"card_bank" : "JORDAN KUWAIT BANK",
"BatchNo" : "20180222",
"MerchantName" : "Obaida",
"acquirerresponse" : null,
"id" : 34,
"currency" : "JOD",
"Ip" : "",
"ReturnCode" : "000.000.000",
"ChannelCustomerId" : null,
"AuthorizeId" : "519284",
"MerchantCustomerId" : null,
"Credit" : 7.8700000000000001,
"connectorId" : "1100036178",
"ConnectorTxID2" : "805321185920",
"ReasonCode" : null,
"RequestTimestamp" : "2018-05-22 10:59:46",
"card_level" : "PREPAID",
"ChannelId" : "8a82941755c594e30155d44613f51119",
"created_at" : "2018-02-22 10:59:46",
"Result" : "ACK",
"Debit" : null,
"Email" : "obaida@obaida.com",
"Amount" : 7.8700000000000001,
"RegistrationId" : null,
"ReferenceId" : null,
"AccountHolder" : "MC PP",
"ConnectorTxID1" : "1100036178",
"ResultTimestamp" : "2018-02-22 10:59:46",
"endToEndId" : "2018222125824809",
"IpCountry" : null,
"UpdatedBy" : null,
"CTPE_MERCHANT_ACCOUNT_DATA_ID" : null,
"card_type" : "DEBIT",
"updated_at" : "2018-07-10 11:51:08",
"PaymentType" : "DB",
"Mobile" : "",
"Installments_plans" : null,
"ChannelName" : "First Channel",
"UniqueId" : "8acda4a561b2a32c0161bd2ad29a1361",
"ConnectorTxID3" : "8acda4a561b2a32c0161bd2ad29a1361",
"Installments" : 0,
"ClearingInsitituteName" : "JORDAN KUWAIT BANK",
"ShortId" : "8acda4a561b2a32c0161bd2ad29a1361",
"InsertedBy" : "Hook",
"VerStatus" : "M",
"TransactionId" : "2018222125824809",
"CVVResult" : "M",
"AvsResultCode" : "Unsupported"
}
"""
