//
//  MainNavigationController.swift
//  HyperPay
//
//  Created by Mac on 5/29/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import SideMenu

class BaseViewController:UIViewController {
    
    var isSidMenuSwipeEnabled:Bool = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let navVC = self.navigationController as? MainNavigationController {
            navVC.isSideMenuSwipeEnabled = self.isSidMenuSwipeEnabled
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let navVC = self.navigationController as? MainNavigationController {
            navVC.isSideMenuSwipeEnabled = true
        }
    }
}

class MainNavigationController: UINavigationController {
    
    var screenEdgeRecognizers:[UIGestureRecognizer]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Network.isAuthenticated {
            
            if let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "welcomeVC") as? WelcomeViewController {
                welcomeVC.shouldRestoreUser = true
                self.pushViewController(welcomeVC, animated: false)
            }
        }
        
        if let menuVC = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuVC") as? SideMenuViewController {
            
            menuVC.targetNavigation = self
            
            let menuNavVC = UISideMenuNavigationController(rootViewController: menuVC)
            menuNavVC.isNavigationBarHidden = true
            
            SideMenuManager.default.menuWidth = self.view.frame.width * 0.8
            SideMenuManager.default.menuAnimationFadeStrength = 0.6
            SideMenuManager.default.menuPresentMode = .menuSlideIn
            SideMenuManager.default.menuFadeStatusBar = false
            
            if Locale.current.isRightToLeft {
                
                SideMenuManager.default.menuRightNavigationController = menuNavVC
                SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationBar)
                self.screenEdgeRecognizers = SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.view, forMenu: UIRectEdge.right)
                
            } else {
                
                SideMenuManager.default.menuLeftNavigationController = menuNavVC
                SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationBar)
                self.screenEdgeRecognizers = SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.view, forMenu: UIRectEdge.left)
            }
        }
        
        NotificationCenter.default.addObserver(forName: NotificationNames.ShowSideMenu, object: nil, queue: nil) { (notif) in
            
            let menuVC = Locale.current.isRightToLeft
                ? SideMenuManager.default.menuRightNavigationController!
                : SideMenuManager.default.menuLeftNavigationController!
            self.present(menuVC, animated: true, completion: nil)
        }
        
        NotificationCenter.default.addObserver(forName: NotificationNames.HideSideMenu, object: nil, queue: nil) { (notif) in
            
            let menuVC = Locale.current.isRightToLeft
                ? SideMenuManager.default.menuRightNavigationController!
                : SideMenuManager.default.menuLeftNavigationController!
            menuVC.dismiss(animated: true, completion: nil)
        }
        
        NotificationCenter.default.addObserver(forName: NotificationNames.HideSideMenuNonAnimated, object: nil, queue: nil) { (notif) in
            
            let menuVC = Locale.current.isRightToLeft
                ? SideMenuManager.default.menuRightNavigationController!
                : SideMenuManager.default.menuLeftNavigationController!
            
            menuVC.dismiss(animated: false, completion: nil)
        }
    }
    
    private var _isSideMenuSwipeEnabled:Bool = true
    var isSideMenuSwipeEnabled:Bool {
        
        get { return self._isSideMenuSwipeEnabled  }
        set {
            self._isSideMenuSwipeEnabled = newValue
            self.screenEdgeRecognizers?.forEach { (reco) in
                reco.isEnabled = newValue
            }
        }
    }
}
