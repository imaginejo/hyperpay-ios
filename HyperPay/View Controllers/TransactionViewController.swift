//
//  TransactionViewController.swift
//  HyperPay
//
//  Created by Mac on 5/23/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class TransactionReport {
    
    var id:String = UUID().uuidString
    var accountHolder:String
    var volume:Double
    var currency:String = "USD"
    var date:Date
    
    var properties = [(String, String)]()
    
    init(holder:String, volume:Double, date:Date) {
        
        self.accountHolder = holder
        self.volume = volume
        self.date = date
    }
}

class TransactionsViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var channelSelector: ChannelSelector!
    var reportsList = [ReportsList]()
    class ReportsList {
        
        var day:String
        var list = [TransactionReport]()
        
        init(_ day:String) {
            self.day = day
        }
    }
    
    func reportsListForDay(_ day:String) -> ReportsList? {
        
        return self.reportsList.first { (repList) -> Bool in
            return repList.day == day
        }
    }
    
    @IBOutlet weak var rejectedValueLabel: UILabel!
    @IBOutlet weak var pendingValueLabel: UILabel!
    @IBOutlet weak var acceptedValueLabel: UILabel!
    @IBOutlet weak var amountValueLabel: UILabel!
    
    @IBOutlet weak var statsView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var amountNameLabel: UILabel!
    @IBOutlet weak var reportTypeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func showSideMenu(_ sender: Any) {
        
        NotificationCenter.default.post(name: NotificationNames.ShowSideMenu, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 180
        self.reloadTransactionsData()
        
        if #available(iOS 11, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.reportsList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.reportsList[section].list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionsTableViewCell
        let report = self.reportsList[indexPath.section].list[indexPath.row]
        
        cell.displayReport( report )
        
        return cell
    }
    
    func reloadTransactionsData() -> Void {
        
        self.amountValueLabel.setMoney(5000.89)
        self.acceptedValueLabel.setMoney(3500)
        self.pendingValueLabel.setMoney(485)
        self.rejectedValueLabel.setMoney(1015.76)
        
        let h:Double = 3600
        let d:Double = 24 * h
        
        // Random data
        let allReports = [
            TransactionReport(holder: "Barbara Stephens", volume: 230, date: Date(timeIntervalSinceNow: -4 * h)),
            TransactionReport(holder: "Megan Edwards", volume: 4357.1, date: Date(timeIntervalSinceNow: -2 * h)),
            TransactionReport(holder: "Willie Bennett", volume: 645.02, date: Date(timeIntervalSinceNow: -1 * h)),
            TransactionReport(holder: "Sharon Rogers", volume: 235.50, date: Date(timeIntervalSinceNow: -1 * d - 1 * h)),
            TransactionReport(holder: "Anthony Harris", volume: 74754.56, date: Date(timeIntervalSinceNow: -1 * d - 7 * h)),
            TransactionReport(holder: "Bell Howard", volume: 2350.72, date: Date(timeIntervalSinceNow: -1 * d - 5 * h)),
            TransactionReport(holder: "Khaled Ahmad", volume: 123.23, date: Date(timeIntervalSinceNow: -3 * d)),
            TransactionReport(holder: "Mohammed Abed", volume: 45.61, date: Date(timeIntervalSinceNow: -3 * d - 1 * h)),
            TransactionReport(holder: "Sarah Jameel", volume: 564.12, date: Date(timeIntervalSinceNow: -3 * d - 3 * h)),
            TransactionReport(holder: "Jaber Ahmad", volume: 85343214.11, date: Date(timeIntervalSinceNow: -3 * d - 7 * h)),
            TransactionReport(holder: "Salem AbdelJalil", volume: 8.02, date: Date(timeIntervalSinceNow: -7 * d - 1 * h)),
            TransactionReport(holder: "Micheal Jackson", volume: 131.04, date: Date(timeIntervalSinceNow: -7 * d - 3 * h)),
        ]
        
        self.reportsList.removeAll()
        
        let df = DateFormatter()
        df.dateFormat = "d MMM yyyy"
        
        allReports.forEach { (rep) in
            
            let day = df.string(from: rep.date).uppercased()
            
            var dayList:ReportsList
            if let _list = self.reportsListForDay(day) {
                dayList = _list
            } else {
                dayList = ReportsList(day)
                self.reportsList.append(dayList)
            }
            
            switch self.selectedReportType {
                
            case .general:
                rep.properties = [
                    ("UNIQE ID", "756783559378954234234"),
                    ("RISK SCORE", "-100")
                ]
            case .reconciliation:
                rep.properties = [
                    ("BIN\\ BIN COUNTRY", "455232 - SAUDI ARABIA"),
                    ("UNIQE ID", "756783559378954234234"),
                    ("ENTITY ID", "874682352321"),
                    ("MERCHANT BANK ID", "874682352321")
                ]
            case .risk:
                rep.properties = [
                    ("BIN\\ BIN COUNTRY", "455232 - SAUDI ARABIA"),
                    ("UNIQE ID", "7567835593784234234"),
                    ("RESPONSE CODE", "000.000.000")
                ]
            }
            
            dayList.list.append(rep)
        }
        
        self.reportsList.forEach { (dayList) in
            
            dayList.list.sort(by: { (rep1, rep2) -> Bool in
                return rep1.date.timeIntervalSince1970 > rep2.date.timeIntervalSince1970
            })
        }
        
        self.reportsList.sort { (repList1, repList2) -> Bool in
            
            if let date1 = df.date(from: repList1.day),
                let date2 = df.date(from: repList2.day) {
                return date1.timeIntervalSince1970 > date2.timeIntervalSince1970
            }
            
            return false
        }
        
        self.tableView.setContentOffset(CGPoint.zero, animated: false)
        self.tableView.reloadData()
    }
    
    var headersMap = [Int:DayHeaderView]()
    var selectedReportType:ReportType = .general
    
    private class AlertAction: UIAlertAction {
        var type:ReportType = .general
    }
    
    @IBAction func changeReportType(_ sender: Any) {
        
        let alert = UIAlertController(title: "Select Report Type", message: nil, preferredStyle: .actionSheet)
        let handler:((UIAlertAction) -> Void) = { (action) in
            
            let type = (action as! AlertAction).type
            
            self.selectedReportType = type
            self.reportTypeButton.setTitle(type.rawValue, for: .normal)
            self.statsView.isHidden = self.selectedReportType == .reconciliation
            self.amountNameLabel.text = self.selectedReportType == .reconciliation ? "Total Reconciliation" : "Balance Income"
            
            self.reloadTransactionsData()
            
        }
        
        let option1 = AlertAction(title: ReportType.general.rawValue, style: .default, handler: handler)
        let option2 = AlertAction(title: ReportType.reconciliation.rawValue, style: .default, handler: handler)
        let option3 = AlertAction(title: ReportType.risk.rawValue, style: .default, handler: handler)
        
        option1.type = ReportType.general
        option2.type = ReportType.reconciliation
        option3.type = ReportType.risk
        
        switch self.selectedReportType {
        case .general:
            option1.setValue(#imageLiteral(resourceName: "check_mark"), forKey: "image")
        case .reconciliation:
            option2.setValue(#imageLiteral(resourceName: "check_mark"), forKey: "image")
        case .risk:
            option3.setValue(#imageLiteral(resourceName: "check_mark"), forKey: "image")
        }
        
        alert.addAction(option1)
        alert.addAction(option2)
        alert.addAction(option3)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension TransactionsViewController {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = DayHeaderView.create()
        view.dateLabel.text = self.reportsList[section].day
        
        self.headersMap[section] = view
        return view
    }
    
    func tableView(_ tableView: UITableView, didEndDisplayingHeaderView view: UIView, forSection section: Int) {
        
        (view as! DayHeaderView).isDisplayed = false
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        (view as! DayHeaderView).isDisplayed = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let coy = self.tableView.contentOffset.y
        
        self.headersMap.forEach { (section, view) in
            
            let hy = self.tableView.rectForHeader(inSection: section).origin.y
            let isFloating = coy >= hy && view.isDisplayed
            
            if isFloating != view.isFloating {
                
                if isFloating {
                    view.darken()
                } else {
                    view.lighten()
                }
            }
            
            view.isFloating = isFloating
        }
    }
}

enum ReportType:String {
    case general = "General"
    case reconciliation = "Reconciliation"
    case risk = "Risk"
}
