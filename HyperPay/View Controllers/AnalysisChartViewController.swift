//
//  TransactionCountViewController.swift
//  HyperPay
//
//  Created by Mac on 6/10/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import Charts
import Moya

enum ChartType {
    case transactionCount
    case turnovers
    
    var title:String {
        
        switch self {
        case .turnovers:
            return "turnovers".local
        case .transactionCount:
            return "transaction-count".local
        }
    }
}

class AnalysisChartViewController: UIViewController, TimeUnitStepperDelegate {
    
    var chartType:ChartType = .transactionCount
    
    @IBOutlet weak var chartTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet var buttons: [UIButton]!
    
    @IBOutlet weak var orangeLine: UIView!
    @IBOutlet weak var timeUnitStepper: TimeUnitStepper!
    @IBOutlet weak var barChart: BarChartView!

    var currentIndex:Int = 0
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.titleLabel.text = self.chartType.title
        self.timeUnitStepper.delegate = self
        self.setupChart()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.showTabContent(atIndex: 0)
    }
    
    func setupChart() -> Void {
        
        let marker = BalloonMarker(color: UIColor.blue,
                                   font: UIFont.boldSystemFont(ofSize: 11),
                                   textColor: UIColor.white,
                                   insets: UIEdgeInsets(top: 5, left: 5, bottom: 16, right: 5))
        
        marker.valueFormatter = MarkerValueFormatter(block: { (value) -> String in
            
            return self.numberFormatter.string(from: NSNumber(value: value))!
        })
        
        barChart.marker = marker
        barChart.noDataText = ""
        barChart.noDataTextColor = UIColor.white
        barChart.legend.enabled = false
        barChart.chartDescription?.enabled = false
        barChart.backgroundColor = UIColor.clear
        barChart.fitBars = true
        
        let lineColor = UIColor(white: 1, alpha: 0.4)
        let labelColor = UIColor(white: 1, alpha: 0.8)
        
        barChart.xAxis.labelTextColor = labelColor
        barChart.xAxis.axisLineColor = lineColor
        barChart.xAxis.gridColor = lineColor
        
        barChart.xAxis.labelPosition = .bottom
        barChart.xAxis.drawGridLinesEnabled = false
        barChart.xAxis.granularity = 1
        barChart.xAxis.granularityEnabled = true
        
        barChart.pinchZoomEnabled = false
        barChart.doubleTapToZoomEnabled = false
        
        barChart.setExtraOffsets(left: 5, top: 0, right: 20, bottom: 20)
        barChart.rightAxis.enabled = false
        
        barChart.leftAxis.axisMinimum = 0
        barChart.leftAxis.labelTextColor = labelColor
        barChart.leftAxis.axisLineColor = lineColor
        barChart.leftAxis.gridColor = lineColor
        
        if self.chartType == .transactionCount {
            barChart.leftAxis.granularity = 1
            barChart.leftAxis.granularityEnabled = true
        }
        
        barChart.leftAxis.drawAxisLineEnabled = false
        barChart.leftAxis.valueFormatter = DefaultAxisValueFormatter(block: { (value, _) -> String in
            
            return self.numberFormatter.string(from: NSNumber(value: value))!
        })
    }
    
    var dataReq:Cancellable?
    func reloadData() -> Void {
        
        let channelID = Network.authenticatedUser.channels.first!.id
        
        self.adjustChartTitle()
        self.barChart.clear()
        self.indicator.startAnimating()
        
        let start = self.timeUnitStepper.start
        let end = self.timeUnitStepper.end
        
        self.dataReq?.cancel()
        self.dataReq = Network.request(.graph(channel: channelID, from: start, to: end), success:
            { (model) in
                
                self.indicator.stopAnimating()
                
                if let points = model as? [ResponseModel.GraphPoint] {
                    
                    print("Chart Loaded: \(points.count)")
                    self.rebuildChart(points)
                }
                
        }, failure: { [weak self] (error) in
            
            self?.indicator.stopAnimating()
            if(self != nil){
                if (error as NSError?)?.code == NSURLErrorCancelled {
                    showErrorMessage(error.localizedDescription)
                }
            }
        })
    }
    
    func timeUnitStepper(_ unitStepper: TimeUnitStepper, didChangeRangeStartingAt start: Date, endingAt end: Date) {
        
        self.reloadData()
    }
    
    private func adjustChartTitle() {
        
        let prop = self.chartType.title.uppercased()
        
        let timeUnit:String
        switch self.timeUnitStepper.unit {
        case .day:
            timeUnit = "chart-day".local.uppercased()
        case .week:
            timeUnit = "chart-week".local.uppercased()
        case .month, .quarter:
            timeUnit = "chart-month".local.uppercased()
        case .year:
            timeUnit = "chart-year".local.uppercased()
        }
        
        self.chartTitleLabel.text = "\(prop) \("per".local.uppercased()) \(timeUnit)"
    }
    
    private var numberFormatter:NumberFormatter {
        
        let nf = NumberFormatter()
        nf.locale = Locale(identifier: "en_US")
        
        if self.chartType == .transactionCount {
            
            nf.maximumFractionDigits = 0
            nf.numberStyle = .none
            
        } else {
            
            nf.maximumFractionDigits = 2
            nf.numberStyle = .currency
            nf.currencySymbol = ""
        }
        
        return nf
    }
    
    private var dateFormatter:DateFormatter {
        
        let df = DateFormatter()
        
        switch self.timeUnitStepper.unit {
        case .year:
            df.dateFormat = "MMM"
        case .month:
            df.dateFormat = "dd"
        case .week, .day:
            df.dateFormat = "EEE"
        default:
            df.dateFormat = "dd/MM"
        }
        
        return df
    }
    
    func rebuildChart(_ points:[ResponseModel.GraphPoint]) -> Void {
        
        self.barChart.clear()
        
        var entries = [BarChartDataEntry]()
        var labels = [String]()
        points.forEach { (point) in
            
            labels.append( self.dateFormatter.string(from: point.date).replacingArabicNumbers() )
            
            let yval = self.chartType == .transactionCount ? Double(point.count) : point.volume
            entries.append( BarChartDataEntry(x: Double(entries.count), y: yval))
        }
        
        let dataSet = BarChartDataSet(values: entries, label: "Volume")
        dataSet.setColor(UIColor(white: 1, alpha: 0.6))
        dataSet.highlightAlpha = 1
        dataSet.highlightColor = UIColor.orange
        dataSet.drawValuesEnabled = false
        dataSet.axisDependency = .left
        
        barChart.data = BarChartData(dataSet: dataSet)
        barChart.xAxis.labelCount = labels.count
        barChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: labels)
        barChart.xAxis.labelFont = UIFont(name: Locale.current.isRightToLeft  ? "DINNextLTW23-Regular" : "SeroPro", size: 12) ?? barChart.xAxis.labelFont
        barChart.notifyDataSetChanged()
    }
    
    func showTabContent(atIndex index:Int) -> Void {
        
        self.currentIndex = index
        self.focusOnTab(atIndex: index)
        
        print("Tab Selected: \(index)")
        switch index {
        case 0:
            self.timeUnitStepper.unit = .day
        case 1:
            self.timeUnitStepper.unit = .week
        case 2:
            self.timeUnitStepper.unit = .month
        case 3:
            self.timeUnitStepper.unit = .year
        default:
            break
        }
        
        self.reloadData()
    }
    
    func focusOnTab(atIndex index:Int) -> Void {
        
        for btn in self.buttons {
            
            if Locale.current.isRightToLeft {
                btn.titleLabel?.font = UIFont(name: btn.tag == index ? "DINNextLTW23-Bold" : "DINNextLTW23-Regular", size: 14)
            } else {
                btn.titleLabel?.font = UIFont(name: btn.tag == index ? "SeroPro-Bold" : "SeroPro", size: 14)
            }
            
            let color = btn.tag == index ? UIColor.white : UIColor(valueRed: 116, green: 133, blue: 158, alpha: 0.8)
            btn.setTitleColor(color, for: .normal)
            
            if btn.tag == index {
            
                var olf = self.orangeLine.frame
                let blp = btn.convert(CGPoint.zero, to: self.orangeLine.superview)
                
                UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: .calculationModeCubic, animations: {
                    
                    UIView.setAnimationCurve(.easeInOut)
                    UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.6, animations: {
                        
                        olf.origin.x = min(olf.origin.x, blp.x)
                        olf.size.width = btn.frame.width + olf.size.width
                        self.orangeLine.frame = olf
                    })
                    
                    UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.4, animations: {
                        
                        olf.origin.x = blp.x
                        olf.size.width = btn.frame.width
                        self.orangeLine.frame = olf
                    })
                    
                }) { (finish) in
                    
                    print("Finished")
                }
            }
        }
    }
    
    @IBAction func timePeriodPressed(_ sender:Any) -> Void {
        
        let button = sender as! UIButton
        
        self.showTabContent(atIndex: button.tag)
    }
    
    @IBAction func goback(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
