//
//  LoginViewController.swift
//  HyperPay
//
//  Created by Mac on 5/22/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import Validator
import Alamofire
import AlamofireImage
import SVProgressHUD

enum ValidationError : Error {
    case password
    case email
    case confirm
    case name
}

class LoginViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet var roundViews: [UIView]!
    
    @IBOutlet weak var phoneField: PhoneNumberField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var bottomConstant: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Phone : "788149187"
        // Pass: "test1234"
        
        self.passwordField.delegate = self
        
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }

        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillShow, object: nil, queue: nil) { [weak self] (notif) in
            
            if let frame = notif.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
                self?.bottomConstant.constant = frame.height * -1
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    self?.view.layoutIfNeeded()
                })
            }
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillHide, object: nil, queue: nil) { [weak self] _ in
            
            self?.bottomConstant.constant = 0
            UIView.animate(withDuration: 0.2, animations: { [weak self] in
                self?.view.layoutIfNeeded()
            })
        }
        
        self.isSidMenuSwipeEnabled = false
        self.roundViews.forEach { (view) in
            view.layer.cornerRadius = 4
            view.applyDarkShadow(opacity: 0.4, offsetY: 1, radius: 2)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.passwordField.text = nil
    }
    
    @IBAction func viewDidTap(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func submitLogin(_ sender: Any) {
        
        guard
            let phone = self.phoneField.phoneNumber,
            let password = self.passwordField.text,
            password.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == false else {
                
            showErrorMessage("error-fill-usr-pwd".local)
            return
        }
        
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
        
        Network.authenticateUser(withPhoneNumber: phone, andPassword: password) { (success, error) in
            
            SVProgressHUD.dismiss()
            
            if success, let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "welcomeVC") {
                
                let transition = CATransition()
                transition.duration = 0.45
                transition.type = "flip"
                transition.subtype = kCATransitionFromLeft
                self.navigationController?.view.layer.add(transition, forKey: kCATransition)
                self.navigationController?.pushViewController(welcomeVC, animated: false)
                
            } else {
                
                showErrorMessage(error ?? "error-random".local)
            }
        }
    }
}


