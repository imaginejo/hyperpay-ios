//
//  AccountSettingsViewController.swift
//  HyperPay
//
//  Created by Mac on 5/31/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import SVProgressHUD
import Moya

fileprivate enum Language {
    case arabic
    case english
    
    var code:String {
        switch self {
        case .english:
            return "en"
        case .arabic:
            return "ar"
        }
    }
    
    var region:String {
        switch self {
        case .english:
            return "US"
        case .arabic:
            return "JO"
        }
    }
}

class SettingsViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var pushNotifSwitch: UISwitch!
    @IBOutlet weak var englishSelectionView: UIImageView!
    @IBOutlet weak var arabicSelectionView: UIImageView!
    
    @IBOutlet weak var oldPasswordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    fileprivate var currentLanguage:Language = Locale.current.isRightToLeft ? .arabic : .english
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.oldPasswordField.delegate = self
        self.newPasswordField.delegate = self
        self.confirmPasswordField.delegate = self
        
        self.fillEdgeSpace(withColor: #colorLiteral(red: 0.9568627451, green: 0.4666666667, blue: 0.1294117647, alpha: 1), edge: .bottom)
        self.setupKeyboardObservers()
        self.switchToLanguage(self.currentLanguage)
        self.pushNotifSwitch.isOn = Network.authenticatedUser.transactionNotificaiton
    }
    
    @IBAction func viewDidTap(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func setupKeyboardObservers() -> Void {
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillShow, object: nil, queue: nil) { [weak self] (notif) in
            
            if let frame = notif.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
                self?.bottomConst.constant = frame.height * -1
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    self?.view.layoutIfNeeded()
                })
            }
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillHide, object: nil, queue: nil) { [weak self] _ in
            
            self?.bottomConst.constant = 0
            UIView.animate(withDuration: 0.2, animations: { [weak self] in
                self?.view.layoutIfNeeded()
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func applySettings(_ sender: Any) {
        
        var old:String?
        var new:String?
        
        if let newpass = self.newPasswordField.text, newpass.isEmpty == false {
            
            if let oldpass = self.oldPasswordField.text, oldpass.isEmpty == false {
                
                if let confirm = self.confirmPasswordField.text, confirm.isEmpty == false {
                    
                    if confirm == newpass {
                        
                        old = oldpass
                        new = newpass
                        
                    } else {
                        showErrorMessage( "error-pwd-confirm".local )
                        return
                    }
                    
                } else {
                    showErrorMessage("error-confirm-new-pwd".local)
                    return
                }
                
            } else {
                showErrorMessage("error-fill-old-pwd".local)
                return
            }
        }
        
        let push = self.pushNotifSwitch.isOn
        let lang = self.selectedLanguage == .arabic ? "ar" : "en"
        
        SVProgressHUD.show()
        
        Network.request(.updateUserSettings(push: push, lang: lang, oldPass: old, newPass: new), success: { (_) in
            
            SVProgressHUD.dismiss()
            Network.authenticatedUser.transactionNotificaiton = push
            self.applyLanguageChanges()
            
        }) { (err) in
            
            print("UPDATING USER SETTINGS: \n\(err.localizedDescription)")
            
            SVProgressHUD.dismiss()
            
            if err.error is CustomError {
                showErrorMessage(err.localizedDescription)
            } else {
                SVProgressHUD.showError(withStatus: "error-update-settings".local)
            }
        }
    }
    
    @IBAction func chooseEnglish(_ sender: Any) {
        
        self.promptUserForLanguageChange(language: .english)
    }
    
    @IBAction func chooseArabic(_ sender: Any) {
        
        self.promptUserForLanguageChange(language: .arabic)
    }
    
    private var selectedLanguage:Language!
    private func switchToLanguage (_ lang:Language) -> Void {
        
        self.englishSelectionView.isHidden = lang == .arabic
        self.arabicSelectionView.isHidden = lang == .english
        self.selectedLanguage = lang
    }
    
    fileprivate func promptUserForLanguageChange(language:Language) {
        
        guard self.currentLanguage != language else {
            self.switchToLanguage(language)
            return
        }
        
        let alert = UIAlertController(title: "change-lang-prompt-title".local,
                                      message: "change-lang-prompt-msg".local,
                                      preferredStyle: .alert)
        
        let proceed = UIAlertAction(title: "ok".local, style: .default, handler: { (action) in
            self.switchToLanguage(language)
        })
        
        let cancel = UIAlertAction(title: "cancel".local, style: .destructive, handler: nil)
        alert.addAction(cancel)
        alert.addAction(proceed)
        
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
    }
    
    func applyLanguageChanges() -> Void {
        
        guard self.currentLanguage != self.selectedLanguage else {
            return
        }
        
        LangChangeCloseViewController.launch(withLanguageCode: self.selectedLanguage.code,
                                             andRegion: self.selectedLanguage.region)
    }
    
    @IBAction func showOldPassword(_ sender: Any) {
        
        let button = sender as! UIButton
        
        if button.tag == 0 {
            
            button.setImage(#imageLiteral(resourceName: "non_visability"), for: .normal)
            button.tag = 1
            self.oldPasswordField.isSecureTextEntry = false
            
        } else {
            
            button.setImage(#imageLiteral(resourceName: "visibility"), for: .normal)
            button.tag = 0
            self.oldPasswordField.isSecureTextEntry = true
        }
    }
    
    @IBAction func showEnterNewPassword(_ sender: Any) {
        
        let button = sender as! UIButton
        
        if button.tag == 0 {
            
            button.setImage(#imageLiteral(resourceName: "non_visability"), for: .normal)
            button.tag = 1
            self.newPasswordField.isSecureTextEntry = false
            self.confirmPasswordField.isSecureTextEntry = false
            
        } else {
            
            button.setImage(#imageLiteral(resourceName: "visibility"), for: .normal)
            button.tag = 0
            self.newPasswordField.isSecureTextEntry = true
            self.confirmPasswordField.isSecureTextEntry = true
        }
    }
}


