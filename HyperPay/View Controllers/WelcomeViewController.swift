//
//  WelcomeViewController.swift
//  HyperPay
//
//  Created by Mac on 6/6/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import UIImageColors

class WelcomeViewController: BaseViewController {
    
    var shouldRestoreUser:Bool = false
    
    @IBOutlet weak var logoContainer: UIView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var hyperPayLogo: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isSidMenuSwipeEnabled = false
        hyperPayLogo.image = hyperPayLogo.image!.withRenderingMode(.alwaysTemplate)
        
        self.titleLabel.text = UserDefaults.standard[.companyName] ?? "Unknown"
        
        let image:UIImage
        if let userImageData:Data = UserDefaults.standard[.companyPicture],
            let userImg = UIImage(data: userImageData) {
            image = userImg
        } else {
            image = #imageLiteral(resourceName: "company_logo")
        }
        
        image.getColors { colors in
            
            self.logo.image = image
            self.logo.backgroundColor = UIColor.white
            self.logo.layer.cornerRadius = self.logo.frame.width * 0.5
            self.logo.layer.masksToBounds = true
            
            self.logoContainer.layer.cornerRadius = self.logo.frame.width * 0.5
            self.logoContainer.layer.borderWidth = 5
            self.logoContainer.layer.borderColor = UIColor.white.cgColor
            self.logoContainer.applyDarkShadow(opacity: 0.3, offsetY: 2)
            
            let gradient = CAGradientLayer()
            gradient.frame = self.view.bounds
            gradient.colors = [
                colors.primary.cgColor,
                colors.background.cgColor
            ]
            
            gradient.locations = [0.2, 0.8]
            gradient.startPoint = CGPoint(x: 0.15, y: 0)
            gradient.endPoint = CGPoint(x: 0.85, y: 1)
            
            var pr:CGFloat = 0, pg:CGFloat = 0, pb:CGFloat = 0
            if let primary = colors.primary,
                primary.getRed(&pr, green: &pg, blue: &pb, alpha: nil) {
                
                if (pr * 0.299 + pg * 0.587 + pb * 0.114) * 255 > 186 {
                    
                    self.titleLabel.textColor = UIColor.black
                    self.hyperPayLogo.tintColor = UIColor.black
                } else {
                    
                    self.titleLabel.textColor = UIColor.white
                    self.hyperPayLogo.tintColor = UIColor.white
                }
            }
            
            self.titleLabel.applyDarkShadow(opacity: 0.3, offsetY: 1)
            self.view.layer.insertSublayer(gradient, at: 0)
        }
        
        if self.shouldRestoreUser {
            
            Network.restoreUser { (success, error) in
                if success {
                    self.loadNextView()
                } else {
                    Network.logoutUser()
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
            
        } else {
            
            self.perform(#selector(self.loadNextView), with: nil, afterDelay: 1.0 )
        }
    }
    
    @objc func loadNextView() {
        
        if let MainTabViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainTabBar") {
            
            self.navigationController?.pushViewController(MainTabViewController, animated: true)
        }
    }

}
