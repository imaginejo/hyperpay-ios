//
//  SelectValueView.swift
//  HyperControls
//
//  Created by Suhayb Ahmad on 6/21/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

protocol SelectValueViewDelegate:class {
    
    func selectValueViewDidRequestMore( _ selectView:SelectValueView)
    func selectValueViewDidRequestDeletion( _ selectView:SelectValueView)
    func selectValueView(_ selectView:SelectValueView, didAssignValue value:String?)
}

class SelectValueView: UIStackView, ItemPickerViewControllerDelegate {
    
    weak var delegate:SelectValueViewDelegate?
    
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var valueView: UIView!
    @IBOutlet weak var orButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    class func create( highlighted:Bool ) -> SelectValueView {
        
        let view = UIView.loadFromNibNamed("SelectValueView") as! SelectValueView
        
        if highlighted {
            
            view.orButton.setTitleColor(#colorLiteral(red: 0.1197363362, green: 0.5927556157, blue: 0.3233833313, alpha: 1), for: .normal)
            view.orButton.layer.borderColor = #colorLiteral(red: 0.1197363362, green: 0.5927556157, blue: 0.3233833313, alpha: 1)
        } else {
            
            view.orButton.setTitleColor(#colorLiteral(red: 0.3791251243, green: 0.4906057938, blue: 0.5880173908, alpha: 1), for: .normal)
            view.orButton.layer.borderColor = UIColor(white: 0.93, alpha: 1).cgColor
        }
        
        return view
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [self.deleteButton, self.orButton, self.valueView].forEach { (view) in
            
            view?.applyDarkShadow(opacity: 0.07, offsetY: 0, radius: 2)
            view?.layer.borderColor = UIColor(white: 0.93, alpha: 1).cgColor
            view?.layer.borderWidth = 1.0
            view?.layer.cornerRadius = 4.0
        }
    }
    
    var selectedValue:String?
    
    func showFieldValue(_ value:String? ) -> Void {
        
        self.selectedValue = value
        
        var title:String = "select-value".local
        if let vl = value,
            let pvalue = self.currentField?.possibleValues?.first(where: { $0.value == vl }) {
            title = pvalue.title
        }
        
        self.valueLabel.text = title
        self.valueLabel.textColor = value == nil ? UIColor.lightGray : UIColor.black
        self.clearButton.isHidden = value == nil
    }
    
    @IBAction func orPressed(_ sender: Any) {
        
        self.delegate?.selectValueViewDidRequestMore(self)
    }
    
    @IBAction func deleteValue(_ sender: Any) {
        
        self.delegate?.selectValueViewDidRequestDeletion(self)
    }
    
    var excludedValues = [String]()
    
    weak var currentField:SearchFilterField?
    @IBAction func launchValueSelection(_ sender: Any) {
        
        if let items = self.currentField?.possibleValues?.filter({ (pval) -> Bool in
            
            return self.excludedValues.contains(pval.value) == false
            
        }).map({ (value) -> SelectionItem in
            
            return SelectionItem(value)
            
        }), items.count > 0 {
            
            ItemPickerViewController.launch(forList: items, withItemName: self.currentField?.title, withDelegate: self)
        }
    }
    
    func itemPicker(_ itemSelector: ItemPickerViewController, didSelectItem item: ItemPickerViewController.Item) {
        
        let value = (item as! SelectionItem).pvalue.value
        self.showFieldValue( value )
        self.delegate?.selectValueView(self, didAssignValue: value)
    }
    
    func itemPickerDidClearSelection(_ itemSelector: ItemPickerViewController) {
        
        self.showFieldValue(nil)
        self.delegate?.selectValueView(self, didAssignValue: nil)
    }
    
    @IBAction func clearSelection(_ sender: Any) {
        
        self.showFieldValue(nil)
        self.delegate?.selectValueView(self, didAssignValue: nil)
    }
    
    class SelectionItem :ItemPickerViewController.Item {
        
        var pvalue:SearchFilterField.PossibleValue
        init(_ vl:SearchFilterField.PossibleValue) {
            self.pvalue = vl
        }
        
        override var title: String {
            return self.pvalue.title
        }
    }
}
