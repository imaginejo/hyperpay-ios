//
//  TransactionsTableViewCell.swift
//  HyperPay
//
//  Created by Mac on 5/23/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class TransactionsTableViewCell: UITableViewCell {

    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var transIdLabel: UILabel!
    
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var channelLabel: InsetLabel!
    @IBOutlet weak var paymentTypeLabel: InsetLabel!
    @IBOutlet weak var paymentMethodLabel: InsetLabel!
    
    @IBAction func transIdLabelTapped(_ sender: Any) {
        
        showAlertMessage("Transaction ID", message: self.currentReport.transactionId, okLabel: "Close")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        self.channelLabel.insetX = 7
        self.paymentMethodLabel.insetX = 7
        self.paymentTypeLabel.insetX = 7
        
        self.container.layer.cornerRadius = 6.2
        self.container.applyDarkShadow(opacity: 0.4, offsetY: 1)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector( transIdLabelTapped(_:) ))
        
        self.transIdLabel.isUserInteractionEnabled = true
        self.transIdLabel.addGestureRecognizer(tap)
    }
    
    weak var currentReport:TransactionReport!
    func displayReport(_ report:TransactionReport, asType type:ReportType) -> Void {
        
        let df = DateFormatter()
        df.dateFormat = "d MMMM yyyy - h:mm a"
        
        self.currentReport = report
        self.dateLabel.text = df.string(from: report.requestDate).uppercased().replacingArabicNumbers()
        
        switch type {
        case .general, .reconciliation:
            self.customerNameLabel.text = report.accountHolder
            self.transIdLabel.text = report.transactionId
            
        case .risk:
            self.customerNameLabel.text = report.insitituteName
            self.transIdLabel.text = report.authorizeId
        }
        
        switch type {
        case .general, .risk:
            
            let amountStr = NSMutableAttributedString()
            
            switch report.paymentType.uppercased() {
            case "PA":
                
                amountStr.append(NSAttributedString(string: "("))
                amountStr.append(moneyString(report.amount, size: 14))
                amountStr.append(NSAttributedString(string: ")"))
                
            case "RF", "RV":
                
                amountStr.append(NSAttributedString(string: "-"))
                amountStr.append(moneyString(report.amount, size: 14))
            default:
                
                amountStr.append(moneyString(report.amount, size: 14))
            }
            
            self.volumeLabel.attributedText = amountStr
            self.currencyLabel.text = report.currency
            
            if report.result == "ACK" {
                self.volumeLabel.textColor = #colorLiteral(red: 0.1843137255, green: 0.5647058824, blue: 0.2862745098, alpha: 1)
            } else {
                self.volumeLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            }
            
        case .reconciliation:
            
            let accNum = "\(report.fullAccountNumber)"
            
            self.volumeLabel.attributedText = NSAttributedString(string: accNum, attributes: [
                .foregroundColor: #colorLiteral(red: 0.3051207114, green: 0.6280395486, blue: 0.748853518, alpha: 1)
            ])
            
            self.currencyLabel.text = report.connectorTxID3
        }
        
        switch type {
        case .general:
            self.paymentMethodLabel.text = report.paymentMethod
            self.paymentTypeLabel.text = report.paymentType
            self.channelLabel.text = report.channelName
            
        case .reconciliation:
            self.paymentMethodLabel.text = report.verStatus
            self.paymentTypeLabel.text = report.ccvResult
            self.channelLabel.text = report.channelName
            
        case .risk:
            self.paymentMethodLabel.text = report.verStatus
            self.paymentTypeLabel.text = report.ccvResult
            
            if report.result == "ACK" {
                self.channelLabel.text = "approved".local
                self.channelLabel.backgroundColor = #colorLiteral(red: 0.6535916924, green: 0.738933742, blue: 0.2482890487, alpha: 1)
            } else {
                self.channelLabel.text = "declined".local
                self.channelLabel.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            }
        }
        
        self.updateFields(report, asType: type)
    }
    
    func updateFields(_ report:TransactionReport, asType type:ReportType) -> Void {
        
        self.stackView.arrangedSubviews.forEach { (v) in self.stackView.removeArrangedSubview(v)}
        self.stackView.subviews.forEach { (v) in v.removeFromSuperview()}
        
        let fields:[(String, String)]
        
        switch type {
        case .general:
            fields = [
                ("UNIQUE ID", report.uniqueId ),
                ("RISK SCORE", report.riskScore ?? "N/A" )
            ]
        case .reconciliation:
            fields = [
                ("BIN\\ BIN COUNTRY", "\(report.bin) \\ \( report.binCountry )"),
                ("UNIQUE ID", report.uniqueId ),
                ("ENTITY ID", report.channelId ),
                ("MERCHANT BANK ID", "TBD" )
            ]
        case .risk:
            fields = [
                ("BIN\\ BIN COUNTRY", "\(report.bin) \\ \( report.binCountry )"),
                ("UNIQUE ID", report.uniqueId ),
                ("RESPONSE CODE", report.returnCode )
            ]
        }
        
        fields.forEach { (name, value) in
            
            let view = TransactionPropertyView.create()
            
            view.nameLabel.text = name.uppercased().local
            view.valueLabel.text = value
            
            let const = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal,
                                           toItem: nil, attribute: .notAnAttribute,
                                           multiplier: 1, constant: 35)
            const.priority = .defaultHigh
            view.addConstraint( const )
            
            self.stackView.addArrangedSubview(view)
        }
    }
}

func moneyString(_ volume:Double, currency:String = "", font:String? = nil, size:CGFloat) -> NSAttributedString {
    
    let fontName = font ?? "SeroPro-Medium"
    
    let nf = NumberFormatter()
    nf.locale = Locale(identifier: "en_US")
    nf.currencySymbol = currency
    nf.alwaysShowsDecimalSeparator = true
    nf.numberStyle = .currency
    nf.maximumFractionDigits = 2
    nf.minimumFractionDigits = 2
    
    if let amountStr = nf.string(from: NSNumber(value: volume) ){
        
        let size2 = size * 10 / 14
        let strAttr = NSMutableAttributedString(string: amountStr)
        strAttr.addAttribute(.font, value: UIFont(name: fontName, size: size)!, range: NSRange(location: 0, length: amountStr.count) )
        strAttr.addAttributes([
            NSAttributedStringKey.font: UIFont(name: fontName, size: size2)!,
            NSAttributedStringKey.baselineOffset: 3 / 14 * size
        ], range: NSRange(location: amountStr.count - 2, length: 2))
        
        return strAttr
    }
    
    return NSAttributedString(string: "\(volume)", attributes: [
        NSAttributedStringKey.font: UIFont(name: "SeroPro-Medium", size: size)!,
    ])
}

extension UILabel {
    
    func setMoney( _ volume:Double, currency:String = "") -> Void {
        self.attributedText = moneyString(volume, currency: currency, font: self.font.fontName, size: self.font.pointSize)
    }
}

class InsetLabel:UILabel {
    
    var insetX:CGFloat = 0
    var insetY:CGFloat = 0
    
    open override var intrinsicContentSize: CGSize {
        
        var size = super.intrinsicContentSize
        size.width += self.insetX * 2.0
        size.height += self.insetY * 2.0
        
        return size
    }
}
