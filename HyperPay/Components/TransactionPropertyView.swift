//
//  TransactionPropertyView.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 6/27/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class TransactionPropertyView: UIView {
    
    class func create() -> TransactionPropertyView {
        
        let view =  UIView.loadFromNibNamed("TransactionPropertyView") as! TransactionPropertyView
        return view
    }

    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
}
