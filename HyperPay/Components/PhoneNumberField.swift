//
//  PhoneNumberField.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 9/19/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class PhoneNumberField: UIView, ItemSelectorViewControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var flagView: UIImageView!
    @IBOutlet weak var dialCodeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textField.delegate = self
        self.textField.returnKeyType = .done
        self.textField.keyboardType = .phonePad
        self.itemSelectorDidSelectItem( CountryInfo.country(forCode: Locale.current.regionCode ?? "JO")! )
    }
    
    var selectedCountry:CountryInfo!
    
    @IBAction func launchCountrySelection(_ sender: Any) {
        
        ItemSelectorViewController.launch(forList: CountryInfo.all,
                                          withTitle: "Select Country", withDelegate: self)
    }
    
    func itemSelectorDidSelectItem(_ item: ItemSelectorViewController.Item) {
        
        if let selItem = item as? CountryInfo {
            
            self.selectedCountry = selItem
            self.dialCodeLabel.text = selItem.dialCode
            self.flagView.image = UIImage(named: selItem.imageName!)
        }
    }
    
    func itemSelectorDidClearSelection(_ itemSelector: ItemSelectorViewController) {
        
        self.selectedCountry = CountryInfo.country(forCode: "JO")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    var text:String? {
        set { self.textField.text = newValue }
        get { return self.textField.text }
    }
    
    var phoneNumber:String? {
        
        guard let phone = self.textField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
            phone.isEmpty == false else {
                return nil
        }
        
        return self.selectedCountry.dialCode
            .replacingOccurrences(of: "+", with: "")
            .replacingOccurrences(of: " ", with: "")
            + phone.replacingOccurrences(of: " ", with: "").replacingArabicNumbers()
    }
}
