//
//  ChannelSelector.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 6/27/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class ChannelCell:UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
}

protocol ChannelSelectorDelegate:class {
    
    func channelSelector(_ selector:ChannelSelector, didSelectChannel channel:ResponseModel.User.Channel, atIndex index:Int)
}

class ChannelSelector: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource {
    
    weak var selectorDelegate:ChannelSelectorDelegate?
    
    private let ITEM_WIDTH:CGFloat = 120
    var snappingLayout = SnappingCollectionViewLayout()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        snappingLayout.itemSize = CGSize(width: ITEM_WIDTH, height: 35)
        snappingLayout.minimumLineSpacing = 0
        snappingLayout.minimumInteritemSpacing = 10000
        snappingLayout.scrollDirection = .horizontal
        
        self.delegate = self
        self.dataSource = self
        self.collectionViewLayout = self.snappingLayout
        self.decelerationRate = UIScrollViewDecelerationRateFast
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let vf = self.frame
        let sl = (vf.width - ITEM_WIDTH) / 2.0
        snappingLayout.sectionInset = UIEdgeInsets(top: 0, left: sl, bottom: 0, right: sl)
    }
    
    var channels = [ResponseModel.User.Channel]()
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.channels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "channelCell", for: indexPath) as! ChannelCell
        
        cell.titleLabel.text = self.channels[indexPath.item].name
        cell.titleLabel.textColor = indexPath.item == self.selectedIndex
            ? UIColor.white
            : UIColor(white: 1, alpha: 0.5)
        
        return cell
    }
    
    var selectedIndex:Int = 0
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.focusOnItem(withIndex: indexPath)
    }
    
    func resetToSelectedIndex() -> Void {
        self.reloadData()
        self.scrollToItem(at: IndexPath(item: self.selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    private func focusOnItem(withIndex index:IndexPath) -> Void {
        
        self.selectedIndex = index.item
        self.reloadData()
        self.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
        self.selectorDelegate?.channelSelector(self, didSelectChannel: self.channels[index.item], atIndex: index.item)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        var cCell:UICollectionViewCell?
        for cell in self.visibleCells {
            
            let x0 = self.contentOffset.x + self.frame.width / 2.0
            let cx = cell.frame.midX
            
            if abs(cx - x0) < ITEM_WIDTH * 0.5 {
                cCell = cell
                break;
            }
        }
        
        if let _cell = cCell, let index = self.indexPath(for: _cell) {
            
            self.focusOnItem(withIndex: index)
        }
    }
}
