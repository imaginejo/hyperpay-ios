//
//  DayHeaderView.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 6/26/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class DayHeaderView: UIView {
    
    class func create() -> DayHeaderView {
        
        return UIView.loadFromNibNamed("DayHeaderView") as! DayHeaderView
    }
    
    var isFloating:Bool = false
    var isDisplayed:Bool = false
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var container: UIView!
    
    func darken() -> Void {
        
        UIView.animate(withDuration: 0.3) {
            self.container.backgroundColor = UIColor(valueRed: 40, green: 60, blue: 90, alpha: 0.7)
        }
    }
    
    func lighten() -> Void {
        
        UIView.animate(withDuration: 0.3) {
            self.container.backgroundColor = UIColor(white: 1, alpha: 0.15)
        }
    }
}
