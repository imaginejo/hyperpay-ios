//
//  DateRangeSelector.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 8/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

protocol DateRangeSelectorDelegate:class {
    
    func dateRangeSelector(_ selector:DateRangeSelector, didChangeRange range:(Date, Date)) -> Void
    func dateRangeSelectorDidClear(_ selector:DateRangeSelector) -> Void
}

class DateRangeSelector: UIView, DatePickerViewControllerDelegate {
    
    @IBOutlet weak var startDateLabel:UILabel!
    @IBOutlet weak var endDateLabel:UILabel!
    
    weak var delegate:DateRangeSelectorDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.applyDarkShadow(opacity: 0.07, offsetY: 0, radius: 2)
        self.layer.borderColor = UIColor(white: 0.93, alpha: 1).cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 4.0
        self.displayDates()
    }
    
    func setRange(start:Date, end:Date) -> Void {
        self.startDate = start
        self.endDate = end
        self.displayDates()
    }
    
    @IBAction func startDateTapped(sender: Any) {
        
        DatePickerViewController.launch(delegate: self, initialDate: self.startDate, tag: 0)
    }
    
    @IBAction func endDateTapped(sender: Any) {
        
        DatePickerViewController.launch(delegate: self, initialDate: self.endDate, tag: 1)
    }
    
    var startDate:Date?
    var endDate:Date?
    
    func datePickerController(_ datePickerVC: DatePickerViewController, didSelectDate selectedDate: Date) {
        
        switch datePickerVC.tag {
        case 0:
            self.startDate = selectedDate
        case 1:
            self.endDate = selectedDate
        default:
            break
        }
        
        self.displayDates()
        self.checkDates()
    }
    
    func datePickerControllerDidClearSelection(_ datePickerVC: DatePickerViewController) {
        
        switch datePickerVC.tag {
        case 0:
            self.startDate = nil
        case 1:
            self.endDate = nil
        default:
            break
        }
        
        self.displayDates()
        self.checkDates()
    }
    
    func checkDates() -> Void {
        
        if let start = self.startDate, let end = self.endDate {
            self.delegate?.dateRangeSelector(self, didChangeRange: (start, end))
        } else {
            self.delegate?.dateRangeSelectorDidClear(self)
        }
    }
    
    func displayDates() -> Void {
        
        let locale = Locale(identifier: "en_US")
        
        self.startDateLabel.text = self.startDate?.format("dd/MM/yyyy", locale: locale).replacingArabicNumbers() ?? "from-date".local
        self.endDateLabel.text = self.endDate?.format("dd/MM/yyyy", locale: locale).replacingArabicNumbers() ?? "to-date".local
    }
}
