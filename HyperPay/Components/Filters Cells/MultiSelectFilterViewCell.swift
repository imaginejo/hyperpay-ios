//
//  FilterViewCell.swift
//  HyperControls
//
//  Created by Suhayb Ahmad on 6/21/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class MultiSelectFilterViewCell: FilterViewCell, SelectValueViewDelegate {
    
    @IBOutlet weak var valuesStack: UIStackView!
    
    override func popForFilter(_ filter:SearchFilter) -> Void {
        
        super.popForFilter(filter)
        
        if filter.multiValue().values.count == 0 {
            filter.multiValue(inserting: nil, at: 0)
        }
        
        self.updateValuesFields()
    }
    
    func updateValuesFields() -> Void {
        
        self.valuesStack.arrangedSubviews.forEach { (view) in
            self.valuesStack.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
        
        let values = self.currentFilter.multiValue().values
        for i in 0 ..< values.count {
            
            let selectView = SelectValueView.create(highlighted: i < (values.count - 1) )
            selectView.delegate = self
            selectView.currentField = self.currentFilter.field
            selectView.showFieldValue( values[i] )
            
            self.valuesStack.addArrangedSubview( selectView )
        }
        
        self.updateExcluded()
    }
    
    func selectValueViewDidRequestMore(_ selectView: SelectValueView) {
        
        let valsCount = self.currentFilter.field.possibleValues!.count
        
        if self.currentFilter.multiValue().values.count < valsCount {
            
            if let index = self.valuesStack.arrangedSubviews.index(of: selectView) {
                self.currentFilter.multiValue(inserting: nil, at: index + 1)
                self.updateCellSize()
            }
        }
    }
    
    func selectValueViewDidRequestDeletion(_ selectView: SelectValueView) {
        
        if self.valuesStack.arrangedSubviews.count == 1 {
            
            self.delegate?.filterCell(self, didRequireDeletionFor: self.currentFilter)
            
        } else if let index = self.valuesStack.arrangedSubviews.index(of: selectView) {
            
            self.valuesStack.removeArrangedSubview( selectView )
            selectView.removeFromSuperview()
            
            self.currentFilter.multiValue(removingValueAt: index)
            self.updateCellSize()
        }
    }
    
    func selectValueView(_ selectView: SelectValueView, didAssignValue value: String?) {
        
        if let index = self.valuesStack.arrangedSubviews.index(of: selectView) {
            self.currentFilter.multiValue(updating: value, at: index)
            self.updateExcluded()
        }
    }
    
    func updateExcluded() -> Void {
        
        var exclude = [String]()
        self.currentFilter.multiValue().values.forEach { (value) in
            if let vl = value {
                exclude.append(vl)
            }
        }
        
        self.valuesStack.arrangedSubviews.forEach { (view) in
            (view as! SelectValueView).excludedValues = exclude
        }
    }
    
    func updateCellSize() -> Void {
        
        let num = self.currentFilter.multiValue().values.count
        self.currentFilter.displayHeight = 40 + CGFloat(num) * 40 + CGFloat(num - 1) * 7
        self.delegate?.filterCellRequireUpdate(self)
    }
}
