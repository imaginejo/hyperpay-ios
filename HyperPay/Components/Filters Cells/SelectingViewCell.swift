//
//  SelectingViewCell.swift
//  HyperControls
//
//  Created by Suhayb Ahmad on 6/21/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

protocol SelectingViewCellDelegate:CellDelegate {
    func selectingCellDidRequestFieldFilter(_ filter:SearchFilter) -> Void
}

class SelectingViewCell: UITableViewCell, ItemPickerViewControllerDelegate {
    
    @IBOutlet weak var fieldView: UIView!
    @IBOutlet weak var fieldNameLabel: UILabel!
    
    weak var delegate:SelectingViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        fieldView.applyDarkShadow(opacity: 0.07, offsetY: 0, radius: 2)
        fieldView.layer.borderColor = UIColor(white: 0.93, alpha: 1).cgColor
        fieldView.layer.borderWidth = 1.0
        fieldView.layer.cornerRadius = 4.0
        
        fieldNameLabel.text = "Select field.."
        fieldNameLabel.textColor = UIColor.lightGray
    }
    
    @IBAction func launchFieldSelection(_ sender: Any) {
        
        let items = Network.appConfig.searchFields.map { (field) -> FieldItem in
            return FieldItem(field)
        }
        
        ItemPickerViewController.launch(forList: items, withItemName: "Field", withDelegate: self)
    }
    
    func itemPickerDidClearSelection(_ itemSelector: ItemPickerViewController) {}
    
    func itemPicker(_ itemSelector: ItemPickerViewController, didSelectItem item: ItemPickerViewController.Item) {
        
        let filter = SearchFilter( (item as! FieldItem).field )
        self.delegate?.selectingCellDidRequestFieldFilter(filter)
    }
}

class FieldItem:ItemPickerViewController.Item {
    
    unowned var field:SearchFilterField
    init(_ field:SearchFilterField ) {
        self.field = field
        super.init()
    }
    
    override var title: String {
        return self.field.title
    }
}
