//
//  ConditionViewCell.swift
//  HyperControls
//
//  Created by Suhayb Ahmad on 7/2/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class ConditionViewCell: UITableViewCell {

    @IBOutlet weak var optionsControl: UISegmentedControl!
    
    weak var currentFilter:SearchFilter?
    
    @IBAction func segmentsChanged(_ sender: Any) {
        
        self.currentFilter?.condition = self.optionsControl.selectedSegmentIndex == 0
            ? FilterCondition.and
            : FilterCondition.or
    }
}
