//
//  SelectFilterViewCell.swift
//  HyperControls
//
//  Created by Suhayb Ahmad on 6/25/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

protocol CellDelegate:class {
    func filterCellRequireUpdate(_ cell:UITableViewCell) -> Void
}

protocol FilterCellDelegate:CellDelegate {
    func filterCell(_ cell:UITableViewCell, didRequireDeletionFor filter:SearchFilter)
}

class FilterViewCell:UITableViewCell {
    
    @IBOutlet weak var fieldName: UILabel!
    
    weak var delegate:FilterCellDelegate?
    weak var currentFilter:SearchFilter!
    
    func popForFilter(_ filter:SearchFilter) -> Void {
        
        self.currentFilter = filter
        self.fieldName.text = "\(self.currentFilter.field.title.uppercased()) \("is".local.uppercased())"
    }
}

class SelectFilterViewCell: FilterViewCell, SelectValueViewDelegate {

    var selectView: SelectValueView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectView = SelectValueView.create(highlighted: false)
        self.selectView.removeArrangedSubview(self.selectView.orButton)
        self.selectView.orButton.removeFromSuperview()
        self.selectView.delegate = self
        
        self.addSubview(self.selectView)
        
        self.addConstraint(NSLayoutConstraint(item: self.selectView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 30))
        self.addConstraint(NSLayoutConstraint(item: self.selectView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 15))
        self.addConstraint(NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal, toItem: self.selectView, attribute: .right, multiplier: 1, constant: 15))
        
        self.selectView.addConstraint( NSLayoutConstraint(item: self.selectView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40) )
    }
    
    override func popForFilter(_ filter:SearchFilter) -> Void {
        
        super.popForFilter( filter )
        
        self.selectView.currentField = self.currentFilter.field
        self.selectView.showFieldValue(self.currentFilter.simpleValue().value )
    }
    
    func selectValueViewDidRequestMore(_ selectView: SelectValueView) {}
    func selectValueViewDidRequestDeletion(_ selectView: SelectValueView) {
        
        self.delegate?.filterCell(self, didRequireDeletionFor: self.currentFilter)
    }
    
    func selectValueView(_ selectView: SelectValueView, didAssignValue value: String?) {
        
        self.currentFilter.simpleValue(value: value)
    }
}
