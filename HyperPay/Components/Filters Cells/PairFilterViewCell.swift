//
//  PairFilterViewCell.swift
//  HyperControls
//
//  Created by Suhayb Ahmad on 6/25/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class PairFilterViewCell: FilterViewCell, UITextFieldDelegate {

    @IBOutlet weak var fieldsStack: UIStackView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var valueField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var views:[UIView] = [self.deleteButton]
        
        views.append(contentsOf: self.fieldsStack.arrangedSubviews)
        views.forEach { (view) in
            
            view.applyDarkShadow(opacity: 0.07, offsetY: 0, radius: 2)
            view.layer.borderColor = UIColor(white: 0.93, alpha: 1).cgColor
            view.layer.borderWidth = 1.0
            view.layer.cornerRadius = 4.0
        }
    }
    
    override func popForFilter(_ filter: SearchFilter) {
        super.popForFilter( filter )
        
        self.nameField.text = filter.pairValue().name
        self.valueField.text = filter.pairValue().value
    }
    
    @IBAction func deleteFilter(_ sender: Any) {
        
        self.delegate?.filterCell(self, didRequireDeletionFor: self.currentFilter)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == self.nameField {
            self.currentFilter.pairValue(name: textField.text)
        } else {
            self.currentFilter.pairValue(value: textField.text)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}
