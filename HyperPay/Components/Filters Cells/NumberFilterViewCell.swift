//
//  NumberFilterViewCell.swift
//  HyperControls
//
//  Created by Suhayb Ahmad on 6/25/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class NumberFilterViewCell: FilterViewCell, ItemPickerViewControllerDelegate, UITextFieldDelegate {
    
    class OperationItem:ItemPickerViewController.Item {
        
        var operation:SearchFilterNumberComparison
        init(_ op:SearchFilterNumberComparison) {
            self.operation = op
        }
        override var title: String {
            return self.operation.rawValue
        }
    }
    
    @IBOutlet weak var valueField: UITextField!
    @IBOutlet weak var operationLabel: UILabel!
    @IBOutlet weak var operationView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var valueView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [self.operationView, self.deleteButton, self.valueView].forEach { (view) in
            
            view?.applyDarkShadow(opacity: 0.07, offsetY: 0, radius: 2)
            view?.layer.borderColor = UIColor(white: 0.93, alpha: 1).cgColor
            view?.layer.borderWidth = 1.0
            view?.layer.cornerRadius = 4.0
        }
    }
    
    override func popForFilter(_ filter: SearchFilter) {
        
        super.popForFilter(filter)
        
        self.valueField.text = "\( filter.numberValue().value )"
        self.showOperationValue( self.currentFilter.numberValue().operation )
    }
    
    @IBAction func launchOperationSelection(_ sender: Any) {
        
        ItemPickerViewController.launch(forList: [
            OperationItem(.equals),
            OperationItem(.greaterThan),
            OperationItem(.greaterThanOrEqual),
            OperationItem(.smallerThan),
            OperationItem(.smallerThanOrEqual)
        ], withItemName: "Operation", withDelegate: self)
    }
    
    @IBAction func deleteFilter(_ sender: Any) {
        
        self.delegate?.filterCell(self, didRequireDeletionFor: self.currentFilter)
    }
    
    func itemPicker(_ itemSelector: ItemPickerViewController, didSelectItem item: ItemPickerViewController.Item) {
        
        self.showOperationValue( (item as! OperationItem).operation )
    }
    
    func itemPickerDidClearSelection(_ itemSelector: ItemPickerViewController) {
        
        self.showOperationValue( .equals )
    }
    
    func showOperationValue(_ value:SearchFilterNumberComparison ) -> Void {
        
        self.currentFilter.numberValue(operation: value)
        self.operationLabel.text = value.rawValue
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let str = textField.text, let number = Double(str) {
            self.currentFilter.numberValue(value: number)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}


