//
//  TextFilterViewCell.swift
//  HyperControls
//
//  Created by Suhayb Ahmad on 6/25/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class TextFilterViewCell: FilterViewCell, UITextFieldDelegate {

    @IBOutlet weak var valueField: UITextField!
    @IBOutlet weak var valueView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [self.valueView, self.deleteButton].forEach { (view) in
            
            view?.applyDarkShadow(opacity: 0.07, offsetY: 0, radius: 2)
            view?.layer.borderColor = UIColor(white: 0.93, alpha: 1).cgColor
            view?.layer.borderWidth = 1.0
            view?.layer.cornerRadius = 4.0
        }
    }
    
    override func popForFilter(_ filter: SearchFilter) {
        super.popForFilter(filter)
        
        self.valueField.text = filter.simpleValue().value
    }
    
    @IBAction func deleteFilter(_ sender: Any) {
        
        self.delegate?.filterCell(self, didRequireDeletionFor: self.currentFilter)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.currentFilter.simpleValue(value: textField.text)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}
