//
//  DashboardTableViewCell.swift
//  HyperPay
//
//  Created by Suhayb Ahmad on 6/28/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

protocol DashboardTableViewCellDelegate:class {
    
    func dashboardCellDidTapHeader(_ cell:DashboardTableViewCell) -> Void
}

class DashboardTableViewCell: UITableViewCell {
    
    // Turnover
    @IBOutlet weak var turnoverVolumePercentLabel: UILabel!
    @IBOutlet weak var turnoverVolumeAmountLabel: UILabel!
    @IBOutlet weak var turnoverVolumeIconView: UIImageView!
    
    @IBOutlet weak var turnoverCountPercentLabel: UILabel!
    @IBOutlet weak var turnoverCountAmountLabel: UILabel!
    @IBOutlet weak var turnoverCountIconView: UIImageView!
    
    //Rejected
    @IBOutlet weak var rejectedVolumePercentLabel: UILabel!
    @IBOutlet weak var rejectedVolumeAmountLabel: UILabel!
    @IBOutlet weak var rejectedVolumeIconView: UIImageView!
    
    @IBOutlet weak var rejectedCountPercentLabel: UILabel!
    @IBOutlet weak var rejectedCountAmountLabel: UILabel!
    @IBOutlet weak var rejectedCountIconView: UIImageView!
    
    //Refunds
    @IBOutlet weak var refundsVolumePercentLabel: UILabel!
    @IBOutlet weak var refundsVolumeAmountLabel: UILabel!
    @IBOutlet weak var refundsVolumeIconView: UIImageView!
    
    @IBOutlet weak var refundsCountPercentLabel: UILabel!
    @IBOutlet weak var refundsCountAmountLabel: UILabel!
    @IBOutlet weak var refundsCountIconView: UIImageView!
    
    // Summary
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var negativeLabel: UILabel!
    @IBOutlet weak var positiveLabel: UILabel!
    @IBOutlet weak var neutralLabel: UILabel!
    @IBOutlet weak var rejectedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.container.layer.cornerRadius = 5
        self.background.layer.cornerRadius = 5
        self.background.applyDarkShadow(opacity: 0.4, offsetY: 1, radius: 2)
    }
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var background: UIView!
    
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    weak var delegate:DashboardTableViewCellDelegate?
    
    @IBOutlet weak var collapseButton: UIButton!
    @IBOutlet var belowViews: [UIView]!
    
    private let percentDF:NumberFormatter = {
        
        let percentDF = NumberFormatter()
        percentDF.locale = Locale(identifier: "en_US")
        percentDF.alwaysShowsDecimalSeparator = true
        percentDF.numberStyle = .none
        percentDF.minimumFractionDigits = 1
        percentDF.minimumIntegerDigits = 1
        return percentDF
    }()
    
    func percentString(from value:Int) -> String {
        if let str = percentDF.string(from: value) {
            return str + "%"
        }
        return "0.0%"
    }
    
    weak var currentDisplay:DashboardChannelDisplay!
    func show(_ disp:DashboardChannelDisplay ) -> Void {
        
        let info = disp.info
        let summary = disp.summary
        
        self.nameLabel.text = info.name
        
        let count = "⇋ \(summary.totalCount)"
        let countStr = "\(count) \("TRANSACTIONS".local)"
        
        let countAttStr = NSMutableAttributedString(string: countStr)
        countAttStr.addAttribute(
            .foregroundColor,
            value: UIColor.orange,
            range: (countStr as NSString).range(of: count))
        
        self.countLabel.attributedText = countAttStr
        
        self.negativeLabel.text = "\( summary.negative.count )"
        self.positiveLabel.text = "\( summary.positive.count )"
        self.neutralLabel.text = "\( summary.neutral.count )"
        self.rejectedLabel.text = "\( summary.rejected.count )"
        
        
        let moneyDF = NumberFormatter()
        moneyDF.locale = Locale(identifier: "en_US")
        moneyDF.currencySymbol = "$"
        moneyDF.alwaysShowsDecimalSeparator = true
        moneyDF.numberStyle = .currency
        moneyDF.maximumFractionDigits = 2
        moneyDF.minimumFractionDigits = 2
        moneyDF.minimumIntegerDigits = 1
        
        
        let positive = summary.positive
        self.turnoverVolumeAmountLabel.text = moneyDF.string(from: positive.volume)
        self.turnoverVolumePercentLabel.text = percentString(from: positive.volumeGrowth)
        self.turnoverVolumeIconView.image = imageForGrowth( positive.volumeGrowth )
        
        self.turnoverCountAmountLabel.text = "\(positive.count)"
        self.turnoverCountPercentLabel.text = percentString(from: positive.countGrowth)
        self.turnoverCountIconView.image = imageForGrowth( positive.countGrowth )
        
        
        let rejected = summary.rejected
        self.rejectedVolumeAmountLabel.text = moneyDF.string(from: rejected.volume)
        self.rejectedVolumePercentLabel.text = percentString(from: rejected.volumeGrowth)
        self.rejectedVolumeIconView.image = imageForGrowth( rejected.volumeGrowth )
            
        self.rejectedCountAmountLabel.text = "\(rejected.count)"
        self.rejectedCountPercentLabel.text = percentString(from: rejected.countGrowth)
        self.rejectedCountIconView.image = imageForGrowth( rejected.countGrowth )
        
        
        let negative = summary.negative
        self.refundsVolumeAmountLabel.text = moneyDF.string(from: negative.volume)
        self.refundsVolumePercentLabel.text = percentString(from: negative.volumeGrowth)
        self.refundsVolumeIconView.image = imageForGrowth( negative.volumeGrowth )
        
        self.refundsCountAmountLabel.text = "\(negative.count)"
        self.refundsCountPercentLabel.text = percentString(from: negative.countGrowth)
        self.refundsCountIconView.image = imageForGrowth( negative.countGrowth )
        
        
        self.currentDisplay = disp
        self.adapt(disp)
    }
    
    func imageForGrowth(_ change:Int ) -> UIImage {
        if change == 0 {
            return #imageLiteral(resourceName: "NoChange_Icon")
        } else if change < 0 {
            return #imageLiteral(resourceName: "DownData_Icon")
        } else {
            return #imageLiteral(resourceName: "UpData_Icon")
        }
    }
    
    func adapt(_ disp:DashboardChannelDisplay ) -> Void {
        
        for view in self.belowViews {
            view.isHidden = disp.state.hidden
        }
        
        self.collapseButton.setImage(disp.state.carretImage, for: .normal)
        self.containerHeight.constant = disp.state.height
    }
    
    @IBAction func headerDidTap(_ sender: Any) {
        
        self.delegate?.dashboardCellDidTapHeader(self)
    }
}

extension NumberFormatter {
    
    func string(from value:Int) -> String? {
        return  self.string(from: NSNumber(value: value))
    }
    
    func string(from value:Double) -> String? {
        return  self.string(from: NSNumber(value: value))
    }
    
    func string(from value:Float) -> String? {
        return  self.string(from: NSNumber(value: value))
    }
}
