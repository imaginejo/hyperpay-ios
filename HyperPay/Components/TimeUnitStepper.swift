//
//  TimeUnitStepper.swift
//  ChartsPlayground
//
//  Created by Suhayb Ahmad on 6/20/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

enum TimeUnit {
    case day
    case week
    case month
    case quarter
    case year
}

protocol TimeUnitStepperDelegate:class {
    
    func timeUnitStepper(_ unitStepper:TimeUnitStepper, didChangeRangeStartingAt start:Date, endingAt end:Date) -> Void
}

class TimeUnitStepper: UIView {
    
    weak var delegate:TimeUnitStepperDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var calendar = Calendar(identifier: .gregorian)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.calculateTime()
        self.displayTime()
    }
    
    @IBOutlet weak var timeLabel:UILabel!
    @IBOutlet weak var nextButton:UIButton!
    @IBOutlet weak var prevButton:UIButton!
    
    var refDate = Date()
    
    var value:Int = 0
    var start:Date = Date()
    var end:Date = Date()
    var duration:TimeInterval {
        return self.end.timeIntervalSince(self.start)
    }
    
    private var _unit:TimeUnit = .day
    
    var unit:TimeUnit {
        
        set{
            let prev = self._unit
            self._unit = newValue
            self.recalculateValue(prev)
            self.calculateTime()
            self.displayTime()
        }
        
        get {
            return self._unit
        }
    }
    
    private func calculateTime() -> Void {
        
        var date1:Date, date2:Date
        var component:Calendar.Component, dv = 1
                
        switch self.unit {
        case .day:
            
            date1 = self.calendar.startOfDay(for: refDate)
            component = .day
            
        case .week:
            
            date1 = self.calendar.date(from:
                self.calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: refDate)
            )!
            
            component = .weekOfYear
            
        case .month:
            
            date1 = self.calendar.date(from:
                self.calendar.dateComponents([.year, .month], from: refDate)
            )!
            
            component = .month
            
        case .quarter:
            
            var comps = self.calendar.dateComponents([.year, .month], from: refDate)
            let months = Double( comps.month! )
            comps.month = Int( (ceil( months / 3.0 ) - 1 ) * 3 ) + 1
            
            date1 = self.calendar.date(from: comps)!
            component = .month
            dv = 3
            
        case .year:
            
            date1 = self.calendar.date(from:
                self.calendar.dateComponents([.year], from: refDate)
            )!
            
            component = .year
        }
        
        date1 = self.calendar.date(byAdding: component, value: self.value * dv, to: date1)!
        date2 = self.calendar.date(byAdding: component, value: dv, to: date1)!
        
        date1 = self.calendar.date(bySetting: .hour, value: 0, of: date1)!
        date1 = self.calendar.date(bySetting: .minute, value: 0, of: date1)!
        date1 = self.calendar.date(bySetting: .second, value: 0, of: date1)!
        date1 = self.calendar.date(bySetting: .nanosecond, value: 0, of: date1)!
        
        date2 = self.calendar.date(bySetting: .hour, value: 0, of: date2)!
        date2 = self.calendar.date(bySetting: .minute, value: 0, of: date2)!
        date2 = self.calendar.date(bySetting: .second, value: 0, of: date2)!
        date2 = self.calendar.date(bySetting: .nanosecond, value: 0, of: date2)!
        date2 = self.calendar.date(byAdding: .nanosecond, value: Int(-10e+5), to: date2)!
        
        self.start = date1
        self.end = date2
    }
    
    private func displayTime() -> Void {
        
        let df = DateFormatter()
        var date = self.start
        
        switch self.unit {
        case .day:
            df.dateFormat = "EEEE - d/M/yyyy"
        case .week:
            df.dateFormat = "ww - yyyy"
        case .month:
            df.dateFormat = "MMMM - yyyy"
        case .quarter:
            df.dateFormat = "QQQ - yyyy"
        case .year:
            df.dateFormat = "yyyy"
        }
        
        switch self.unit {
        case .week:
            date = self.end.addingTimeInterval(-3600 * 10) // random near end date
        case .month, .quarter:
            date = self.start.addingTimeInterval(self.duration / 2.0)
        default:
            break
        }
        
        self.timeLabel.text = df.string(from: date).replacingArabicNumbers()
    }
    
    private func recalculateValue(_ prev:TimeUnit ) -> Void {
        
        if prev == .quarter {
            self.start = self.calendar.date(byAdding: .day, value: 1, to: self.start)!
            self.end = self.start.addingTimeInterval(self.duration)
        }
        
        switch self.unit {
        case .day:
            self.value = self.calendar.dateComponents([.day], from: refDate, to: self.start).day!
        case .week:
            self.value = self.calendar.dateComponents([.weekOfYear], from: refDate, to: self.start).weekOfYear!
        case .month:
            self.value = self.calendar.dateComponents([.month], from: refDate, to: self.start).month!
        case .quarter:
            
            var comps = self.calendar.dateComponents([.year, .month], from: refDate)
            var months = Double(comps.month!)
            comps.month = Int( (ceil( months / 3.0 ) - 1 ) * 3 ) + 1
            
            let date1 = self.calendar.date(from: comps)!
            
            comps = self.calendar.dateComponents([.year, .month], from: self.start)
            months = Double(comps.month!)
            comps.month = Int( (ceil( months / 3.0 ) - 1 ) * 3 ) + 1
            
            let date2 = self.calendar.date(from: comps)!
            self.value = Int( floor( Double(self.calendar.dateComponents([.month], from: date1, to: date2).month!) / 3.0 ) )
            
        case .year:
            self.value = self.calendar.dateComponents([.year], from: refDate, to: self.start).year!
        }
    }
    
    @IBAction func nextTime(_ sender: Any) {
        
        self.value += 1
        self.calculateTime()
        self.displayTime()
        self.delegate?.timeUnitStepper(self, didChangeRangeStartingAt: self.start, endingAt: self.end)
    }
    
    @IBAction func pastTime(_ sender: Any) {
        
        self.value -= 1
        self.calculateTime()
        self.displayTime()
        self.delegate?.timeUnitStepper(self, didChangeRangeStartingAt: self.start, endingAt: self.end)
    }
    
    func calculateLabelDate() -> Date {
        
        let dc1 = self.calendar.dateComponents(in: .current, from: self.start)
        let dc2 = self.calendar.dateComponents(in: .current, from: self.end)
        return dc2.year! > dc1.year!
            ? self.end
            : self.start.addingTimeInterval(self.duration / 2.0)
    }
    
    @IBAction func popUpCalendarSelection(_ sender: Any) {
        
        let mode:CalendarSelectionMode?
        
        switch self._unit {
        case .day:
            mode = .day
        case .week:
            mode = .week
        case .month:
            mode = .month
        case .year:
            mode = .year
        default:
            return
        }
        
        let date = calculateLabelDate()
        
        CalendarViewController.launch(mode: mode!, withInitialDate: date ) { (start, end) in
            
            self.start = start
            self.end = end
            
            self.recalculateValue(self._unit)
            self.displayTime()
            self.delegate?.timeUnitStepper(self, didChangeRangeStartingAt: self.start, endingAt: self.end)
        }
    }
}
