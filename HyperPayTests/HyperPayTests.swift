//
//  HyperPayTests.swift
//  HyperPayTests
//
//  Created by Suhayb Ahmad on 7/9/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import XCTest
import Alamofire
import Moya
@testable import HyperPay

class HyperPayTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testAppConfig(){
        
        let loadExp = expectation(description: "Loaded")
        
        Network.request(.authenticate(username: "962788149187", password: "123456"), success: { (model) in
            
            assert(model is ResponseModel.User)

            Network.authenticatedUser = model as! ResponseModel.User
            
            Network.request(.appConfig, success: { (model) in
                
                loadExp.fulfill()
                print(model)
                
            }) { (err) in
                
                loadExp.fulfill()
                assertionFailure(err.localizedDescription)
            }
            
        }) { (err) in
            
            loadExp.fulfill()
            assertionFailure(err.localizedDescription)
        }
        
        wait(for: [loadExp], timeout: 30)
    }
    
    func testSummary() {
        
        let loadExp = expectation(description: "Loaded")
        
        Network.request(.authenticate(username: "962788149187", password: "123456"), success: { (model) in
            
            assert(model is ResponseModel.User)
            
            Network.authenticatedUser = model as! ResponseModel.User
            
            Network.request(.summary(date: Date()), success: { (model) in
                
                loadExp.fulfill()
                print(model)
                
            }) { (err) in
                
                loadExp.fulfill()
                assertionFailure(err.localizedDescription)
            }
            
        }) { (err) in
            
            loadExp.fulfill()
            assertionFailure(err.localizedDescription)
        }
        
        wait(for: [loadExp], timeout: 30)
    }
    
    func testTransactions() {
        
        let loadExp = expectation(description: "Loaded")
        
        Network.request(.authenticate(username: "962788149187", password: "123456"), success: { (model) in
            
            assert(model is ResponseModel.User)
            
            let user = model as! ResponseModel.User
            
            Network.authenticatedUser = user
            Network.request(.transactions(
                reportType: "General",
                channel: user.channels.first!.id,
                fromDate: nil,
                toDate: nil,
                lastTransactionId: 0,
                pageSize: 5,
                sort: "Amount_desc"), success: { (model) in
                    
                    loadExp.fulfill()
                    print(model)
                    
            }, failure: { (err) in
                
                loadExp.fulfill()
                assertionFailure(err.localizedDescription)
            })
            
        }) { (err) in
            
            loadExp.fulfill()
            assertionFailure(err.localizedDescription)
        }
        
        wait(for: [loadExp], timeout: 30)
    }
    
    func testGraph() {
        
        let loadExp = expectation(description: "Loaded")
        
        Network.request(.authenticate(username: "962788149187", password: "123456"), success: { (model) in
            
            assert(model is ResponseModel.User)
            
            let user = model as! ResponseModel.User
            
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd"
            df.locale = Locale(identifier: "en_US")
            
            Network.authenticatedUser = user
            Network.request(.graph(
                channel: user.channels.first!.id,
                from: df.date(from: "2018-07-01")!,
                to: df.date(from: "2018-08-02")! ), success: { (model) in
                    
                    print(model)
                    loadExp.fulfill()
                    
            }, failure: { (err) in
                
                assertionFailure(err.localizedDescription)
                loadExp.fulfill()
            })
            
        }) { (err) in
            
            loadExp.fulfill()
            assertionFailure(err.localizedDescription)
        }
        
        wait(for: [loadExp], timeout: 30)
    }
    
    func testNotifications() {
        
        let loadExp = expectation(description: "Loaded")
        
        Network.request(.authenticate(username: "962788149187", password: "123456"), success: { (model) in
            
            assert(model is ResponseModel.User)
            
            let user = model as! ResponseModel.User
            
            Network.authenticatedUser = user
            Network.request(.notifications(lastNotificationId: 0, pageSize: 30), success: { (model) in
                
                print(model)
            }, failure: { (err) in
                
                assertionFailure(err.localizedDescription)
                loadExp.fulfill()
                
            })
            
        }) { (err) in
            
            assertionFailure(err.localizedDescription)
            loadExp.fulfill()
        }
        
        wait(for: [loadExp], timeout: 60)
    }
}
